﻿var page = require('webpage').create();
var system = require('system');

var lastReceived = new Date().getTime();
var requestCount = 0;
var responseCount = 0;
var requestIds = [];
var startTime = new Date().getTime();

page.onResourceReceived = function (response) {
    var index = requestIds.indexOf(response.id);

    if (index !== -1) {
        lastReceived = new Date().getTime();
        responseCount++;
        requestIds[index] = null;
    }
};

page.onResourceRequested = function (request, networkRequest) {
    if (request.url.indexOf('.png') > 0 || request.url.indexOf('.jpg') > 0 || request.url.indexOf('.gif') > 0 || request.url.indexOf('.css') > 0 || request.url.indexOf('.wott') > 0) {
        networkRequest.abort();
        return;
    }

    if (requestIds.indexOf(request.id) === -1) {
        requestIds.push(request.id);
        requestCount++;
    }
};

// Open the page
page.open(system.args[1], function () { });

var checkComplete = function () {
    // We don't allow it to take longer than 5 seconds but
    // don't return until all requests are finished
    if ((new Date().getTime() - lastReceived > 1000 && requestCount === responseCount) || new Date().getTime() - startTime > 10000) {
        clearInterval(checkCompleteInterval);
        console.log(page.content);
        phantom.exit();
    }
}
// Let us check to see if the page is finished rendering
var checkCompleteInterval = setInterval(checkComplete, 1);