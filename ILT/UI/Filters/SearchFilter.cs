﻿using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Crosscutting.Infrastructure;
using NReco.PhantomJS;

namespace UI.Filters
{
    public class SearchFilter : ActionFilterAttribute
    {
        static readonly string[] bots =
        {
            "googlebot", "yahoo", "baiduspider", "yandexbot", "teoma",
            "bingbot", "baiduspider", "facebookexternalhit", "twitterbot", "yandex", "rogerbot",
            "linkedinbot", "embedly", "bufferbot", "quora link preview", "showyoubot", "outbrain", "vkshare", "Mediapartners-Google", "Google-StructuredDataTestingTool"
        };

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            foreach (var bot in bots)
            {
                if (!HttpContext.Current.Request.UserAgent.Contains(bot, StringComparison.OrdinalIgnoreCase)) continue;
                var memoryStream = new MemoryStream();

                var phantomJS = new PhantomJS();
                phantomJS.Run("../proccessPage.js", new[] { HttpContext.Current.Request.Url.ToString() }, Stream.Null, memoryStream);

                filterContext.Result = new ContentResult
                {
                    Content = Encoding.UTF8.GetString(memoryStream.ToArray()),
                    ContentType = "text/html"
                };
            }
        }
    }
}