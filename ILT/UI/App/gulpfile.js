﻿require('date-utils');

var gulp = require('gulp'),
    _ = require('lodash'),
    $ = require('gulp-load-plugins')(),
    version = new Date().toFormat("_MM_DD_YYYY_HH24_MI_SS");
var mainBowerFiles = require('main-bower-files');

var File = {
    Extension: {
        JS: '.js',
        CSS: '.css'
    },

    Suffix: {
        Version: version,
        Min: '.min',
        VersionMin: version + '.min'
    }
};

var Sources = {
    'JS' : mainBowerFiles(/\.(js)$/).concat( [
        'app/app.js',
        'app/envinronment-constants.js',
        'app/routes.config.js',
        'app/translate.config.js',
        'app/**/*config*.js',
        'app/components/**/module.js',
        'app/components/**/*Base.js',
        'app/components/**/*.js'
    ]),
    'LESS': mainBowerFiles(/\.(less|css)$/).concat([
        'app/app.less',
        'app/components/**/*.css',
        'app/components/**/**/*.less'
    ]),
    'HTML': 'app/index.html',
    'TMPL': ['app/components/**/templates/**/*.html'],
    'DIST': 'dist'
};

var Settings = {
    Babel: {
        presets: ['es2015'],
        ignore:  ['app/lib/**/*.js']
    },
    Rename: { suffix: File.Suffix.VersionMin }
};

gulp.task('clean', function() {
    return gulp
        .src(Sources.DIST + '/*')
        .pipe($.clean({force: true}));
});

gulp.task('js-debug', function () {
    return gulp
        .src(Sources.JS)
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        .pipe($.babel(Settings.Babel))
        .pipe($.concat('bundle.js'))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(Sources.DIST));
});

gulp.task('js-release', function () {
    return gulp
        .src(Sources.JS)
        .pipe($.babel(Settings.Babel))
        .pipe($.ngAnnotate())
        .pipe($.concat('bundle.js'))
        .pipe(gulp.dest(Sources.DIST));
});

gulp.task('less-debug', function () {
    console.log(Sources.LESS);
    return gulp
        .src(Sources.LESS)
        .pipe($.plumber())
        .pipe($.less())
        .pipe($.sourcemaps.init())
        .pipe($.autoprefixer())
        .pipe($.concat('bundle.css'))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(Sources.DIST));
});

gulp.task('less-release', function () {
    return gulp
        .src(Sources.LESS)
        .pipe($.plumber())
        .pipe($.less())
        .pipe($.autoprefixer())
        .pipe($.concat('bundle.css'))
        .pipe($.minifyCss())
        .pipe($.rename(Settings.Rename))
        .pipe(gulp.dest(Sources.DIST));
});

gulp.task('html-debug', function() {
    return gulp
        .src(Sources.HTML)
        .pipe($.htmlReplace({
            'css': '/app/dist/bundle' + File.Extension.CSS,
            'js': '/app/dist/bundle' + File.Extension.JS,
            'templates': '/app/dist/templates' +File.Extension.JS
        }))
        .pipe($.rename('Index.cshtml'))
        .pipe(gulp.dest('../Views/Shared/'));
});

gulp.task('html-release', function() {
    return gulp
        .src(Sources.HTML)
        .pipe($.htmlReplace({
            'css': '/app/dist/bundle' + File.Suffix.VersionMin + File.Extension.CSS,
            'js': '/app/dist/bundle' + File.Suffix.VersionMin + File.Extension.JS
        }))
        .pipe($.rename('Index.cshtml'))
        .pipe(gulp.dest('../Views/Shared/'));
});

gulp.task('build-version-script', ['js-release','template-cache'], function () {
    return gulp
        .src(['dist/bundle.js', 'dist/templates.js'])
        .pipe($.concat('bundle' + File.Suffix.VersionMin + File.Extension.JS))
        .pipe($.uglify())
        .pipe(gulp.dest(Sources.DIST));
});

gulp.task('template-cache', function() {
    return gulp
        .src(Sources.TMPL)
        .pipe($.plumber())
        .pipe($.minifyHtml({empty: true}))
        .pipe($.angularTemplatecache(
            'templates.js',
            {
                module: 'app',
                root: 'components',
                standAlone: false
            }
        ))
        .pipe(gulp.dest(Sources.DIST));
});

//tasks to run
gulp.task('release', ['clean', 'build-version-script', 'less-release', 'html-release']);
gulp.task('debug', ['clean', 'js-debug', 'less-debug', 'template-cache', 'html-debug']);
gulp.task('watch', ['debug'], function() {
    gulp.watch(Sources.LESS, ['less-debug']);
    gulp.watch(Sources.JS, ['js-debug']);
    gulp.watch(Sources.TMPL, ['template-cache']);
    gulp.watch(Sources.HTML, ['html-debug']);
});

gulp.task('default', ['watch']);
