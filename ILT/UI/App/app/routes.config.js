﻿'use strict';

angular
    .module('app')
    .config(['$provide', function ($provide) {

        $provide.decorator('$state', [
            '$delegate', 'currentSocialNetwork',
            function stateDecorator($delegate, currentSocialNetwork) {


                var go = $delegate.go;

                function newGo() {
                    var args = Array.prototype.slice.call(arguments);
                    args[0] = currentSocialNetwork.name + '-' + args[0];
                    return go.apply($delegate, args);
                }

                $delegate.go = newGo;

                return $delegate;
            }
        ]);

    }])
    /*.run(function($state, $rootScope, TokenStoreSvc) {

        $rootScope.$state = $state;

        $rootScope.$on('$stateChangeStart', function (event, toState) {
            debugger;
            if (!(toState.auth && toState.auth.required)) {
                return;
            }

            if (toState.auth && toState.auth.required && !TokenStoreSvc.isUserLoggedIn()) {
                event.preventDefault();
                $state.go('signup');
                return;
            }

            /*if(!TokenStoreSvc.isSignupFinished()) {
                $state.go('completesignup');
                event.preventDefault();
                return;
            }

            if(!TokenStoreSvc.hasPermissions(toState.auth.required)) {
                $state.go('signup');
            }
        });

    })*/
    .config(['$locationProvider', '$urlMatcherFactoryProvider', function($locationProvider, $urlMatcherFactoryProvider){
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $urlMatcherFactoryProvider.caseInsensitive(true);

    }])
    .run(['$rootScope', function($rootScope){
        $rootScope.$on('$stateChangeSuccess',function(){
            $("html, body").animate({ scrollTop: 0 }, 200);
        })
    }]);