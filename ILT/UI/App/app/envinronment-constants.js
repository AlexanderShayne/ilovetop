﻿angular
    .module('app')
    .constant("ENVIRONMENT_CONSTANTS", {
        SERVICES_URI: "/api",
        SERVICES: {
            REPORTS: '/reports',
            PAGES: '/pages',
            POSTS: '/posts'
        },
        AUTH_REDIRECT_URI: '/AuthComplete.html'
    });
