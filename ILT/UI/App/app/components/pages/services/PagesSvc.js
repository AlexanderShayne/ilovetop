(function(){
    class PagesSvc{
        constructor(ENVIRONMENT_CONSTANTS, $resource, currentSocialNetwork){
            this.ENVIRONMENT_CONSTANTS = ENVIRONMENT_CONSTANTS;
            this.$resource = $resource;
            this.currentSocialNetwork = currentSocialNetwork;
        }

        get serviceUri(){
            return this.ENVIRONMENT_CONSTANTS.SERVICES_URI + '/' + this.currentSocialNetwork.name + this.ENVIRONMENT_CONSTANTS.SERVICES.PAGES;
        }

        query(query){
            return this._getResource()
                .get(query)
                .$promise;
        }

        removePage(pageId){
            return this._getResource()
                .removePage({
                    id : pageId
                })
                .$promise;
        }

        _getResource(){
            return this.$resource(
                this.serviceUri + '/:id',
                { id: '@id' },
                {
                    removePage: {
                        method: 'patch'
                    }
                });
        }
    }

    angular
        .module('app')
        .service('PagesSvc', PagesSvc);
})();
