(function(){
    class PagesCtrl{
        constructor(data, PagesSvc, TokenStoreSvc){
            this.data = data;
            this.PagesSvc = PagesSvc;
            this.TokenStoreSvc = TokenStoreSvc;
        }

        getTranslateKey(key){

            if (key.startsWith('top-posts-in')){
                return 'TOP_POSTS_IN';
            }

            return key.replace(/-/g, '_').toUpperCase();
        }

        removePage(item){
            this
                .PagesSvc
                .removePage(item.id)
                .then(() => {
                    item.excludeFromReports = true;
                });
        }

        isUserLoggedIn(){
            let result = this.TokenStoreSvc.isUserLoggedIn();
            return result;
        }
    }

    angular
        .module('app')
        .controller('PagesCtrl', PagesCtrl);
})();


