'use strict';

angular
    .module('app')
    .config(function($httpProvider) {
        $httpProvider.interceptors.push(AuthInterceptor);
    });

function AuthInterceptor($q, TokenStoreSvc, $injector) {
    return {
        request: request,
        responseError: responseError
    };

    function request(config) {
        if(!TokenStoreSvc.isUserLoggedIn() || config.dontNeedAuthorization) {
            return config;
        }

        if(TokenStoreSvc.isTokenExpired()) {
            return $injector
                .get('UsersSvc')
                .refreshToken()
                .then((token) => _addAuthHeaders(token, config));
        }

        return _addAuthHeaders(TokenStoreSvc.getToken(), config);
    }

    function responseError(rejection) {
        if (rejection.status === 401) {
            if(TokenStoreSvc.isUserLoggedIn() && TokenStoreSvc.isTokenExpired()) {
                return $injector
                    .get('UsersSvc')
                    .refreshToken()
                    .then((token) => _addAuthHeaders(token, rejection.config));
            }
            $injector.get('$state').go('signup');
        }

        return $q.reject(rejection);
    }

    function _addAuthHeaders(token, config) {
        config.headers = config.headers || {};
        config.headers.Authorization = 'Bearer ' + token;

        return config;
    }
}