'use strict';

const CLIENT_ID = 1;

class UsersSvc{
    constructor(ENVIRONMENT_CONSTANTS, $resource, lodash, TokenStoreSvc, $http){
        this.ENVIRONMENT_CONSTANTS = ENVIRONMENT_CONSTANTS;
        this.$resource = $resource;
        this._ = lodash;
        this.tokenStoreSvc = TokenStoreSvc;
        this.$http = $http;
    }

    get serviceUri() {
        return this.ENVIRONMENT_CONSTANTS.SERVICES_URI + this.ENVIRONMENT_CONSTANTS.SERVICES.USERS;
    }

    save(user){
        return this._getResource()
                    .save(user)
                    .$promise;
    }

    login(credentials) {
        return this._makeAuthRequest({
            'grant_type': 'password',
            'client_id': CLIENT_ID,
            'username': credentials.email,
            'password': credentials.password
        });
    }

    logout() {
        this.tokenStoreSvc.logout();
    }

    refreshToken() {
        return this._makeAuthRequest({
            'grant_type': 'refresh_token',
            'client_id': CLIENT_ID,
            'refresh_token': this.tokenStoreSvc.getRefreshToken()
        });
    }

    isUserLoggedIn(){
        return this.tokenStoreSvc.isUserLoggedIn();
    }

    _getResource(){
        return this.$resource(this.serviceUri + '/:id', { id: '@id' }, {
            exists: { url: this.serviceUri, method: 'HEAD' },
            updatePassword: { url: this.serviceUri, method: 'PATCH' }
        });
    }

    _makeAuthRequest(params) {
        var request = {
            method: 'POST',
            dontNeedAuthorization: true,
            url: this.serviceUri + '/Token',
            data: this._parametrize(params),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        };

        return this.$http(request).then((res) => {
            return this.tokenStoreSvc.setToken(res.data);
        });
    }

    _parametrize(data) {
        return this._.reduce(data, function (memo, item, key) {
            return memo + key + "=" + item + "&";
        }, '');
    }
}

angular
    .module('app')
    .service('UsersSvc', UsersSvc);