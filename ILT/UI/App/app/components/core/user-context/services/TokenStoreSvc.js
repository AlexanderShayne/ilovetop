'use strict';

class TokenStoreSvc {
    constructor(localStorageService) {
        this.store = localStorageService;
        this.storeKeys = {
            accessToken: 'access_token',
            refreshToken: 'refresh_token'
        };
        this.user = {isAdmin: this.hasAdminPermissions()};
    }

    hasAdminPermissions() {
        return this.isUserLoggedIn() && this.hasPermissions(['Admin']);
    }

    logout() {
        _.each(this.storeKeys, (localStorageKey) => {
            this.store.remove(localStorageKey);
        });
        this.user.isAdmin = this.hasAdminPermissions();
    }

    setToken(authData) {
        this.store.set(this.storeKeys.accessToken, authData.access_token);

        if (authData.refresh_token) {
            this.store.set(this.storeKeys.refreshToken, authData.refresh_token);
        }

        this.user.isAdmin = this.hasAdminPermissions();

        return authData.access_token;
    }

    isSignupFinished() {
        return !!this.getTokenPayload().roles;
    }

    hasPermissions(roles) {
        let userRoles = (this.getTokenPayload() || {}).roles;
        let normalizedUserRoles = this._normalizeRoles(userRoles);

        if (!roles) {
            return true;
        }

        if (!userRoles) {
            return false;
        }

        for (var i = 0; i < roles.length; i++) {
            if (_.includes(normalizedUserRoles, roles[i])) {
                return true;
            }
        }

        return false;
    }

    getTokenPayload() {
        let token = this.getToken();

        if (!token) return null;

        return this._parseToken(token);
    }

    getToken() {
        return this.store.get(this.storeKeys.accessToken);
    }

    getRefreshToken() {
        return this.store.get(this.storeKeys.refreshToken);
    }

    isTokenExpired() {
        return this.getTokenPayload().expDate <= new Date();
    }

    isUserLoggedIn() {
        return !!this.getTokenPayload();
    }

    _normalizeRoles(userRoles) {
        return !(userRoles instanceof Array) ? [userRoles] : userRoles;
    }

    _parseToken(token) {
        let parsedTokenPayload = JSON.parse(this._decode64(token.split('.')[1]));
        let expDate = new Date(parsedTokenPayload.exp * 1000);

        if (expDate <= new Date()) {
            return null;
        }

        return {
            id: parsedTokenPayload.nameid,
            userName: parsedTokenPayload.unique_name,
            roles: parsedTokenPayload.role,
            token: token,
            expDate: new Date(parsedTokenPayload.exp * 1000)
        };
    }

    _decode64(input) {
        if (atob) {
            return atob(input);
        }

        var keyStr = "ABCDEFGHIJKLMNOP" +
            "QRSTUVWXYZabcdef" +
            "ghijklmnopqrstuv" +
            "wxyz0123456789+/" +
            "=";

        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
        var base64test = /[^A-Za-z0-9\+\/\=]/g;
        if (base64test.exec(input)) {
            alert("There were invalid base64 characters in the input text.\n" +
                "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                "Expect errors in decoding.");
        }
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        do {
            enc1 = keyStr.indexOf(input.charAt(i++));
            enc2 = keyStr.indexOf(input.charAt(i++));
            enc3 = keyStr.indexOf(input.charAt(i++));
            enc4 = keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";

        } while (i < input.length);

        return output;
    }
}

angular
    .module('app')
    .service('TokenStoreSvc', TokenStoreSvc);