(function(){
    angular
        .module('app')
        .constant('socialNetwork', {
            vk: 4,
            coub: 5,
            vine: 6
        });
})();