﻿(function(){

    angular
        .module('app')
        .value('currentSocialNetwork', {});

    class MainCtrl{
        constructor($rootScope, socialNetwork, $location, currentSocialNetwork, $state){

            this.currentSocialNetwork = $rootScope.currentSocialNetwork = currentSocialNetwork;
            this.socialNetwork = $rootScope.socialNetwork = socialNetwork;
            this.$state = $state;

            var socialNetworkId = this._getSocialNetworkIdFromUrl($location.url());
            this._setupSocialNetwork(socialNetworkId);

            $rootScope.metatags = {
                title: 'I Love Top - только лучшее - наслаждайтесь!',
                description: 'Мы собрали для вас лучшие новости и сообщества в vk.com',
                image: ''
            };

            $rootScope.changeSocialNetwork = (socialNetworkId) => this.changeSocialNetwork(socialNetworkId);
        }

        changeSocialNetwork(socialNetworkId){
            this._setupSocialNetwork(socialNetworkId);
            this.$state.go('reports');
        }

        _getSocialNetworkIdFromUrl(url){
            if (url.startsWith('/coub')){
                return this.socialNetwork.coub;
            } else if (url.startsWith('/vine')) {
                return this.socialNetwork.vine;
            } else {
                return this.socialNetwork.vk;
            }
        }

        _setupSocialNetwork(socialNetworkId){
            if (socialNetworkId === this.socialNetwork.coub){
                this.currentSocialNetwork.id = this.socialNetwork.coub;
                this.currentSocialNetwork.name = 'coub';
                this.currentSocialNetwork.skin = 'green';
            } else if (socialNetworkId === this.socialNetwork.vine){
                this.currentSocialNetwork.id = this.socialNetwork.vine;
                this.currentSocialNetwork.name = 'vine';
                this.currentSocialNetwork.skin = 'orange';
            } else {
                this.currentSocialNetwork.id = this.socialNetwork.vk;
                this.currentSocialNetwork.name = 'vk';
                this.currentSocialNetwork.skin = 'blue';
            }
        }
    }

    angular
        .module('app')
        .controller('MainCtrl', MainCtrl);
})();

