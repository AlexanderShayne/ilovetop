﻿(function(){
    angular
        .module('vine')
        .config(['$translateProvider', function ($translateProvider) {

            $translateProvider
                .translations('ru-RU', {
                    'VINE.MENU.REPORTS': 'Все списки',
                    'VINE.MENU.PAGES': 'Все пользователи',
                    'VINE.MENU.TOP_POSTS': 'Топ вайны',
                    'VINE.MENU.TOP_PAGES': 'Топ пользователи',
                    'VINE.MENU.ANTI_TOP_PAGES': 'Худшие пользователи',
                    'VINE.MENU.TOP_POSTS_IN': 'Топ вайны у пользователя'
                });
        }]);
})();

