(function(){
    angular
        .module('vine')
        .config(['$translateProvider', function ($translateProvider) {

            $translateProvider
                .translations('ru-RU', {
                    'VINE.PAGES.PAGE_TITLE': 'Пользователи участвующие в статистике (>100.000 подписчиков)',
                    'VINE.PAGES.PAGE_DESCRIPTION': 'Мы добавляем в списки и собираем статистику только пользователей где больше 100.000 подписчиеов. Если Вашей страницы тут нет, наберите 100.000+ подписчиков и она автоматически добавится в этот список.',
                    'VINE.PAGES.NAME': 'Название',
                    'VINE.PAGES.CITY': 'Город',
                    'VINE.PAGES.COUNTRY': 'Страна',
                    'VINE.PAGES.FOLLOWERS_COUNT': 'Кол-во подписчиков',
                    'VINE.PAGES.LIKES_COUNT': 'Кол-во нравится',
                    'VINE.PAGES.REPORTS': 'Списки',
                    'VINE.PAGES.UPDATED_AT': 'Обновлено',
                    'VINE.PAGES.TOTAL': 'Кол-во',
                    'VINE.PAGES.DAY_CHANGES': 'Изименений за ДЕНЬ',
                    'VINE.PAGES.WEEK_CHANGES': 'Изименений за НЕДЕЛЮ',
                    'VINE.PAGES.MONTH_CHANGES': 'Изименений за МЕСЯЦ',
                    'VINE.PAGES.HASHTAGS': 'Хештеги'
                });
        }]);
})();
