(function(){
    angular
        .module('vine')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('vine-pages', {
                    url: '/vine/pages?page&pageSize',
                    templateUrl: 'components/pages/templates/pages.html',
                    controller: 'PagesCtrl',
                    controllerAs: 'vm',
                    resolve: {
                        data: function (PagesSvc, $stateParams) {
                            var page = $stateParams.page || 1;
                            var pageSize = $stateParams.pageSize || 10;

                            return PagesSvc.query({pageSize: pageSize, page: page});
                        }
                    }
                })
        }]);
})();
