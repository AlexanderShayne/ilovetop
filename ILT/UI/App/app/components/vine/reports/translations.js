(function(){
    angular
        .module('vine')
        .config(['$translateProvider', function ($translateProvider) {

            $translateProvider
                .translations('ru-RU', {

                    'VINE.REPORTS.TOP_POSTS_TITLE': 'Лучшие вайны за день/неделю/месяц',
                    'VINE.REPORTS.TOP_POSTS_DESCRIPTION': 'Только лучшее, наслаждайтесь!',

                    'VINE.REPORTS.TOP_PAGES_TITLE': 'Лучшие пользователи',
                    'VINE.REPORTS.ANTI_TOP_PAGES_TITLE': 'Худшие пользователи',

                    'VINE.REPORTS.TOP_POSTS_IN_TITLE': 'Лучшие вайны за все время у пользователя',

                    'VINE.REPORTS.REPOSTS': 'Репосты',
                    'VINE.REPORTS.UPDATED': 'Обновлено',
                    'VINE.REPORTS.FOR': 'За',
                    'VINE.REPORTS.POSTS': 'вайнов',
                    'VINE.REPORTS.PAGES': 'пользователей',
                    'VINE.REPORTS.TOP': 'топ',
                    'VINE.REPORTS.ANTI_TOP': 'анти-топ',

                    'VINE.REPORTS.TOP_PAGES.TITLE': 'Лучшие пользователи',
                    'VINE.REPORTS.TOP_PAGES.DESCRIPTION': 'Мы собрали для Вас лучшие страницы по подписчикам со всего vine.co',

                    'VINE.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_DAY.TITLE': 'Лучшие по подписчикам за ДЕНЬ',
                    'VINE.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_DAY.DESCRIPTION': 'Мы собрали для Вас пользователей которые набрали больше всего подписчиков за день',

                    'VINE.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_WEEK.TITLE': 'Лучшие по подписчикам за НЕДЕЛЮ',
                    'VINE.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_WEEK.DESCRIPTION': 'Мы собрали для Вас пользователей которые набрали больше всего подписчиков за неделю',

                    'VINE.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_MONTH.TITLE': 'Лучшие по подписчикам за МЕСЯЦ',
                    'VINE.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_MONTH.DESCRIPTION': 'Мы собрали для Вас пользователей которые набрали больше всего подписчиков за месяц',

                    'VINE.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_DAY.TITLE': 'Худшие по подписчикам за ДЕНЬ',
                    'VINE.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_DAY.DESCRIPTION': 'Мы собрали для Вас пользователей которые потеряли больше всего подписчиков за день',

                    'VINE.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_WEEK.TITLE': 'Худшие по подписчикам за НЕДЕЛЮ',
                    'VINE.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_WEEK.DESCRIPTION': 'Мы собрали для Вас пользователей которые потеряли больше всего подписчиков за неделю',

                    'VINE.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_MONTH.TITLE': 'Худшие по подписчикам за МЕСЯЦ',
                    'VINE.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_MONTH.DESCRIPTION': 'Мы собрали для Вас пользователей которые потеряли больше всего подписчиков за месяц',

                    'VINE.REPORTS.TOP_PAGES_BY_LIKES_PER_DAY.TITLE': 'Лучшие по Нравится за ДЕНЬ',
                    'VINE.REPORTS.TOP_PAGES_BY_LIKES_PER_DAY.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего Нравится за день',

                    'VINE.REPORTS.TOP_PAGES_BY_LIKES_PER_WEEK.TITLE': 'Лучшие по Нравится за НЕДЕЛЮ',
                    'VINE.REPORTS.TOP_PAGES_BY_LIKES_PER_WEEK.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего Нравится за неделю',

                    'VINE.REPORTS.TOP_PAGES_BY_LIKES_PER_MONTH.TITLE': 'Лучшие по Нравится за МЕСЯЦ',
                    'VINE.REPORTS.TOP_PAGES_BY_LIKES_PER_MONTH.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего Нравится за месяц',

                    'VINE.REPORTS.MORE_THAN_1000000_FOLLOWERS.TITLE': '>1.000.000 подписчиков',
                    'VINE.REPORTS.MORE_THAN_1000000_FOLLOWERS.DESCRIPTION': 'Мы собрали для Вас пользователей vine.co у которых больше 1 миллиона подписчиков! Самые интересные страницы со всего vine.co',

                    'VINE.REPORTS.TOP_POSTS_PER_DAY.TITLE': 'Лучшие вайны за ДЕНЬ',
                    'VINE.REPORTS.TOP_POSTS_PER_WEEK.TITLE': 'Лучшие вайны за НЕДЕЛЮ',
                    'VINE.REPORTS.TOP_POSTS_PER_MONTH.TITLE': 'Лучшие вайны за МЕСЯЦ',
                    'VINE.REPORTS.TOP_POSTS_PER_DAY.DESCRIPTION': 'Каждый день мы сканируем все вайны из vine.co и подбираем для Вас самое лучшее!',
                    'VINE.REPORTS.TOP_POSTS_PER_WEEK.DESCRIPTION': 'Каждый день мы сканируем все вайны из vine.co и подбираем для Вас самое лучшее!',
                    'VINE.REPORTS.TOP_POSTS_PER_MONTH.DESCRIPTION': 'Каждый день мы сканируем все вайны из vine.co и подбираем для Вас самое лучшее!',

                    'VINE.REPORTS.TOP_POSTS_IN.TITLE': 'лучшее у пользователя',
                    'VINE.REPORTS.TOP_POSTS_IN.DESCRIPTION': 'Мы выбрали лучшее у пользователя. Наслаждайтесь!',
                    'VINE.REPORTS.TOP_POSTS_IN.BEST_POSTS': 'лучшие вайны',

                    'VINE.REPORTS.TOP_POSTS_PER.TITLE': 'лучшие вайны за',

                    'VINE.REPORTS.DAY_CHANGES': 'изменений за ДЕНЬ',
                    'VINE.REPORTS.WEEK_CHANGES': 'изменений за НЕДЕЛЮ',
                    'VINE.REPORTS.MONTH_CHANGES': 'изменений за МЕСЯЦ',

                    'VINE.PAGE.TOP': 'Топ',
                    'VINE.PAGE.NAME': 'Название',
                    'VINE.PAGE.MEMBERS_COUNT': 'Кол-во подписчиков',
                    'VINE.PAGE.LIKES_COUNT': 'Кол-во нравится',
                    'VINE.PAGE.REPORTS': 'Списки',
                    'VINE.PAGE.HASHTAGS': 'Хештеги'
                });
        }]);
})();
