(function(){
    angular
        .module('vine')
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('vine-reports', {
                    url: '/vine/reports?page&pageSize&reportCategoryFilter',
                    templateUrl: 'components/reports/templates/reports.html',
                    controller: 'ReportsCtrl',
                    controllerAs: 'vm',
                    resolve: {
                        data: function (ReportsSvc, $stateParams) {
                            var parameters = {
                                page: $stateParams.page || 1,
                                pageSize: $stateParams.pageSize || 10
                            };

                            if ($stateParams.reportCategoryFilter){
                                parameters.reportCategoryFilter = $stateParams.reportCategoryFilter;
                            }

                            return ReportsSvc.query(parameters);
                        }
                    }
                })
                .state('vine-report', {
                    url: '/vine/reports/{id}?page&pageSize&periodsAgo',
                    templateUrl: 'components/reports/templates/report.html',
                    controller: 'ReportCtrl',
                    controllerAs: 'vm',
                    resolve: {
                        data: function (ReportsSvc, $stateParams) {

                            var parameters = {
                                page: $stateParams.page || 1,
                                pageSize: $stateParams.pageSize || 10,
                                id: $stateParams.id
                            };

                            if ($stateParams.periodsAgo){
                                parameters.periodsAgo = $stateParams.periodsAgo;
                            }

                            return ReportsSvc.get(parameters);
                        }
                    }
                })
        }]);
})();

