(function(){
    angular
        .module('coub')
        .config(['$translateProvider', function ($translateProvider) {

            $translateProvider
                .translations('ru-RU', {
                    'COUB.PAGES.PAGE_TITLE': 'Каналы участвующие в статистике (>1.000 подписчиков)',
                    'COUB.PAGES.PAGE_DESCRIPTION': 'Мы добавляем в списки и собираем статистику только на каналах где больше 1.000 подписчиеов. Если Вашего канала тут нет, наберите 1.000+ подписчиков и он автоматически добавится в этот список.',
                    'COUB.PAGES.NAME': 'Название',
                    'COUB.PAGES.CITY': 'Город',
                    'COUB.PAGES.COUNTRY': 'Страна',
                    'COUB.PAGES.FOLLOWERS_COUNT': 'Кол-во подписчиков',
                    'COUB.PAGES.LIKES_COUNT': 'Кол-во нравится',
                    'COUB.PAGES.REPORTS': 'Списки',
                    'COUB.PAGES.UPDATED_AT': 'Обновлено',
                    'COUB.PAGES.TOTAL': 'Кол-во',
                    'COUB.PAGES.DAY_CHANGES': 'Изименений за ДЕНЬ',
                    'COUB.PAGES.WEEK_CHANGES': 'Изименений за НЕДЕЛЮ',
                    'COUB.PAGES.MONTH_CHANGES': 'Изименений за МЕСЯЦ',
                    'COUB.PAGES.HASHTAGS': 'Хештеги'
                });
        }]);
})();
