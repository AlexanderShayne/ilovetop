(function(){
    angular
        .module('coub')
        .config(['$translateProvider', function ($translateProvider) {

            $translateProvider
                .translations('ru-RU', {

                    'COUB.REPORTS.TOP_POSTS_TITLE': 'Лучшие коубы за день/неделю/месяц',
                    'COUB.REPORTS.TOP_POSTS_DESCRIPTION': 'Только лучшее, наслаждайтесь!',

                    'COUB.REPORTS.TOP_PAGES_TITLE': 'Лучшие каналы',
                    'COUB.REPORTS.ANTI_TOP_PAGES_TITLE': 'Худшие каналы',

                    'COUB.REPORTS.TOP_POSTS_IN_TITLE': 'Лучшие коубы за все время на каналах',

                    'COUB.REPORTS.REPOSTS': 'Рекоубы',
                    'COUB.REPORTS.UPDATED': 'Обновлено',
                    'COUB.REPORTS.FOR': 'За',
                    'COUB.REPORTS.POSTS': 'коубов',
                    'COUB.REPORTS.PAGES': 'каналов',
                    'COUB.REPORTS.TOP': 'топ',
                    'COUB.REPORTS.ANTI_TOP': 'анти-топ',

                    'COUB.REPORTS.TOP_PAGES.TITLE': 'Лучшие каналы',
                    'COUB.REPORTS.TOP_PAGES.DESCRIPTION': 'Мы собрали для Вас лучшие каналы по подписчикам со всего coub.com',

                    'COUB.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_DAY.TITLE': 'Лучшие по подписчикам за ДЕНЬ',
                    'COUB.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_DAY.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего подписчиков за день',

                    'COUB.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_WEEK.TITLE': 'Лучшие по подписчикам за НЕДЕЛЮ',
                    'COUB.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_WEEK.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего подписчиков за неделю',

                    'COUB.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_MONTH.TITLE': 'Лучшие по подписчикам за МЕСЯЦ',
                    'COUB.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_MONTH.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего подписчиков за месяц',

                    'COUB.REPORTS.TOP_PAGES_BY_LIKES_PER_DAY.TITLE': 'Лучшие по Нравится за ДЕНЬ',
                    'COUB.REPORTS.TOP_PAGES_BY_LIKES_PER_DAY.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего Нравится за день',

                    'COUB.REPORTS.TOP_PAGES_BY_LIKES_PER_WEEK.TITLE': 'Лучшие по Нравится за НЕДЕЛЮ',
                    'COUB.REPORTS.TOP_PAGES_BY_LIKES_PER_WEEK.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего Нравится за неделю',

                    'COUB.REPORTS.TOP_PAGES_BY_LIKES_PER_MONTH.TITLE': 'Лучшие по Нравится за МЕСЯЦ',
                    'COUB.REPORTS.TOP_PAGES_BY_LIKES_PER_MONTH.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего Нравится за месяц',

                    'COUB.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_DAY.TITLE': 'Худшие по подписчикам за ДЕНЬ',
                    'COUB.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_DAY.DESCRIPTION': 'Мы собрали для Вас каналы которые потеряли больше всего подписчиков за день',

                    'COUB.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_WEEK.TITLE': 'Худшие по подписчикам за НЕДЕЛЮ',
                    'COUB.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_WEEK.DESCRIPTION': 'Мы собрали для Вас каналы которые потеряли больше всего подписчиков за неделю',

                    'COUB.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_MONTH.TITLE': 'Худшие по подписчикам за МЕСЯЦ',
                    'COUB.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_MONTH.DESCRIPTION': 'Мы собрали для Вас каналы которые потеряли больше всего подписчиков за месяц',

                    'COUB.REPORTS.MORE_THAN_1000000_FOLLOWERS.TITLE': '>1.000.000 подписчиков',
                    'COUB.REPORTS.MORE_THAN_1000000_FOLLOWERS.DESCRIPTION': 'Мы собрали для Вас каналы coub.com в которых больше 1 миллиона подписчиков! Самые интересные каналы со всего coub.com',

                    'COUB.REPORTS.TOP_POSTS_PER_DAY.TITLE': 'Лучшие коубы за ДЕНЬ',
                    'COUB.REPORTS.TOP_POSTS_PER_WEEK.TITLE': 'Лучшие коубы за НЕДЕЛЮ',
                    'COUB.REPORTS.TOP_POSTS_PER_MONTH.TITLE': 'Лучшие коубы за МЕСЯЦ',
                    'COUB.REPORTS.TOP_POSTS_PER_DAY.DESCRIPTION': 'Каждый день мы сканируем все коубы из coub.com и подбираем для Вас самое лучшее!',
                    'COUB.REPORTS.TOP_POSTS_PER_WEEK.DESCRIPTION': 'Каждый день мы сканируем все коубы из coub.com и подбираем для Вас самое лучшее!',
                    'COUB.REPORTS.TOP_POSTS_PER_MONTH.DESCRIPTION': 'Каждый день мы сканируем все коубы из coub.com и подбираем для Вас самое лучшее!',

                    'COUB.REPORTS.TOP_POSTS_IN.TITLE': 'лучшее на канале',
                    'COUB.REPORTS.TOP_POSTS_IN.DESCRIPTION': 'Мы выбрали лучшее на канале. Наслаждайтесь!',
                    'COUB.REPORTS.TOP_POSTS_IN.BEST_POSTS': 'лучшие коубы',

                    'COUB.REPORTS.TOP_POSTS_PER.TITLE': 'лучшие коубы за',

                    'COUB.REPORTS.DAY_CHANGES': 'изменений за ДЕНЬ',
                    'COUB.REPORTS.WEEK_CHANGES': 'изменений за НЕДЕЛЮ',
                    'COUB.REPORTS.MONTH_CHANGES': 'изменений за МЕСЯЦ',

                    'COUB.PAGE.TOP': 'Топ',
                    'COUB.PAGE.NAME': 'Название',
                    'COUB.PAGE.MEMBERS_COUNT': 'Кол-во подписчиков',
                    'COUB.PAGE.LIKES_COUNT': 'Кол-во нравится',
                    'COUB.PAGE.REPORTS': 'Списки',
                    'COUB.PAGE.HASHTAGS': 'Хештеги'
                });
        }]);
})();
