﻿(function(){
    angular
        .module('vk')
        .config(['$translateProvider', function ($translateProvider) {

            $translateProvider
                .translations('ru-RU', {
                    'COUB.MENU.REPORTS': 'Все списки',
                    'COUB.MENU.PAGES': 'Все каналы',
                    'COUB.MENU.TOP_POSTS': 'Топ коубы',
                    'COUB.MENU.TOP_PAGES': 'Топ каналы',
                    'COUB.MENU.ANTI_TOP_PAGES': 'Худшие каналы',
                    'COUB.MENU.TOP_POSTS_IN': 'Топ коубы на каналах'
                });
        }]);
})();

