(function(){
    class PostsSvc{
        constructor(ENVIRONMENT_CONSTANTS, $resource, currentSocialNetwork){
            this.ENVIRONMENT_CONSTANTS = ENVIRONMENT_CONSTANTS;
            this.$resource = $resource;
            this.currentSocialNetwork = currentSocialNetwork;
        }

        get serviceUri(){
            return this.ENVIRONMENT_CONSTANTS.SERVICES_URI + '/' + this.currentSocialNetwork.name + this.ENVIRONMENT_CONSTANTS.SERVICES.POSTS;
        }

        getHash(pageId, postId){
            return this._getResource()
                .get({id: pageId + '_' + postId})
                .$promise;
        }

        _getResource(){
            return this.$resource(this.serviceUri + '/:id/hash', { id: '@id' });
        }
    }

    angular
        .module('app')
        .service('PostsSvc', PostsSvc);
})();
