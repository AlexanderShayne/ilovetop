(function () {

    class ReportCtrl{
        constructor(data, $stateParams, $rootScope, $translate, $location, ReportsSvc, TokenStoreSvc){

            this.data = data;
            this.$stateParams = $stateParams;
            this.$rootScope = $rootScope;
            this.$translate = $translate;
            this.$location = $location;
            this.ReportsSvc = ReportsSvc;
            this.TokenStoreSvc = TokenStoreSvc;

            this.data.translateKey = this.getTranslateKey(data.key);

            this._setupPeriods();
            this._setMetaTags();
        }

        getTranslateKey(reportKey){

            if (reportKey.indexOf('top-posts-in') >= 0){
                return 'TOP_POSTS_IN';
            }

            return reportKey.replace(/-/g, '_').toUpperCase();
        }

        removePost(item){
            this
                .ReportsSvc
                .removePost(item.page.id, item.id)
                .then(() => {
                    var index = this.data.posts.indexOf(item);
                    this.data.posts.splice(index, 1);
                });
        }

        isUserLoggedIn(){
            return this.TokenStoreSvc.isUserLoggedIn();
        }

        _setupPeriods(){
            var current = +(this.$stateParams.periodsAgo || 0);

            this.periods = {
                current: current,
                prev: current - 1,
                next: current + 1,
                hasPrev: current > 0
            };
        }

        _setMetaTags(){

            var image = this.data.photo200;

            if (image.indexOf('http') < 0){
                image = this.$location.protocol() + "://" + this.$location.host() + image;
            }

            this.$rootScope.metatags =  {
                image: image
            };

            this.$translate('REPORTS.' + this.data.translateKey + '.TITLE')
                .then((translation) => {
                    var title = '';

                    if (this.data.translateKey === 'TOP_POSTS_IN'){
                        title = this.data.communities[0].name + ': ';
                    }

                    title += translation;

                    this.$rootScope.metatags.title = title;
                });

            this.$translate('REPORTS.' + this.data.translateKey + '.DESCRIPTION')
                .then((translation) => {

                    if (translation === 'REPORTS.' + this.data.translateKey + '.DESCRIPTION'){
                        translation = '';
                    }

                    this.$rootScope.metatags.description = translation;
                })
        }
    }

    angular
        .module('app')
        .controller('ReportCtrl', ReportCtrl);
})();