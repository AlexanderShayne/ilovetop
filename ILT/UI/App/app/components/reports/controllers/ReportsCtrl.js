(function(){
    class ReportsCtrl{
        constructor(data){
            this.data = data;

            this.sortedData = {};

            _.forEach(this.data.items, (report) => {
                this.sortedData[report.reportCategory] = this.sortedData[report.reportCategory] || [];
                this.sortedData[report.reportCategory].push(report);
            });
        }

        getTranslateKey(key){

            if (key.startsWith('top-posts-in')){
                return 'TOP_POSTS_IN';
            }

            return key.replace(/-/g, '_').toUpperCase();
        }
    }

    angular
        .module('app')
        .controller('ReportsCtrl', ReportsCtrl);
})();


