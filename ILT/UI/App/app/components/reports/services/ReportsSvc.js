(function(){
    class ReportsSvc{
        constructor(ENVIRONMENT_CONSTANTS, $resource, currentSocialNetwork){
            this.ENVIRONMENT_CONSTANTS = ENVIRONMENT_CONSTANTS;
            this.$resource = $resource;
            this.currentSocialNetwork = currentSocialNetwork;
        }

        get serviceUri(){
            return this.ENVIRONMENT_CONSTANTS.SERVICES_URI + '/' + this.currentSocialNetwork.name + this.ENVIRONMENT_CONSTANTS.SERVICES.REPORTS;
        }

        query(query){
            return this._getResource()
                .get(query)
                .$promise;
        }

        get(query){
            return this._getResource()
                .get(query)
                .$promise;
        }

        removePost(pageId, postId){
            return this._getResource()
                .removePost({
                    postId : pageId + '_' + postId
                })
                .$promise;
        }

        _getResource(){
            return this.$resource(
                this.serviceUri + '/:id',
                { id: '@id' },
                {
                    removePost: {
                        url: this.serviceUri + '/posts/:postId',
                        method: 'delete'
                    }
                }
            );
        }
    }

    angular
        .module('app')
        .service('ReportsSvc', ReportsSvc);
})();

