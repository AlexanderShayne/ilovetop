/*(function(){
    angular
        .module('vk')
        .directive('vkPost', function(PostsSvc){
            return {
                template: '<div id="vk_post_-{{post.page.id}}_{{post.id}}"></div>' +
            '<script type="text/javascript">' +
                '(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//vk.com/js/api/openapi.js?125"; fjs.parentNode.insertBefore(js, fjs); }(document, \'script\', \'vk_openapi_js\'));' +
            '(function() {' +
                'if (!window.VK || !VK.Widgets || !VK.Widgets.Post || !VK.Widgets.Post(\'vk_post-{{post.page.id}}_{{post.id}}\', -{{post.page.id}}, {{post.id}}, \'{{post.hash}}\', {redesign: 1, width: 665, base_domain: \'//new.vk.com\'})) setTimeout(arguments.callee, 50);' +
            '}());' +
            '</script>',
                link: function(scope, element, attrs){
                    PostsSvc
                        .getHash(scope.post.page.id, scope.post.id)
                        .then(function(result){
                            scope.post.hash = result.hash;
                        });
                }
            }
        });
})();*/

(function(){

    var updateScripts = function (scope, element, hash) {
        element.empty();
        var scriptTag = angular.element(
            document.createElement("script"));
        var source = getVkScript(scope.post.page.id, scope.post.id, hash);
        scriptTag.text(source);
        scriptTag.id =
            element.append(scriptTag);
    };

    var getVkScript = function(pageId, postId, hash){
        return '(function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//vk.com/js/api/openapi.js?125"; fjs.parentNode.insertBefore(js, fjs); }(document, \'script\', \'vk_openapi_js\'));  (function() {    if (!window.VK || !VK.Widgets || !VK.Widgets.Post || !VK.Widgets.Post("vk_post_-' + pageId + '_' + postId + '", -' + pageId + ', ' + postId +', \'' + hash +'\', {width: 695, base_domain: \'//new.vk.com\'})) setTimeout(arguments.callee, 50);  }());';
    };

    angular
        .module('vk')
        .directive('vkPost', function (PostsSvc) {
            return {
                restrict: 'A',
                link: function(scope, element) {
                    PostsSvc
                        .getHash(scope.post.page.id, scope.post.id)
                        .then(function(result){
                            updateScripts(scope, element, result.hash);
                        });
                }
            };
        });
})();