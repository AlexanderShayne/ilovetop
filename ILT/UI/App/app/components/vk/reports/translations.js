(function(){
    angular
        .module('vk')
        .config(['$translateProvider', function ($translateProvider) {

            $translateProvider
                .translations('ru-RU', {

                    'VK.REPORTS.TOP_POSTS_TITLE': 'Лучшие новости за день/неделю/месяц',
                    'VK.REPORTS.TOP_POSTS_DESCRIPTION': 'Только лучшее, наслаждайтесь!',

                    'VK.REPORTS.TOP_PAGES_TITLE': 'Лучшие страницы',
                    'VK.REPORTS.ANTI_TOP_PAGES_TITLE': 'Худшие страницы',

                    'VK.REPORTS.TOP_POSTS_IN_TITLE': 'Лучшие новости за все время на страницах',

                    'VK.REPORTS.REPOSTS': 'Репосты',
                    'VK.REPORTS.UPDATED': 'Обновлено',
                    'VK.REPORTS.FOR': 'За',
                    'VK.REPORTS.POSTS': 'новостей',
                    'VK.REPORTS.PAGES': 'страниц',
                    'VK.REPORTS.TOP': 'топ',
                    'VK.REPORTS.ANTI_TOP': 'анти-топ',

                    'VK.REPORTS.TOP_PAGES.TITLE': 'Лучшие страницы',
                    'VK.REPORTS.TOP_PAGES.DESCRIPTION': 'Мы собрали для Вас лучшие страницы по подписчикам со всего vk.com',

                    'VK.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_DAY.TITLE': 'Лучшие по подписчикам за ДЕНЬ',
                    'VK.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_DAY.DESCRIPTION': 'Мы собрали для Вас страницы которые набрали больше всего подписчиков за день',

                    'VK.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_WEEK.TITLE': 'Лучшие по подписчикам за НЕДЕЛЮ',
                    'VK.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_WEEK.DESCRIPTION': 'Мы собрали для Вас страницы которые набрали больше всего подписчиков за неделю',

                    'VK.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_MONTH.TITLE': 'Лучшие по подписчикам за МЕСЯЦ',
                    'VK.REPORTS.TOP_PAGES_BY_FOLLOWERS_PER_MONTH.DESCRIPTION': 'Мы собрали для Вас страницы которые набрали больше всего подписчиков за месяц',

                    'VK.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_DAY.TITLE': 'Худшие по подписчикам за ДЕНЬ',
                    'VK.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_DAY.DESCRIPTION': 'Мы собрали для Вас страницы которые потеряли больше всего подписчиков за день',

                    'VK.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_WEEK.TITLE': 'Худшие по подписчикам за НЕДЕЛЮ',
                    'VK.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_WEEK.DESCRIPTION': 'Мы собрали для Вас страницы которые потеряли больше всего подписчиков за неделю',

                    'VK.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_MONTH.TITLE': 'Худшие по подписчикам за МЕСЯЦ',
                    'VK.REPORTS.ANTI_TOP_PAGES_BY_FOLLOWERS_PER_MONTH.DESCRIPTION': 'Мы собрали для Вас страницы которые потеряли больше всего подписчиков за месяц',

                    'VK.REPORTS.TOP_PAGES_BY_LIKES_PER_DAY.TITLE': 'Лучшие по Нравится за ДЕНЬ',
                    'VK.REPORTS.TOP_PAGES_BY_LIKES_PER_DAY.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего Нравится за день',

                    'VK.REPORTS.TOP_PAGES_BY_LIKES_PER_WEEK.TITLE': 'Лучшие по Нравится за НЕДЕЛЮ',
                    'VK.REPORTS.TOP_PAGES_BY_LIKES_PER_WEEK.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего Нравится за неделю',

                    'VK.REPORTS.TOP_PAGES_BY_LIKES_PER_MONTH.TITLE': 'Лучшие по Нравится за МЕСЯЦ',
                    'VK.REPORTS.TOP_PAGES_BY_LIKES_PER_MONTH.DESCRIPTION': 'Мы собрали для Вас каналы которые набрали больше всего Нравится за месяц',

                    'VK.REPORTS.MORE_THAN_1000000_FOLLOWERS.TITLE': '>1.000.000 подписчиков',
                    'VK.REPORTS.MORE_THAN_1000000_FOLLOWERS.DESCRIPTION': 'Мы собрали для Вас страницы vk.com в которых больше 1 миллиона подписчиков! Самые интересные страницы со всего vk.com',

                    'VK.REPORTS.TOP_POSTS_PER_DAY.TITLE': 'Лучшие новости за ДЕНЬ',
                    'VK.REPORTS.TOP_POSTS_PER_WEEK.TITLE': 'Лучшие новости за НЕДЕЛЮ',
                    'VK.REPORTS.TOP_POSTS_PER_MONTH.TITLE': 'Лучшие новости за МЕСЯЦ',
                    'VK.REPORTS.TOP_POSTS_PER_DAY.DESCRIPTION': 'Каждый день мы сканируем все новости из vk.com и подбираем для Вас самое лучшее!',
                    'VK.REPORTS.TOP_POSTS_PER_WEEK.DESCRIPTION': 'Каждый день мы сканируем все новости из vk.com и подбираем для Вас самое лучшее!',
                    'VK.REPORTS.TOP_POSTS_PER_MONTH.DESCRIPTION': 'Каждый день мы сканируем все новости из vk.com и подбираем для Вас самое лучшее!',

                    'VK.REPORTS.TOP_POSTS_IN.TITLE': 'лучшее на странице',
                    'VK.REPORTS.TOP_POSTS_IN.DESCRIPTION': 'Мы выбрали лучшее на странице. Наслаждайтесь!',
                    'VK.REPORTS.TOP_POSTS_IN.BEST_POSTS': 'лучшие новости',

                    'VK.REPORTS.TOP_POSTS_PER.TITLE': 'лучшие новости за',

                    'VK.REPORTS.DAY_CHANGES': 'изменений за ДЕНЬ',
                    'VK.REPORTS.WEEK_CHANGES': 'изменений за НЕДЕЛЮ',
                    'VK.REPORTS.MONTH_CHANGES': 'изменений за МЕСЯЦ',

                    'VK.PAGE.TOP': 'Топ',
                    'VK.PAGE.NAME': 'Название',
                    'VK.PAGE.MEMBERS_COUNT': 'Кол-во подписчиков',
                    'VK.PAGE.LIKES_COUNT': 'Кол-во нравится',
                    'VK.PAGE.REPORTS': 'Списки',
                    'VK.PAGE.HASHTAGS': 'Хештеги'
                });
        }]);
})();
