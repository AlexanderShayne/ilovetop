(function(){
    angular
        .module('vk')
        .config(['$translateProvider', function ($translateProvider) {

            $translateProvider
                .translations('ru-RU', {
                    'VK.PAGES.PAGE_TITLE': 'Страницы участвующие в статистике (>100.000 подписчиков)',
                    'VK.PAGES.PAGE_DESCRIPTION': 'Мы добавляем в списки и собираем статистику только на страницах где больше 10.000 подписчиеов. Если Вашей страницы тут нет, наберите 10.000+ подписчиков и она автоматически добавится в этот список.',
                    'VK.PAGES.NAME': 'Название',
                    'VK.PAGES.CITY': 'Город',
                    'VK.PAGES.COUNTRY': 'Страна',
                    'VK.PAGES.FOLLOWERS_COUNT': 'Кол-во подписчиков',
                    'VK.PAGES.LIKES_COUNT': 'Кол-во нравится',
                    'VK.PAGES.REPORTS': 'Списки',
                    'VK.PAGES.UPDATED_AT': 'Обновлено',
                    'VK.PAGES.TOTAL': 'Кол-во',
                    'VK.PAGES.DAY_CHANGES': 'Изименений за ДЕНЬ',
                    'VK.PAGES.WEEK_CHANGES': 'Изименений за НЕДЕЛЮ',
                    'VK.PAGES.MONTH_CHANGES': 'Изименений за МЕСЯЦ',
                    'VK.PAGES.HASHTAGS': 'Хештеги'
                });
        }]);
})();
