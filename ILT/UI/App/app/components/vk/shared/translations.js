﻿(function(){
    angular
        .module('vk')
        .config(['$translateProvider', function ($translateProvider) {

            $translateProvider
                .translations('ru-RU', {
                    'VK.MENU.REPORTS': 'Все списки',
                    'VK.MENU.PAGES': 'Все страницы',
                    'VK.MENU.TOP_POSTS': 'Топ новости',
                    'VK.MENU.TOP_PAGES': 'Топ страницы',
                    'VK.MENU.ANTI_TOP_PAGES': 'Худшие страницы',
                    'VK.MENU.TOP_POSTS_IN': 'Топ новости на страницах'
                });
        }]);
})();

