(function(){
    angular
        .module('vk', [
            'ui.router',
            'ui.bootstrap',
            'pascalprecht.translate',
            'angularMoment',
            'metatags',
            'ilt.paging',
            'angular-loading-bar',
            'ngSanitize',
            '720kb.socialshare'
        ]);
})();

