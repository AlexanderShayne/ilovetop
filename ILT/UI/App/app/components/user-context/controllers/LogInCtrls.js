"use strict";

class LogInCtrl {
    constructor(UsersSvc) {
        this.UsersSvc = UsersSvc;
        this.submitInProgress = false;
    }

    onSubmit(form) {
        this.hideNotifications();
        if (!this._canSubmit(form)) return;

        this.inProgress = true;

        this.UsersSvc
            .login(this.credentials)
            .catch((err) => {
                if(err && err.data && err.data.error == "invalid_grant") {
                    this.showInvalidUserNameOrPassword = true;
                }

                this.showUnexpectedError = true;
                this.inProgress = false;
            })
            .finally(() => this.inProgress = false);
    }

    hideNotifications(){
        this.showInvalidUserNameOrPassword = this.showUnexpectedError = false;
    }

    isUserLoggedIn(){
        return this.UsersSvc.isUserLoggedIn();
    }

    logout(){
        this.UsersSvc.logout();
    }

    _canSubmit (form){
        return form.$valid && !form.$pending && !this.inProgress;
    }
}

angular
    .module('app')
    .controller('LogInCtrl', LogInCtrl);
