"use strict";

class LogInCompleteCtrl {
    constructor(TokenStoreSvc, $stateParams, $state) {

        TokenStoreSvc.setToken({
            access_token: $stateParams.access_token,
            refresh_token: $stateParams.refresh_token
        });

        $state.go('reports');
    }
}

angular
    .module('app')
    .controller('LogInCompleteCtrl', LogInCompleteCtrl);
