class SignUpCtrl {
    constructor(UsersSvc, $state) {
        this.UsersSvc = UsersSvc;
        this.$state = $state;
        this.submitInProgress = false;
    }

    onSubmit(form) {
        if (!this._canSubmit(form)) return;

        this.submitInProgress = true;

        this.UsersSvc
            .save(this.user)
            .then(() =>
                this.UsersSvc
                    .login({ email: this.user.email, password: this.user.newPassword })
                    .then(() => this.$state.go('home'))
            )
            .finally(() => this.submitInProgress = false);
    }

    _canSubmit (form){
        return form.$valid && !form.$pending && !this.submitInProgress;
    }
}

angular
    .module('app')
    .controller('SignUpCtrl', SignUpCtrl);
