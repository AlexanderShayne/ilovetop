
angular
    .module('app')
    .config(['$translateProvider', function ($translateProvider) {

        $translateProvider
            .translations('ru-RU', {
                'USER_CONTEXT.LOGIN': 'Войти',
                'USER_CONTEXT.LOGOUT': 'Выйти',
                'USER_CONTEXT.USERNAME': 'Имя',
                'USER_CONTEXT.PASSWORD': 'Пароль',
                'USER_CONTEXT.FAIL.REQUIRED_EMAIL': 'Введите имейл.',
                'USER_CONTEXT.FAIL.INVALID_EMAIL': 'Введен неправильный имейл.',
                'USER_CONTEXT.FAIL.REQUIRED_PASSWORD': 'Введите пароль.',
                'USER_CONTEXT.FAIL.MINLENGTH_PASSWORD': 'Пароль должен быть не менее 6 символов.',
                'USER_CONTEXT.FAIL.INVALID_USERNAME_OR_PASSWORD': 'Username or password are incorrect.',
                'USER_CONTEXT.FAIL.UNEXPECTED': 'Something went wrong.'
            });

    }]);