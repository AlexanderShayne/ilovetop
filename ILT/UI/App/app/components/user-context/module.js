'use strict';

angular
    .module('app')
    .config(['$stateProvider', function($stateProvider){
        $stateProvider
            .state('signup', {
                url: '/SignUp',
                templateUrl: 'components/user-context/templates/signup.html',
                controller: 'SignUpCtrl',
                controllerAs: 'vm'
            })
            .state('loginComplete', {
                url: '/loginComplete?access_token',
                controller: 'LogInCompleteCtrl'
            })
    }]);