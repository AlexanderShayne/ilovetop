﻿(function (){
    angular
        .module('app', [
            'ngAnimate',
            'ngResource',
            'ui.router',
            'ui.bootstrap',
            'LocalStorageModule',
            'pascalprecht.translate',
            'angular-loading-bar',
            'metatags',
            'ngSanitize',
            'vk',
            'coub',
            'vine'
        ])
        .config(function($sceDelegateProvider) {
            $sceDelegateProvider.resourceUrlWhitelist([
                // Allow same origin resource loads.
                'self',
                // Allow loading from our assets domain.  Notice the difference between * and **.
                'http://coub.com/**',
                'https://vine.co/**'
            ])});
})();

