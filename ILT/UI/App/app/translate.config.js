﻿angular
    .module('app')
    .config(['$translateProvider', '$provide', function ($translateProvider, $provide) {

        $translateProvider.preferredLanguage('ru-RU');

        $provide.decorator('$translate', [
            '$delegate', 'currentSocialNetwork',
            function translateDecorator($delegate, currentSocialNetwork) {

                var instant = $delegate.instant;

                function newInstant() {
                    var args = Array.prototype.slice.call(arguments);
                    args[0] = currentSocialNetwork.name.toUpperCase() + '.' + args[0];
                    return instant.apply($delegate, args);
                }

                $delegate.instant = newInstant;

                return $delegate;
            }
        ]);

    }]);
