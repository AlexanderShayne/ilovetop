﻿var system = require('system'),
    webpage = require('webpage');

var page = webpage.create(),
    testindex = 0,
    loadInProgress = false,
    requestIds = [],
    requestIds2 = [];

var settings = JSON.parse(system.args[1]);
settings.isDebug = true;

var credentials = JSON.parse(system.args[2]);

var inProgress = function () {
    if (loadInProgress) return true;

    var idx;

    for (idx in requestIds) {
        if (requestIds.hasOwnProperty(idx)) {
            if (requestIds[idx] != null) return true;
        }
    }

    for (idx in requestIds2) {
        if (requestIds2.hasOwnProperty(idx)) {
            if (requestIds2[idx] != null) return true;
        }
    }

    return false;
};

var log = function (msg) {
    if (settings.isDebug) {
        console.log(msg);
    }
};

var init = function (page) {
    page.viewportSize = {
        width: 1920,
        height: 1080
    };

    page.onConsoleMessage = function (msg) {
        //if (parsedDownloadToken) {
        //    console.log(parsedDownloadToken);
        //    phantom.exit();
        //}
        //
        log(msg);
    };

    page.onLoadStarted = function () {
        loadInProgress = true;
        log("load started");
    };

    page.onLoadFinished = function () {
        loadInProgress = false;
        log("load finished");
    };

    function clean(id) {

        setTimeout(function () {
            var idx = id;
            var index2 = requestIds2.indexOf(idx);

            if (index2 !== -1) {
                requestIds2[index2] = null;
            }
        }, 4000);
    }

    page.onResourceReceived = function (response) {
        var index = requestIds.indexOf(response.id);
        var index2 = requestIds2.indexOf(response.id);

        if (index !== -1) {
            requestIds[index] = null;
            clean(response.id);

            return;
        }

        if (index2 !== -1) {
            requestIds2[index2] = null;
        }

        log(requestIds);
        log(requestIds2);
        log('is in progress:' + inProgress());
    };

    page.onResourceRequested = function (request, networkRequest) {
        //if (request.url.indexOf('.png') > 0 || request.url.indexOf('.jpg') > 0 || request.url.indexOf('.gif') > 0 || request.url.indexOf('.css') > 0 || request.url.indexOf('.wott') > 0 || request.url.indexOf('.ico') > 0) {
        //    networkRequest.abort();
        //    return;
        //}

        if (requestIds.indexOf(request.id) === -1) {
            requestIds.push(request.id);
        }

        if (requestIds2.indexOf(request.id) === -1) {
            requestIds2.push(request.id);
        }
    };
};

init(page);

function evaluate(page, func) {
    var args = [].slice.call(arguments, 2);
    var fn = "function() { return (" + func.toString() + ").apply(this, " + JSON.stringify(args) + ");}";
    return page.evaluate(fn);
}

var steps = [
    //Open 
    function () {
        page.open(settings.permalink);
    },

    function () {
        loadInProgress = true;

        evaluate(page, function (credentials) {
            document.querySelector('#quick_email').value = credentials.login;
            document.querySelector('#quick_pass').value = credentials.password;
            document.querySelector('#quick_login_button').click();
        }, credentials);

        setTimeout(function () {
            loadInProgress = false;
        }, 3000);
    },

    function () {
        page.open(settings.permalink);
    },

    function () {
        page.evaluate(function () {
            document.querySelector('.reply_link').click();
        });
    },

    function () {
        evaluate(page, function (settings) {
            window.nativeClick = function (selector) {
                var targetNode = document.querySelector(selector);
                if (targetNode) {
                    triggerMouseEvent(targetNode, "focus");
                    triggerMouseEvent(targetNode, "mouseover");
                    triggerMouseEvent(targetNode, "mousedown");
                    triggerMouseEvent(targetNode, "mouseup");
                    triggerMouseEvent(targetNode, "click");
                    triggerMouseEvent(targetNode, "mouseleave");
                }
                else
                    console.log("*** Target node not found!");

                function triggerMouseEvent(node, eventType) {
                    var clickEvent = document.createEvent('MouseEvents');
                    clickEvent.initEvent(eventType, true, true);
                    node.dispatchEvent(clickEvent);
                }
            };

            nativeClick('#like_club_inp');
            nativeClick('#wddi-121306945_like_club_dd');
            document.querySelector('#like_share_text').value = settings.message + settings.hashtags;
            document.querySelector('#like_share_send').click();

        }, settings);
    },

    function () {
        phantom.exit();
    }
];

interval = setInterval(function () {
    if (!inProgress() && typeof steps[testindex] == "function") {
        var step = testindex + 1;
        log("step " + step);

        page.render('before' + step + '.png');

        steps[testindex]();

        page.render('after' + step + '.png');
        testindex++;
    }
    if (typeof steps[testindex] != "function" && !inProgress()) {
        log("test complete!");
        phantom.exit();
    }
}, 50);