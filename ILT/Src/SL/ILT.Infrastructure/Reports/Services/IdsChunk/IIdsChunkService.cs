﻿using System.Collections.Generic;

namespace ILT.Infrastructure.Reports.Services.IdsChunk
{
    public interface IIdsChunkService
    {
        IEnumerable<IEnumerable<long>> GetIdsChunk();
    }
}