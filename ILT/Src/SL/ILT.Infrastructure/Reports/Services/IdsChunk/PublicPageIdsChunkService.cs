﻿using System.Collections.Generic;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Dal.Query.Reports.QueryRepositories;

namespace ILT.Infrastructure.Reports.Services.IdsChunk
{
    public class PublicPageIdsChunkService : IIdsChunkService
    {
        private static class Consts
        {
            public const int Step = 500;
        }

        readonly ISystemReportsQueryRepository systemReportsQueryRepository;
        readonly SocialNetwork socialNetwork;

        public PublicPageIdsChunkService(ISystemReportsQueryRepository systemReportsQueryRepository, SocialNetwork socialNetwork)
        {
            this.systemReportsQueryRepository = systemReportsQueryRepository;
            this.socialNetwork = socialNetwork;
        }

        public IEnumerable<IEnumerable<long>> GetIdsChunk()
        {
            var total = systemReportsQueryRepository.GetPublicPagesTotal(socialNetwork);

            var totalPages = total/Consts.Step + (total%Consts.Step > 0 ? 1 : 0);

            for (int i = 0; i < totalPages; i++)
            {
                var ids = systemReportsQueryRepository.GetPublicPageIds(socialNetwork, i + 1, Consts.Step);

                yield return ids;
            }
        }
    }
}