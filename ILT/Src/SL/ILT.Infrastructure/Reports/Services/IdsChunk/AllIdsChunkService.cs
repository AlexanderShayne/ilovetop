﻿using System.Collections.Generic;
using System.Linq;

namespace ILT.Infrastructure.Reports.Services.IdsChunk
{
    public class AllIdsChunkService : IIdsChunkService
    {
        private static class Consts
        {
            public const int GroupsCount = 2700000;
            public const int Step = 500;
        }

        public IEnumerable<IEnumerable<long>> GetIdsChunk()
        {
            int current = 0 + 1;

            while (current <= Consts.GroupsCount)
            {
                IEnumerable<long> ids = Enumerable.Range(current, Consts.Step).Select(x => (long)x);

                yield return ids;
                current += Consts.Step;
            }
        }
    }
}