﻿using System.Collections.Generic;
using System.Linq;
using ILT.Domain.PublicPages;

namespace ILT.Infrastructure.Reports.Services.IdsChunk
{
    public class InMemoryIdsChunkService : IIdsChunkService
    {
        private static class Consts
        {
            public const int Step = 100;
        }

        readonly IEnumerable<Page> communities;

        public InMemoryIdsChunkService(IEnumerable<Page> pages)
        {
            this.communities = pages.OrderBy(x => x.Id);
        }

        public IEnumerable<IEnumerable<long>> GetIdsChunk()
        {
            var total = communities.Count();

            var totalPages = total / Consts.Step + (total % Consts.Step > 0 ? 1 : 0);

            for (int i = 0; i < totalPages; i++)
            {
                var ids = communities.Skip(Consts.Step*i).Take(Consts.Step).Select(x => x.Id);

                yield return ids;
            }
        }
    }
}