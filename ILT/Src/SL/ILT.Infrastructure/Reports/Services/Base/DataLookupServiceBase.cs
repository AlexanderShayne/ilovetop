using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Crosscutting.Infrastructure;
using ILT.Contracts.Infrastructure.Reports.Services;
using ILT.Domain.Reports;

namespace ILT.Infrastructure.Reports.Services.Base
{
    public abstract class DataLookupServiceBase<T, TChunkItem> : IDataLookupService<T>
    {
        private static class Consts
        {
            public const int ThreadCount = 4;
            public const int MaxConcurrentWorkersPerThread = 10;
            public const int MinChunkSize = 50;
        }

        protected IList<ReportBase<T>> ReportGenerators { get; } = new List<ReportBase<T>>();
        protected LookupType LookupType { get; private set; }

        public void AddReports(params ReportBase<T>[] reports)
        {
            foreach (var reportsGenerator in reports)
            {
                ReportGenerators.Add(reportsGenerator);
            }
        }

        public void Start(LookupType lookupType)
        {
            LookupType = lookupType;

            var threads = new Thread[Consts.ThreadCount];
            var i = 0;

            foreach (var chunk in GetItems().Chunk(ChunkSize))
            {
                threads[i++] = CreateThread(chunk);
            }

            foreach (var thread in threads)
            {
                thread?.Join();
            }
        }

        public ReportsList GetReports()
        {
            return new ReportsList(ReportGenerators);
        }

        #region Proteted Methods

        protected virtual void ProcessChunk(IEnumerable<TChunkItem> chunk)
        {
            var tasks = new List<Task>(ChunkSize);
            var enumerator = chunk.GetEnumerator();
            var chunkCount = chunk.Count();

            var count = chunkCount < Consts.MaxConcurrentWorkersPerThread ? chunkCount : Consts.MaxConcurrentWorkersPerThread;

            for (var i = 0; i < count; i++)
            {
                enumerator.MoveNext();
                tasks.Add(CreateTask(enumerator.Current));
            }

            while (enumerator.MoveNext())
            {
                var completedtask = Task.WhenAny(tasks).Result;

                tasks.Add(CreateTask(enumerator.Current));
                tasks.Remove(completedtask);
            }

            while (tasks.Count > 0)
            {
                var completedtask = Task.WhenAny(tasks).Result;

                tasks.Remove(completedtask);
            }
        }

        protected abstract IEnumerable<TChunkItem> GetItems();
        protected abstract void ProcessItem(TChunkItem item);

        #endregion


        #region Private Methods

        Thread CreateThread(IEnumerable<TChunkItem> chunk)
        {
            var thread = new Thread(() =>
            {
                var c = chunk;
                ProcessChunk(c);
            });

            thread.Start();
            return thread;
        }

        int �hunkSize;
        int ChunkSize
        {
            get
            {
                if (�hunkSize > 0) return �hunkSize;

                var count = GetItems().Count();

                if (count < Consts.MinChunkSize)
                {
                    �hunkSize = count;
                }
                else
                {
                    �hunkSize = count / Consts.ThreadCount + (count % Consts.ThreadCount > 0 ? 1 : 0);
                }

                return �hunkSize;
            }
        }

        Task CreateTask(TChunkItem item)
        {
            var task = new Task(() => ProcessItem(item));
            task.Start();
            return task;
        }

        #endregion
    }
}