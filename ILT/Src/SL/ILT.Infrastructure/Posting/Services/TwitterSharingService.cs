using ILT.Contracts.Domain.Posting;
using ILT.Contracts.Infrastructure.Posting.Services;
using ILT.Domain.Posting;
using LinqToTwitter;

namespace ILT.Infrastructure.Posting.Services
{
    public class TwitterSharingService : ISharingService
    {
        #region Public Methods

        public void Share(PostedElement postedElement, SharingType postingType)
        {
            var auth = new SingleUserAuthorizer
            {
                CredentialStore = new SingleUserInMemoryCredentialStore
                {
                    ConsumerKey = "iC0U0txzSeFQqTRtRKQLMcU5H",
                    ConsumerSecret = "LvCbQdCBzcOztxPU9nxPvGK6wW58UscJjMtsFJraf0jmGvMPlu",
                    AccessToken = "727986438806056960-ifHYiNMdqOvFaVZOXa12GoCJD7sUTYz",
                    AccessTokenSecret = "PiU6vm429mVpj2phEvJjGvTcXRSk3NIE0GokFxy5DTgGH"
                }
            };

            var twitterCtx = new TwitterContext(auth);

            var message = $"{postedElement.Message}{postedElement.Hashtags}\r\n{postedElement.Permalink}";

            var result = twitterCtx.TweetAsync(message).Result;
        }

        #endregion
    }
}