namespace ILT.Infrastructure.Posting.Services
{
    public class Credentials
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}