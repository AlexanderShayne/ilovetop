using System;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Domain.Posting;
using ILT.Domain.Posting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NReco.PhantomJS;
using ISharingService = ILT.Contracts.Infrastructure.Posting.Services.ISharingService;

namespace ILT.Infrastructure.Posting.Services
{
    public class VkSharingService : ISharingService
    {
        #region Public Methods

        public void Share(PostedElement postedElement, SharingType postingType)
        {
            var phantomJs = new PhantomJS();

            phantomJs.OutputReceived += (sender, e) => {
                                                           Console.WriteLine("PhantomJS output: {0}", e.Data);
            };

            var shareFileName =
                postingType == SharingType.Post
                    ? postedElement.SocialNetwork == SocialNetwork.Vk ? "vk-share-vk-post.js" : "vk-share-coub-vine-post.js"
                    : "vk-share-report.js";

            var path = shareFileName;
            var serrializedSettings = Serialize(postedElement);
            var credentials = Serialize(new VkCredentials
            {
                Login = "admin@ilovetop.net",
                Password = "ILOVESHEIN365"
            });

            phantomJs.Run(path, new[] { serrializedSettings, credentials });
        }

        #endregion

        #region Private Methods

        static string Serialize(object inputObject)
        {
            var json = JsonConvert.SerializeObject(inputObject, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });

            return json;
        }

        #endregion

        private class VkCredentials
        {
            public string Login { get; set; }
            public string Password { get; set; }
        }
    }
}