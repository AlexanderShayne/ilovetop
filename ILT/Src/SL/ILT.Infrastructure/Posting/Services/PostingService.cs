﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.ValueObjects.DateTimeRanges;
using ILT.Contracts.Data.Reports.Repositories;
using ILT.Contracts.Domain.Posting;
using ILT.Contracts.Infrastructure.Posting.Services;
using ILT.Domain.Posting;
using ILT.Domain.Reports;
using ISharingService = ILT.Contracts.Infrastructure.Posting.Services.ISharingService;

namespace ILT.Infrastructure.Posting.Services
{
    public class PostingService : IPostingService
    {
        readonly ISystemReportsRepository reportsRepository;
        readonly IDateTimeRangeFactory dateTimeRangeFactory;
        readonly IEnumerable<ISharingService> sharingServices;

        public PostingService(ISystemReportsRepository reportsRepository, IDateTimeRangeFactory dateTimeRangeFactory, IEnumerable<ISharingService> sharingServices)
        {
            this.reportsRepository = reportsRepository;
            this.dateTimeRangeFactory = dateTimeRangeFactory;
            this.sharingServices = sharingServices;
        }

        #region Public Methods

        public void PostNext(SharingType postingType)
        {
            var from = new DateTimeOffset(1, 1, 1, 3, 0, 0, 0, new TimeSpan(0, 3, 0, 0)).ToLocalTime().Hour;
            var to = new DateTimeOffset(1, 1, 1, 8, 0, 0, 0, new TimeSpan(0, 3, 0, 0)).ToLocalTime().Hour;

            if (DateTime.Now.Hour >= from && DateTime.Now.Hour <= to) return;

            var reportKey = GetPostingReportKey();

            //reportKey = "top-posts-per-week";

            var report = GetReport(reportKey);

            var postedElement = report?.GetNextNotPostedElement(postingType);
            if (postedElement == null) return;

            try
            {
                foreach (var sharingService in sharingServices)
                {
                    sharingService.Share(postedElement, postingType);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                report.MarkAsPosted(postingType, postedElement.Permalink);
                reportsRepository.UpdateIsPostedInfo(report);
            }
        }

        #endregion

        #region Private Methods

        string GetPostingReportKey()
        {
            var reportKey = "top-posts-per-day";

            if (DateTime.Today == dateTimeRangeFactory.Create(DateTimeRangeType.Week).End)
            {
                reportKey = "top-posts-per-week";
            }

            if (DateTime.Today == dateTimeRangeFactory.Create(DateTimeRangeType.Month).End)
            {
                reportKey = "top-posts-per-month";
            }

            return reportKey;
        }

        Report GetReport(string reportKey)
        {
            var vkReport = reportsRepository.GetReport(reportKey, SocialNetwork.Vk);
            var coubReport = reportsRepository.GetReport(reportKey, SocialNetwork.Coub);
            var vineReport = reportsRepository.GetReport(reportKey, SocialNetwork.Vine);

            var vkPostedCount = vkReport == null ? Int32.MaxValue : vkReport.ReportPosts.Any(x => !x.IsPosted) ? vkReport.ReportPosts.Count(x => x.IsPosted) : Int32.MaxValue;
            var coubPostedCount = coubReport == null ? Int32.MaxValue : coubReport.ReportPosts.Any(x => !x.IsPosted) ? coubReport.ReportPosts.Count(x => x.IsPosted) : Int32.MaxValue;
            var vinePostedCount = vineReport == null ? Int32.MaxValue : vineReport.ReportPosts.Any(x => !x.IsPosted) ? vineReport.ReportPosts.Count(x => x.IsPosted) : Int32.MaxValue;

            var min = Math.Min(Math.Min(vkPostedCount, coubPostedCount), vinePostedCount);

            if (vkPostedCount == min)
            {
                return vkReport;
            }

            if (coubPostedCount == min)
            {
                return coubReport;
            }

            if (vinePostedCount == min)
            {
                return vineReport;
            }

            return vkReport;
        }

        #endregion
    }
}
