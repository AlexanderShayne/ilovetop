using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Domain.Posting;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Services;
using ILT.Domain.Posting;
using ILT.Vine.Contacts.Infrastructure.VineApis.Services;
using ISharingService = ILT.Contracts.Infrastructure.Posting.Services.ISharingService;

namespace ILT.Infrastructure.Posting.Services
{
    public class CoubSharingService : ISharingService
    {
        #region Private Members

        readonly ICoubShare coubShareService;
        readonly long myCoubChannelId = 2687039;
        readonly IVineVideos vineVideos;

        #endregion

        public CoubSharingService(ICoubShare coubShareService, IVineVideos vineVideos)
        {
            this.coubShareService = coubShareService;
            this.vineVideos = vineVideos;
        }

        #region Public Methods

        public void Share(PostedElement postedElement, SharingType postingType)
        {
            if (postingType != SharingType.Post) return;

            if (postedElement.SocialNetwork == SocialNetwork.Coub)
            {
                coubShareService.Recoub(postedElement.Id, myCoubChannelId);
            }

            if (postedElement.SocialNetwork == SocialNetwork.Vine)
            {
                var videoUrl =
                    vineVideos
                        .GetVineVideo(postedElement.Id)
                        .VideoUrl;

                coubShareService.CreateCoub(videoUrl, postedElement.OriginalTitle, postedElement.Hashtags);
            }
        }

        #endregion

        #region Private Methods
        #endregion
    }
}