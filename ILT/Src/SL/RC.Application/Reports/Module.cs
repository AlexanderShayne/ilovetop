﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ILT.Application.Reports.Services;
using ILT.Contracts.Application.Reports.Services;

namespace ILT.Application.Reports
{
    public class Module : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                    Component
                        .For<IReportsService>()
                        .ImplementedBy<ReportsService>()
                        .LifeStyle
                        .PerWebRequest
                );
        }
    }
}
