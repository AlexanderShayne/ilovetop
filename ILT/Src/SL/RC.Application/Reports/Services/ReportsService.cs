﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Contracts.Dal;
using Contracts.Dal.DomainStack;
using Contracts.Domain.Entities.Model;
using Contracts.Services.Application;
using Contracts.Services.Application.QueryStack.Pagging.Models;
using Contracts.Services.Application.QueryStack.Pagging.Services;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Application.Reports.Models.View;
using ILT.Contracts.Application.Reports.Services;
using ILT.Contracts.Dal.Query.Reports.Dtos.View;
using ILT.Contracts.Dal.Query.Reports.QueryRepositories;
using ILT.Contracts.Data.Reports.Repositories;
using ILT.Contracts.Domain.Reports;

namespace ILT.Application.Reports.Services
{
    public class ReportsService : IReportsService
    {
        readonly IReportsQueryRepository queryRepository;
        readonly IReportsRepository reportsRepository;
        readonly IMapper mapper;
        readonly IPaggingService paggingService;
        readonly IUnitOfWork unitOfWork;
        readonly IGuard guard;

        public ReportsService(IReportsQueryRepository queryRepository, IMapper mapper, IPaggingService paggingService, IReportsRepository reportsRepository, IUnitOfWork unitOfWork, IGuard guard)
        {
            this.queryRepository = queryRepository;
            this.mapper = mapper;
            this.paggingService = paggingService;
            this.reportsRepository = reportsRepository;
            this.unitOfWork = unitOfWork;
            this.guard = guard;
        }

        #region Public Methods

        public PaggedResult<ReportSummary> Query(SocialNetwork socialNetwork, int page, int pageSize, ReportCategory? reportCategoryFilter)
        {
            page = paggingService.MormalizePage(page);
            pageSize = paggingService.NormalizePageSize(pageSize);
            
            IEnumerable<ReportSummaryDto> dtos = queryRepository.Query(socialNetwork, page, pageSize, reportCategoryFilter);
            IEnumerable<ReportSummary> ims = mapper.Map<IEnumerable<ReportSummary>>(dtos);
            long total = queryRepository.GetTotal(socialNetwork, reportCategoryFilter);

            var result = new PaggedResult<ReportSummary>(page, pageSize, ims, total);

            return result;
        }

        public ReportSummary Read(SocialNetwork socialNetwork, string key, int page, int pageSize, int periodsAgo)
        {
            page = paggingService.MormalizePage(page);
            pageSize = paggingService.NormalizePageSize(pageSize);

            ReportSummaryDto dto = queryRepository.Read(socialNetwork, key, page, pageSize, periodsAgo);
            if (dto == null) return null;

            var result = mapper.Map<ReportSummary>(dto);
            result.Page = page;
            result.PageSize = pageSize;

            return result;
        }

        public void Delete(SocialNetwork socialNetwork, long pageId, long postId)
        {
            var reports =
                reportsRepository
                    .ReadReportsByPostId(socialNetwork, pageId, postId)
                    .ToList();

            foreach (var report in reports)
            {
                var reportPostToDelete =
                report
                    .ReportPosts
                    .Single(x => x.PostId == postId && x.PageId == pageId);

                foreach (var reportPost in report.ReportPosts)
                {
                    if (reportPost.Top < reportPostToDelete.Top) continue;

                    reportPost.Top--;
                    reportPost.CrudState = CrudState.Modified;
                }

                reportPostToDelete.CrudState = CrudState.Deleted;

                guard.DomainIsValid(report);

                report.CrudState = CrudState.Modified;
                reportsRepository.PersistEntityGraph(report);
            }

            var result = unitOfWork.SaveAsync().Result;
        }

        #endregion
    }
}
