﻿using System;
using System.ComponentModel;
using AutoMapper;
using ILT.Contracts.Application.Hashtags.Models.Input;
using ILT.Contracts.Application.Hashtags.Models.View;
using ILT.Contracts.Application.Reports.Models.View;
using ILT.Contracts.Dal.Query.PublicPages.Dtos.View;
using ILT.Contracts.Dal.Query.Reports.Dtos.View;
using ILT.Domain.Hashtags;
using ILT.Domain.PublicPages;

namespace ILT.Application
{
    public static class AutoMapperConfig
    {
        public static MapperConfiguration GetConfig()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ReportSummaryDto, ReportSummary>()
                    .ForMember(x => x.ReportCategoryName, opt => opt.MapFrom(c => GetEnumDescription(c.ReportCategory)));
                cfg.CreateMap<PageSummaryDto, Contracts.Application.PublicPages.Models.View.PageSummary>();

                cfg.CreateMap<CreateHashtagDetails, Hashtag>();
                cfg.CreateMap<Hashtag, HashtagSummary>();

                cfg.CreateMap<Page, Contracts.Application.PublicPages.Models.View.PageSummary>();
            });

            return configuration;
        }

        public static string GetEnumDescription(Enum value)
        {
            if (value == null)
            {
                throw new NullReferenceException();
            }

            var fi = value.GetType().GetField(value.ToString());

            var attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }
    }
}
