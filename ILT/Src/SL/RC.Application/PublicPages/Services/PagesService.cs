﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Contracts.Dal;
using Contracts.Dal.DomainStack;
using Contracts.Services.Application;
using Contracts.Services.Application.QueryStack.Pagging.Models;
using Contracts.Services.Application.QueryStack.Pagging.Services;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Application.PublicPages.Models.View;
using ILT.Contracts.Application.PublicPages.Services;
using ILT.Contracts.Dal.Query.PublicPages.Dtos.View;
using ILT.Contracts.Dal.Query.PublicPages.QueryRepositories;
using ILT.Contracts.Data.Hashtags.Repositories;
using ILT.Contracts.Data.PublicPages.Repositories;

namespace ILT.Application.PublicPages.Services
{
    public class PagesService : IPagesService
    {
        readonly IPagesQueryRepository queryRepository;
        readonly IPagesRepository pagesRepository;
        readonly IHashtagsRepository hashtagsRepository;
        readonly IGuard guard;
        readonly IUnitOfWork unitOfWork;
        readonly IMapper mapper;
        readonly IPaggingService paggingService;

        public PagesService(IPagesQueryRepository queryRepository, IMapper mapper, IPagesRepository pagesRepository, IHashtagsRepository hashtagsRepository, IUnitOfWork unitOfWork, IGuard guard, IPaggingService paggingService)
        {
            this.queryRepository = queryRepository;
            this.mapper = mapper;
            this.pagesRepository = pagesRepository;
            this.hashtagsRepository = hashtagsRepository;
            this.unitOfWork = unitOfWork;
            this.guard = guard;
            this.paggingService = paggingService;
        }

        #region Public Methods

        public PaggedResult<PageSummary> Query(SocialNetwork socialNetwork, int page, int pageSize)
        {
            page = paggingService.MormalizePage(page);
            pageSize = paggingService.NormalizePageSize(pageSize);

            IEnumerable<PageSummaryDto> dtos = queryRepository.Query(socialNetwork, page, pageSize);
            IEnumerable<PageSummary> vms = mapper.Map<IEnumerable<PageSummary>>(dtos);
            long total = queryRepository.GetTotal(socialNetwork);

            var result = new PaggedResult<PageSummary>(page, pageSize, vms, total);

            return result;
        }

        public PageSummary AddHashtag(SocialNetwork socialNetwork, long pageId, Guid hashtagId)
        {
            var domain = pagesRepository.Read(socialNetwork, pageId);
            var hashtag = domain.Hashtags.FirstOrDefault(x => x.Id == hashtagId);

            if (hashtag == null)
            {
                hashtag = hashtagsRepository.ReadAsync(hashtagId).Result;
                guard.NotNull(hashtag);

                domain.Hashtags.Add(hashtag);
                guard.DomainIsValid(domain);

                unitOfWork.SaveAsync();
            }

            var result = mapper.Map<PageSummary>(domain);
            return result;
        }

        public PageSummary ExcludeFromReports(SocialNetwork socialNetwork, long pageId)
        {
            pagesRepository.ExcludeFromReports(socialNetwork, pageId);

            return null;

            //var domain = pagesRepository.Read(socialNetwork, pageId);
            //guard.NotNull(domain);
            //
            //domain.ExcludeFromReports = true;
            //guard.DomainIsValid(domain);
            //
            //unitOfWork.Save();
            //
            //var result = mapper.Map<PageSummary>(domain);
            //return result;
        }

        #endregion
    }
}
