﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ILT.Application.PublicPages.Services;
using ILT.Contracts.Application.PublicPages.Services;

namespace ILT.Application.PublicPages
{
    public class Module : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(

                    Component
                        .For<IPagesService>()
                        .ImplementedBy<PagesService>()
                        .LifeStyle
                        .PerWebRequest
                );
        }
    }
}
