﻿using System;
using AutoMapper;
using CacheManager.Core;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Contracts.Services.Application.QueryStack.Pagging.Services;
using Services.Application.Pagging.Services;

namespace ILT.Application
{
    public class Module : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IMapper>()
                    .UsingFactoryMethod(() => AutoMapperConfig.GetConfig().CreateMapper())
                    .LifeStyle
                    .Singleton,

                Component
                    .For<IPaggingService, PaggingService>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For(typeof(ICacheManager<>))
                    .UsingFactoryMethod((t, c, b) => CacheFactory.Build(b.GenericArguments[0], settings =>
                    {
                        settings
                            .WithSystemRuntimeCacheHandle()
                            .WithExpiration(ExpirationMode.Absolute, new TimeSpan(2, 0, 0));
                    }))
                    .LifeStyle
                    .Singleton
                );
        }
    }
}