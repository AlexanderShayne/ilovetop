﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using ILT.Application.Hashtags.Services;
using ILT.Contracts.Application.Hashtags.Services;

namespace ILT.Application.Hashtags
{
    public class Module : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(

                    Component
                        .For<IHashtagsService>()
                        .ImplementedBy<HashtagsService>()
                        .LifeStyle
                        .PerWebRequest
                );
        }
    }
}
