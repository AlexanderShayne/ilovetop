﻿using System;
using AutoMapper;
using Contracts.Dal;
using Contracts.Dal.DomainStack;
using Contracts.Services.Application;
using ILT.Contracts.Application.Hashtags.Models.Input;
using ILT.Contracts.Application.Hashtags.Models.View;
using ILT.Contracts.Application.Hashtags.Services;
using ILT.Contracts.Data.Hashtags.Repositories;
using ILT.Domain.Hashtags;
using Services.Application;

namespace ILT.Application.Hashtags.Services
{
    public class HashtagsService : CrudDomainBase<HashtagSummary, CreateHashtagDetails, CreateHashtagDetails, Hashtag>, IHashtagsService
    {
        #region ctor

        public HashtagsService(IHashtagsRepository repository, IGuard guard, IMapper mapper, IUnitOfWork unitOfWork) : base(repository, guard, mapper, unitOfWork)
        {
        }

        #endregion

        #region Public Methods

        public HashtagSummary AddReportPattern(Guid key, string reportPattern)
        {
            throw new NotImplementedException();
        }

        public HashtagSummary AddPage(Guid key, int pageId)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
