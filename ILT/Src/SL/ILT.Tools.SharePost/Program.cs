﻿using System.Collections.Generic;
using Crosscutting.Infrastructure.RetryInvocation;
using Dal.Configuration.Mappings;
using Domain.ValueObjects.DateTimeRanges;
using ILT.Contracts.Domain.Posting;
using ILT.Contracts.Infrastructure.Posting.Services;
using ILT.Coub.Infrastructure.CoubApis.Services;
using ILT.Data.Reports.Repositories;
using ILT.Infrastructure.Posting.Services;
using ILT.Vine.Infrustructure.VineApis.Services;

namespace ILT.Tools.SharePost
{
    class Program
    {
        static void Main(string[] args)
        {
            var ol = new MappingModule();

            var retry = new Retry();

            var service = new PostingService(new SystemReportsRepository(), new DateTimeRangeFactory(),
                new List<ISharingService>
                {
                    new VkSharingService(),
                    new TwitterSharingService(),
                    new CoubSharingService(
                        new CoubShare(retry, "2174ee91482aff0c173ffbee5874f7b851e3af50d8fbfd387a0ff17239131320"),
                        new VineVideos(retry))
                });

            service.PostNext(SharingType.Post);
        }
    }
}