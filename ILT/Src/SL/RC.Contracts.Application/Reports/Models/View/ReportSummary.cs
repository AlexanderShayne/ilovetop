﻿using ILT.Contracts.Dal.Query.Reports.Dtos.View;

namespace ILT.Contracts.Application.Reports.Models.View
{
    public class ReportSummary : ReportSummaryDto
    {
        public int? Page { get; set; }
        public int? PageSize { get; set; }
        public string ReportCategoryName { get; set; }
    }
}
