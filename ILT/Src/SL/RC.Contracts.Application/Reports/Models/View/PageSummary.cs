﻿namespace ILT.Contracts.Application.Reports.Models.View
{
    public class PageSummary
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ScreenName { get; set; }
        public long MembersCount { get; set; }
        public string Description { get; set; }

        public string City { get; set; }
        public string Country { get; set; }

        public string Photo50 { get; set; }
        public string Photo100 { get; set; }
        public string Photo200 { get; set; }
    }
}