﻿using Contracts.Services.Application.QueryStack.Pagging.Models;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Application.Reports.Models.View;
using ILT.Contracts.Domain.Reports;

namespace ILT.Contracts.Application.Reports.Services
{
    public interface IReportsService
    {
        PaggedResult<ReportSummary> Query(SocialNetwork socialNetwork, int page, int pageSize, ReportCategory? reportCategoryFilter);
        ReportSummary Read(SocialNetwork socialNetwork, string key, int page, int pageSize, int periodsAgo);
        void Delete(SocialNetwork socialNetwork, long pageId, long postId);
    }
}
