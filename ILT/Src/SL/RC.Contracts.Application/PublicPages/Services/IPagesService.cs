﻿using System;
using Contracts.Services.Application.QueryStack.Pagging.Models;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Application.PublicPages.Models.View;

namespace ILT.Contracts.Application.PublicPages.Services
{
    public interface IPagesService
    {
        PaggedResult<PageSummary> Query(SocialNetwork socialNetwork, int page, int pageSize);
        PageSummary AddHashtag(SocialNetwork socialNetwork, long pageId, Guid hashtagId);
        PageSummary ExcludeFromReports(SocialNetwork socialNetwork, long pageId);
    }
}
