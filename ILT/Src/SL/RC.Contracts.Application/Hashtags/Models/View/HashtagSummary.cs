﻿using System;
using ILT.Contracts.Application.Hashtags.Models.Input;

namespace ILT.Contracts.Application.Hashtags.Models.View
{
    public class HashtagSummary : CreateHashtagDetails
    {
        public Guid Id { get; set; }
    }
}
