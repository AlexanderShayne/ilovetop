﻿namespace ILT.Contracts.Application.Hashtags.Models.Input
{
    public class PatternDetails
    {
        public string Pattern { get; set; }
    }
}