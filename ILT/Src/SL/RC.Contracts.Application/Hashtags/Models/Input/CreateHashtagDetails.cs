﻿namespace ILT.Contracts.Application.Hashtags.Models.Input
{
    public class CreateHashtagDetails
    {
        public string Name { get; set; }
    }
}