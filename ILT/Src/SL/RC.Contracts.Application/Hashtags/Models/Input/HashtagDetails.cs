﻿namespace ILT.Contracts.Application.Hashtags.Models.Input
{
    public class HashtagDetails<T> : CreateHashtagDetails
    {
        public T OwnerKey { get; set; }
    }
}
