﻿using System;
using Contracts.Services.Application;
using ILT.Contracts.Application.Hashtags.Models.Input;
using ILT.Contracts.Application.Hashtags.Models.View;

namespace ILT.Contracts.Application.Hashtags.Services
{
    public interface IHashtagsService : IDelete<Guid>, ICreate<HashtagSummary, CreateHashtagDetails>
    {
        HashtagSummary AddReportPattern(Guid key, string reportPattern);
        HashtagSummary AddPage(Guid key, int pageId);
    }
}
