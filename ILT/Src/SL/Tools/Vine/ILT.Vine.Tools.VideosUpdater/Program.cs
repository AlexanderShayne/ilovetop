﻿using System;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Crosscutting.Infrastructure.RetryInvocation;
using Crosscutting.IoC;
using Dal.Configuration.Mappings;
using ILT.Contracts.Infrastructure.Reports.Services;
using ILT.Data.Reports.QueryRepositories;
using ILT.Data.Reports.Repositories;
using ILT.Domain.PublicPages;
using ILT.Domain.Reports.PageReports;
using ILT.Domain.Reports.PostReports;
using ILT.Infrastructure.Reports.Services.IdsChunk;
using ILT.Vine.Infrustructure.Reports.Services;
using ILT.Vine.Infrustructure.VineApis.Services;

namespace ILT.Vine.Tools.VideosUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            var ol = new MappingModule();
            ContainerHolder.Container = new FakeContainer();

            var repository = new SystemReportsRepository();
            var queryRepository = new SystemReportsQueryRepository();
            var retry = new Retry();
            var vineApi = new VineApi(new VineUsers(retry), new VineVideos(retry));

            IIdsChunkService idsChunkService = new PublicPageIdsChunkService(queryRepository, SocialNetwork.Vine);

            var groupService = new UpdateUsersService(vineApi, idsChunkService, repository);

            groupService.AddReports(
                    new MoreThan1000000FollowersReport(SocialNetwork.Vine),
                    new TopPagesReport(SocialNetwork.Vine),
                    new TopPagesByFollowersPerDayReport(SocialNetwork.Vine),
                    new TopPagesByFollowersPerWeekReport(SocialNetwork.Vine),
                    new TopPagesByFollowersPerMonthReport(SocialNetwork.Vine),
                    new AntiTopPagesByFollowersPerDayReport(SocialNetwork.Vine),
                    new AntiTopPagesByFollowersPerWeekReport(SocialNetwork.Vine),
                    new AntiTopPagesByFollowersPerMonthReport(SocialNetwork.Vine),
                    new TopPagesByLikesPerDayReport(SocialNetwork.Vine),
                    new TopPagesByLikesPerWeekReport(SocialNetwork.Vine),
                    new TopPagesByLikesPerMonthReport(SocialNetwork.Vine)
                );

            Console.WriteLine("groupService.Start()");

            groupService.Start(LookupType.Api);

            Console.WriteLine("groupService.End()");

            var reports = groupService.GetReports();

            var communities = reports.SelectMany(x => x.Pages).Distinct(new Page.EqualityComparer()).ToList();

            var postService = new UpdateVinesService(vineApi, new PublicPageIdsChunkService(queryRepository, SocialNetwork.Vine), repository);

            postService.AddReports(TopPostsInPageReport.Create(SocialNetwork.Vine, communities).ToArray());
            postService.AddReports(
                    new TopPostsPerDayReport(SocialNetwork.Vine),
                    new TopPostsPerWeekReport(SocialNetwork.Vine),
                    new TopPostsPerMonthReport(SocialNetwork.Vine),
                    new TopPostsPerYearReport(SocialNetwork.Vine),
                    new TopPostsPerAllTimeReport(SocialNetwork.Vine)
                );

            Console.WriteLine("postService.Start()");

            postService.Start(LookupType.Api);

            Console.WriteLine("postService.End()");

            var postReports = postService.GetReports();

            repository.LogicallyDeleteAllReports(SocialNetwork.Vine);
            repository.MergeReports(reports.Concat(postReports));
        }
    }
}
