﻿using System;
using Crosscutting.Infrastructure.RetryInvocation;
using Crosscutting.IoC;
using Dal.Configuration.Mappings;
using ILT.Contracts.Infrastructure.Reports.Services;
using ILT.Data.Reports.Repositories;
using ILT.Infrastructure.Reports.Services.IdsChunk;
using ILT.Vine.Infrustructure.Reports.Services;
using ILT.Vine.Infrustructure.VineApis.Services;

namespace ILT.Vine.Tools.UsersUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            var ol = new MappingModule();
            ContainerHolder.Container = new FakeContainer();

            var reportsRepository = new SystemReportsRepository();
            var retry = new Retry();

            var vineApi = new VineApi(new VineUsers(retry), new VineVideos(retry));

            var service = new UpdateUsersService(vineApi, null, reportsRepository);

            Console.WriteLine("Started");

            service.Start(LookupType.Api);

            Console.WriteLine("Ended");
        }
    }
}
