﻿using System;
using Contracts.Crosscutting.IoC;
using ILT.Data.Reports.QueryRepositories;

namespace ILT.Vine.Tools.UsersUpdater
{
    public class FakeContainer : IContainer
    {
        public T Resolve<T>() where T : class
        {
            return new SystemReportsQueryRepository() as T;
        }

        public T Resolve<T>(string name) where T : class
        {
            throw new NotImplementedException();
        }

        public T[] ResolveAll<T>() where T : class
        {
            throw new NotImplementedException();
        }
    }
}