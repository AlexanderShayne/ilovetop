﻿using System;
using Crosscutting.Infrastructure.RetryInvocation;
using Crosscutting.IoC;
using Dal.Configuration.Mappings;
using ILT.Contracts.Infrastructure.Reports.Services;
using ILT.Coub.Infrastructure.CoubApis.Services;
using ILT.Coub.Infrastructure.Reports.Services;
using ILT.Data.Reports.Repositories;
using ILT.Infrastructure.Reports.Services.IdsChunk;

namespace ILT.Coub.Tools.ChannelsUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            var ol = new MappingModule();
            ContainerHolder.Container = new FakeContainer();

            var reportsRepository = new SystemReportsRepository();
            var retry = new Retry();

            var coubApi = new CoubApi(new CoubChannels(retry), new CoubChannelCoubs(retry));

            var idsChunkService = new AllIdsChunkService();

            var service = new UpdateChannelsService(coubApi, idsChunkService, reportsRepository);

            Console.WriteLine("Started");

            service.Start(LookupType.Api);

            Console.WriteLine("Ended");
        }
    }
}
