﻿using System;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Crosscutting.Infrastructure.RetryInvocation;
using Crosscutting.IoC;
using Dal.Configuration.Mappings;
using ILT.Contracts.Infrastructure.Reports.Services;
using ILT.Coub.Infrastructure.CoubApis.Services;
using ILT.Coub.Infrastructure.Reports.Services;
using ILT.Data.Reports.QueryRepositories;
using ILT.Data.Reports.Repositories;
using ILT.Domain.PublicPages;
using ILT.Domain.Reports.PageReports;
using ILT.Domain.Reports.PostReports;
using ILT.Infrastructure.Reports.Services.IdsChunk;

namespace ILT.Coub.Tools.ReportsUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            SetupContainer();

            var repository = new SystemReportsRepository();

            var groupsService = CreateUpdateChannelsService();

            groupsService.AddReports(PageReportBase.CreateAllPageReports(SocialNetwork.Coub));

            Console.WriteLine("groupService.Start()");

            groupsService.Start(LookupType.Database);

            Console.WriteLine("groupService.End()");

            var reports = groupsService.GetReports();

            var pages = reports.SelectMany(x => x.Pages).Distinct(new Page.EqualityComparer()).ToList();

            //var repository = new SystemReportsRepository();
            var queryRepository = new SystemReportsQueryRepository();
            var retry = new Retry();
            var coubApi = new CoubApi(new CoubChannels(retry), new CoubChannelCoubs(retry));

            var postService = new UpdateCoubsService(coubApi, new InMemoryIdsChunkService(pages.Skip(20).Take(10)), repository);
            //return postsService;

            //var postService = CreateUpdateCoubsService();

            postService.AddReports(TopPostsReport.CreateAllPostReports(SocialNetwork.Coub, pages.Skip(20).Take(10)));

            Console.WriteLine("postService.Start()");

            postService.Start(LookupType.Api);

            Console.WriteLine("postService.End()");

            var postReports = postService.GetReports();

            repository.LogicallyDeleteAllReports(SocialNetwork.Coub);
            repository.MergeReports(reports.Concat(postReports));
        }

        #region Private Methods

        static void SetupContainer()
        {
            var ol = new MappingModule();
            ContainerHolder.Container = new FakeContainer();
        }

        static UpdateChannelsService CreateUpdateChannelsService()
        {
            var repository = new SystemReportsRepository();
            var queryRepository = new SystemReportsQueryRepository();
            var retry = new Retry();
            var coubApi = new CoubApi(new CoubChannels(retry), new CoubChannelCoubs(retry));

            IIdsChunkService idsChunkService = new PublicPageIdsChunkService(queryRepository, SocialNetwork.Coub);

            var groupsService = new UpdateChannelsService(coubApi, idsChunkService, repository);
            return groupsService;
        }

        static UpdateCoubsService CreateUpdateCoubsService()
        {
            var repository = new SystemReportsRepository();
            var queryRepository = new SystemReportsQueryRepository();
            var retry = new Retry();
            var coubApi = new CoubApi(new CoubChannels(retry), new CoubChannelCoubs(retry));

            var postsService = new UpdateCoubsService(coubApi, new PublicPageIdsChunkService(queryRepository, SocialNetwork.Coub), repository);
            return postsService;
        }

        #endregion
    }
}
