﻿using System;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Crosscutting.Infrastructure.RetryInvocation;
using Crosscutting.IoC;
using Dal.Configuration.Mappings;
using ILT.Contracts.Infrastructure.Reports.Services;
using ILT.Data.Reports.QueryRepositories;
using ILT.Data.Reports.Repositories;
using ILT.Domain.PublicPages;
using ILT.Domain.Reports.PageReports;
using ILT.Domain.Reports.PostReports;
using ILT.Infrastructure.Reports.Services.IdsChunk;
using ILT.Vk.Contracts.Infrastructure.VkApis.Services;
using ILT.Vk.Infrastructure.Reports.Services;
using ILT.Vk.Infrastructure.VkApis.Services;

namespace ILT.Vk.Tools.ReportsUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            var ol = new MappingModule();
            ContainerHolder.Container = new FakeContainer();

            var repository = new SystemReportsRepository();
            var queryRepository = new SystemReportsQueryRepository();
            var vkApi = CreateVkApi();

            IIdsChunkService idsChunkService = new PublicPageIdsChunkService(queryRepository, SocialNetwork.Vk);

            var groupService = new UpdateGroupsService(vkApi, idsChunkService, repository);

            groupService.AddReports(
                    new MoreThan1000000FollowersReport(SocialNetwork.Vk),
                    new TopPagesReport(SocialNetwork.Vk),
                    new TopPagesByFollowersPerDayReport(SocialNetwork.Vk),
                    new TopPagesByFollowersPerWeekReport(SocialNetwork.Vk),
                    new TopPagesByFollowersPerMonthReport(SocialNetwork.Vk),
                    new AntiTopPagesByFollowersPerDayReport(SocialNetwork.Vk),
                    new AntiTopPagesByFollowersPerWeekReport(SocialNetwork.Vk),
                    new AntiTopPagesByFollowersPerMonthReport(SocialNetwork.Vk)
                );

            Console.WriteLine("groupService.Start()");

            groupService.Start(LookupType.Api);

            Console.WriteLine("groupService.End()");

            var reports = groupService.GetReports();

            var communities = reports.SelectMany(x => x.Pages).Distinct(new Page.EqualityComparer()).ToList();

            var postService = new UpdatePostsService(vkApi, repository, new PublicPageIdsChunkService(queryRepository, SocialNetwork.Vk));

            postService.AddReports(TopPostsInPageReport.Create(SocialNetwork.Vk, communities).ToArray());
            postService.AddReports(
                    new TopPostsPerDayReport(SocialNetwork.Vk),
                    new TopPostsPerWeekReport(SocialNetwork.Vk),
                    new TopPostsPerMonthReport(SocialNetwork.Vk),
                    new TopPostsPerYearReport(SocialNetwork.Vk),
                    new TopPostsPerAllTimeReport(SocialNetwork.Vk)
                );

            Console.WriteLine("postService.Start()");

            postService.Start(LookupType.Api);

            Console.WriteLine("postService.End()");

            var postReports = postService.GetReports();

            idsChunkService = new InMemoryIdsChunkService(communities);

            groupService = new UpdateGroupsService(vkApi, idsChunkService, repository);
            groupService.AddReports(
                    new TopPagesByLikesPerDayReport(SocialNetwork.Vk),
                    new TopPagesByLikesPerWeekReport(SocialNetwork.Vk),
                    new TopPagesByLikesPerMonthReport(SocialNetwork.Vk)
                );

            groupService.Start(LookupType.Database);

            var reports2 = groupService.GetReports();

            repository.LogicallyDeleteAllReports(SocialNetwork.Vk);
            repository.MergeReports(reports.Concat(postReports).Concat(reports2));
        }

        public static IVkApi CreateVkApi()
        {
            var retry = new Retry();

            var vkApi = new VkApi(new VkGroups(retry), new VkWall(retry));

            return vkApi;
        }
    }
}
