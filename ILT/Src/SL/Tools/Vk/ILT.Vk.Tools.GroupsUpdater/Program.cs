﻿using System;
using Crosscutting.Infrastructure.RetryInvocation;
using Crosscutting.IoC;
using Dal.Configuration.Mappings;
using ILT.Contracts.Infrastructure.Reports.Services;
using ILT.Data.Reports.Repositories;
using ILT.Infrastructure.Reports.Services.IdsChunk;
using ILT.Vk.Infrastructure.Reports.Services;
using ILT.Vk.Infrastructure.VkApis.Services;

namespace ILT.Vk.Tools.GroupsUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            var ol = new MappingModule();
            ContainerHolder.Container = new FakeContainer();

            var reportsRepository = new SystemReportsRepository();
            var retry = new Retry();

            var vkApi = new VkApi(new VkGroups(retry), new VkWall(retry));

            var idsChunkService = new AllIdsChunkService();

            var service = new UpdateGroupsService(vkApi, idsChunkService, reportsRepository);

            Console.WriteLine("Started");

            service.Start(LookupType.Api);

            Console.WriteLine("Ended");
        }
    }
}
