﻿using System;
using System.IO;
using System.Net;
using Contracts.Services.Infrastructure.SocialNetworks;
using Crosscutting.Infrastructure;
using Crosscutting.Infrastructure.RetryInvocation;
using ILT.Coub.Infrastructure.CoubApis.Services;
using ILT.Data.Reports.Repositories;
using ILT.Vine.Infrustructure.VineApis.Services;

namespace ILT.Tools.VideosDownloader
{
    class Program
    {
        static void Main(string[] args)
        {
            Validate(args);

            var reportKey = args[0];
            SocialNetwork socialNetwork;

            Enum.TryParse(args[1], true, out socialNetwork);

            var repo = new SystemReportsRepository();
            var report = repo.GetReport(reportKey, socialNetwork);


            if (socialNetwork == SocialNetwork.Vine)
            {
                var service = new VineVideos(new Retry());

                foreach (var reportPost in report.ReportPosts)
                {
                    var fileName = GetVideoFileName(reportKey, reportPost.Top);
                    var videoUrl =
                        service
                            .GetVineVideo(reportPost.PostId)
                            .VideoUrl;

                    using (var webClient = new WebClient())
                    {
                        webClient.DownloadFile(videoUrl, fileName);
                    }
                }
            }

            if (socialNetwork == SocialNetwork.Coub)
            {
                var service = new CoubChannelCoubs(new Retry());

                foreach (var reportPost in report.ReportPosts)
                {
                    var videoFilePath = GetVideoFileName(reportKey, reportPost.Top);
                    var audioFilePath = GetAudioFileName(reportKey, reportPost.Top);

                    var tokenIndex = reportPost.Post.Permalink.LastIndexOf('/');

                    string token = reportPost.Post.Permalink.Substring(tokenIndex + 1);

                    var coub = service.GetCoub(token);

                    var videoUrl =
                        coub
                            .FileVersions
                            ?.Web
                            ?.Template.Replace("%{type}", "mp4").Replace("%{version}", "big");

                    if (videoUrl == null) continue;

                    string audioVersion;

                    if (coub.AudioVersions.Versions == null)
                    {
                        audioVersion = null;
                    }
                    else if (coub.AudioVersions.Versions.Contains("high"))
                    {
                        audioVersion = "high";
                    }
                    else if(coub.AudioVersions.Versions.Contains("mid"))
                    {
                        audioVersion = "mid";
                    }
                    else
                    {
                        audioVersion = "low";
                    }

                    var audioUrl =
                        coub
                            .AudioVersions?
                            .Template?.Replace("%{version}", audioVersion);

                    using (var webClient = new WebClient())
                    {
                        try
                        {
                            webClient.DownloadFile(videoUrl, videoFilePath);
                            if (!String.IsNullOrEmpty(audioUrl))
                            {
                                webClient.DownloadFile(audioUrl, audioFilePath);
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }

                    }
                }
            }
        }

        static void Validate(string[] args)
        {
            if (args.IsNullOrEmpty() || args.Length != 2)
            {
                throw new ArgumentException("Provide ReportKey and SocialNetworkName.");
            }

            SocialNetwork socialNetwork;

            if (!Enum.TryParse(args[1], true, out socialNetwork))
            {
                throw new ArgumentException("SocialNetwork is not correct.");
            }
        }

        static string GetVideoFileName(string reportKey, int top)
        {
            CreateFolders(top);
            return $@"C:\Users\Dell XPS 15\iCloudDrive\{top}\{reportKey}.mp4";
        }

        static string GetAudioFileName(string reportKey, int top)
        {
            CreateFolders(top);
            return $@"C:\Users\Dell XPS 15\iCloudDrive\{top}\{reportKey}.mp3";
        }

        static void CreateFolders(int top)
        {
            if (!Directory.Exists(@"C:\Users\Dell XPS 15\iCloudDrive"))
            {
                Directory.CreateDirectory(@"C:\Users\Dell XPS 15\iCloudDrive");
            }

            if (!Directory.Exists($@"C:\Users\Dell XPS 15\iCloudDrive\{top}"))
            {
                Directory.CreateDirectory($@"C:\Users\Dell XPS 15\iCloudDrive\{top}");
            }
        }
    }
}
