using ILT.Contracts.Domain.Posting;
using ILT.Domain.Posting;

namespace ILT.Contracts.Infrastructure.Posting.Services
{
    public interface ISharingService
    {
        void Share(PostedElement postedElement, SharingType postingType);
    }
}