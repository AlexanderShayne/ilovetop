﻿using ILT.Contracts.Domain.Posting;

namespace ILT.Contracts.Infrastructure.Posting.Services
{
    public interface IPostingService
    {
        void PostNext(SharingType postingType);
    }
}
