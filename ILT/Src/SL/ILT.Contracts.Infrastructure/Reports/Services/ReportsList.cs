﻿using System.Collections.Generic;
using ILT.Domain.Reports;

namespace ILT.Contracts.Infrastructure.Reports.Services
{
    public class ReportsList : List<Report>
    {
        public ReportsList(IEnumerable<Report> reports) : base(reports)
        {
        }
    }
}