﻿using ILT.Domain.Reports;

namespace ILT.Contracts.Infrastructure.Reports.Services
{
    public interface IDataLookupService<TData>
    {
        void AddReports(params ReportBase<TData>[] reports);
        void Start(LookupType lookupType);
        ReportsList GetReports();
    }
}
