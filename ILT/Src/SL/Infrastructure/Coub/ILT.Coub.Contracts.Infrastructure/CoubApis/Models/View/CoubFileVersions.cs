using System.Runtime.Serialization;

namespace ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View
{
    [DataContract]
    public class CoubFileVersions
    {
        [DataMember(Name = "web")]
        public CoubVersions Web { get; set; }
    }
}