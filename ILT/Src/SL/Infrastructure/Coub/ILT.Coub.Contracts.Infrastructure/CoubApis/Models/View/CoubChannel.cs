﻿using System.Runtime.Serialization;

namespace ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View
{
    [DataContract]
    public class CoubChannel
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "permalink")]
        public string Permalink { get; set; }

        [DataMember(Name = "followers_count")]
        public int FollowersCount { get; set; }

        [DataMember(Name = "likes_count")]
        public int LikesCount { get; set; }

        [DataMember(Name = "avatar_versions")]
        public CoubVersions AvatarVersions { get; set; }
    }
}
