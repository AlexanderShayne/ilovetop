﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View
{
    [DataContract]
    public class CoubVersions
    {
        [DataMember(Name = "template")]
        public string Template { get; set; }

        [DataMember(Name = "versions")]
        public List<string> Versions { get; set; }

        [DataMember(Name = "types")]
        public List<string> Types { get; set; }
    }
}