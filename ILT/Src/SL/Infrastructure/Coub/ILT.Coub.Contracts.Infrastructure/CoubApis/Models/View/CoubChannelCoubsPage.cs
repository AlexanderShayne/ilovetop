using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View
{
    [DataContract]
    public class CoubChannelCoubsPage
    {
        [DataMember(Name = "coubs")]
        public List<CoubCoub> Coubs { get; set; }

        [DataMember(Name = "total_pages")]
        public int TotalPages { get; set; }
    }
}