﻿using System;
using System.Runtime.Serialization;

namespace ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View
{
    [DataContract]
    public class CoubCoub
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "permalink")]
        public string Permalink { get; set; }

        [DataMember(Name = "created_at")]
        public DateTime CreatedAt { get; set; }

        [DataMember(Name = "updated_at")]
        public DateTime UpdatedAt { get; set; }

        [DataMember(Name = "likes_count")]
        public int LikesCount { get; set; }

        [DataMember(Name = "views_count")]
        public int ViewsCount { get; set; }

        [DataMember(Name = "recoubs_count")]
        public int RecoubsCount { get; set; }

        [DataMember(Name = "comments_count")]
        public int CommentsCount { get; set; }

        [DataMember(Name = "image_versions")]
        public CoubVersions ImageVersions { get; set; }

        [DataMember(Name = "file_versions")]
        public CoubFileVersions FileVersions { get; set; }

        [DataMember(Name = "audio_versions")]
        public CoubVersions AudioVersions { get; set; }
    }
}
