﻿using System.Collections.Generic;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View;

namespace ILT.Coub.Contracts.Infrastructure.CoubApis.Services
{
    public interface ICoubChannels
    {
        IEnumerable<CoubChannel> GetChannels(IEnumerable<long> ids);
    }
}
