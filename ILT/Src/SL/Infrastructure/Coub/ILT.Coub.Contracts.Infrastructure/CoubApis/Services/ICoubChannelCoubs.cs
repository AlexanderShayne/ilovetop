﻿using ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View;

namespace ILT.Coub.Contracts.Infrastructure.CoubApis.Services
{
    public interface ICoubChannelCoubs
    {
        CoubChannelCoubsPage GetCoubs(long channelId, int page, int pageSize);
        CoubCoub GetCoub(long id);
        CoubCoub GetCoub(string key);
    }
}