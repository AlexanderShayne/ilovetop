namespace ILT.Coub.Contracts.Infrastructure.CoubApis.Services
{
    public interface ICoubApi
    {
        ICoubChannels Channels { get; }
        ICoubChannelCoubs ChannelCoubs { get; }
    }
}