namespace ILT.Coub.Contracts.Infrastructure.CoubApis.Services
{
    public interface ICoubShare
    {
        void Recoub(long recoubToId, long channelId);
        void CreateCoub(string videoUrl, string title, string hashtags);
    }
}