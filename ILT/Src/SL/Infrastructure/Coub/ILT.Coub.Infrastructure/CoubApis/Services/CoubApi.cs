using ILT.Coub.Contracts.Infrastructure.CoubApis.Services;

namespace ILT.Coub.Infrastructure.CoubApis.Services
{
    public class CoubApi : ICoubApi
    {
        public CoubApi(ICoubChannels coubChannels, ICoubChannelCoubs coubChannelCoubs)
        {
            Channels = coubChannels;
            ChannelCoubs = coubChannelCoubs;
        }

        #region Public Methods

        public ICoubChannels Channels { get; }
        public ICoubChannelCoubs ChannelCoubs { get; }

        #endregion
    }
}