using System.Linq;
using System.Net;
using Contracts.Crosscutting.RetryInvocation;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Services;
using RestSharp;

namespace ILT.Coub.Infrastructure.CoubApis.Services
{
    public class CoubChannelCoubs : ICoubChannelCoubs
    {
        private static class Consts
        {
            public static class Method
            {
                public const string TimelineChannel = "v2/timeline/channel";
                public const string Coubs = "v2/coubs";
                public const string CoubsV1 = "v1/coubs";
            }

            public static class Param
            {
                //public const string CoubsType = "coubs_type";
                public const string Page = "page";
                public const string PerPage = "per_page";
            }
        }

        readonly RestClient restClient;
        readonly IRetry retry;

        public CoubChannelCoubs(IRetry retry)
        {
            this.retry = retry;
            restClient = new RestClient(CoubConsts.Url);
        }

        #region Public Methods

        public CoubChannelCoubsPage GetCoubs(long channelId, int page, int pageSize)
        {
            var request = new RestRequest($"{Consts.Method.TimelineChannel}/{channelId}", Method.GET);
            AddParametersToGetPage(request, page, pageSize);

            var response = retry.SafeCall(() => restClient.Execute<CoubChannelCoubsPage>(request));

            if (response.StatusCode != HttpStatusCode.OK || response.Data == null)
            {
                return new CoubChannelCoubsPage
                {
                    Coubs = Enumerable.Empty<CoubCoub>().ToList()
                };
            }

            return response.Data;
        }

        public CoubCoub GetCoub(long id)
        {
            return GetCoubV2V1(id);
        }

        public CoubCoub GetCoub(string key)
        {
            return GetCoubV2V1(key);
        }

        #endregion

        #region Private Methods

        void AddParametersToGetPage(RestRequest request, int page, int pageSize)
        {
            //request.AddParameter(Consts.Param.CoubsType, "simples");
            request.AddParameter(Consts.Param.Page, page);
            request.AddParameter(Consts.Param.PerPage, pageSize);
        }

        CoubCoub GetCoubV2V1(object id)
        {
            var request = new RestRequest($"{Consts.Method.Coubs}/{id}", Method.GET);
            var response = retry.SafeCall(() => restClient.Execute<CoubCoub>(request));

            if (response.StatusCode != HttpStatusCode.OK)
            {
                request = new RestRequest($"{Consts.Method.CoubsV1}/{id}", Method.GET);
                response = retry.SafeCall(() => restClient.Execute<CoubCoub>(request));
            }

            return response.Data;
        }

        #endregion
    }
}