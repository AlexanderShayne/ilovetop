using System;
using System.IO;
using System.Linq;
using System.Net;
using Contracts.Crosscutting.RetryInvocation;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Services;
using RestSharp;

namespace ILT.Coub.Infrastructure.CoubApis.Services
{
    public class CoubShare : ICoubShare
    {
        private static class Consts
        {
            public static class Method
            {
                public const string Recoubs = "v2/recoubs";
                public const string CoubsInitUpload = "v2/coubs/init_upload";
                public const string CoubsUploadVideo = "v2/coubs/{0}/upload_video";
                public const string CoubsUploadAudio = "v2/coubs/{0}/upload_audio";
                public const string CoubsFinalizeUpload = "v2/coubs/{0}/finalize_upload";
            }

            public static class Param
            {
                public const string RecoubTo = "recoub_to_id";
                public const string ChannelId = "channel_id";
                public const string AccessToken = "access_token";
                public const string SoundEnabled = "sound_enabled";
                public const string Title = "title";
                public const string OriginalVisibilityType = "original_visibility_type";
                public const string Tags = "tags";
            }
        }

        readonly RestClient restClient;
        readonly IRetry retry;
        readonly string accessToken;

        public CoubShare(IRetry retry, string accessToken)
        {
            this.retry = retry;
            this.accessToken = accessToken;
            restClient = new RestClient(CoubConsts.Url);
        }

        #region Public Methods

        public void Recoub(long recoubToId, long channelId)
        {
            var request = new RestRequest(Consts.Method.Recoubs, Method.POST);
            AddParameters(request, recoubToId, channelId);

            retry.SafeCall(() => restClient.Execute(request));
        }

        public void CreateCoub(string videoUrl, string title, string hashtags)
        {
            hashtags = String.Join(",", hashtags.Split(' ').Select(x => x.TrimStart('#')));

            var tempVideoFile = DownloadVideo(videoUrl);
            var tempAudioFile = ExtractAudio(tempVideoFile);

            CoubCoub coub = InitUpload();
            UploadVideoToCoub(coub.Id, tempVideoFile);
            UploadAudioToCoub(coub.Id, tempAudioFile);
            FinalizeUpload(coub.Id, title, hashtags);
        }

        #endregion

        #region Private Methods

        void AddParameters(RestRequest request, long? recoubToId = null, long? channelId = null)
        {
            if (recoubToId.HasValue)
            {
                request.AddParameter(Consts.Param.RecoubTo, recoubToId.Value, ParameterType.QueryString);
            }

            if (channelId.HasValue)
            {
                request.AddParameter(Consts.Param.ChannelId, channelId.Value, ParameterType.QueryString);
            }

            request.AddParameter(Consts.Param.AccessToken, accessToken, ParameterType.QueryString);
        }

        string DownloadVideo(string videoUrl)
        {
            var tempVideoFile = Path.Combine(Path.GetTempPath(), Guid.NewGuid() + ".mp4");
            using (var webClient = new WebClient())
            {
                webClient.DownloadFile(videoUrl, tempVideoFile);
            }

            return tempVideoFile;
        }

        string ExtractAudio(string videoFilePath)
        {
            var tempAudioFile = Path.Combine(Path.GetTempPath(), Guid.NewGuid() + ".m4a");

            var ffmpegExeFile = Path.GetFullPath("ffmpeg.exe");

            System.Diagnostics.ProcessStartInfo processStartInfo = new System.Diagnostics.ProcessStartInfo("cmd.exe");
            processStartInfo.RedirectStandardInput = true;
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.UseShellExecute = false;

            System.Diagnostics.Process process = System.Diagnostics.Process.Start(processStartInfo);

            if (process != null)
            {
                process.StandardInput.WriteLine($@"set video=""{videoFilePath}""");
                process.StandardInput.WriteLine($@"set audio=""{tempAudioFile}""");
                process.StandardInput.WriteLine($@"{ffmpegExeFile} -i %video% -vn -c:a copy %audio%");
                //process.StandardInput.WriteLine("yourCommand.exe arg1 arg2");

                process.StandardInput.Close(); // line added to stop process from hanging on ReadToEnd()

                string outputString = process.StandardOutput.ReadToEnd();
                //return outputString;
            }

            //System.Diagnostics.Process p = new System.Diagnostics.Process();
            //System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo();
            //info.FileName = "cmd.exe";
            //info.RedirectStandardInput = true;
            //info.RedirectStandardOutput = true;
            //info.UseShellExecute = false;
            //
            //p.StartInfo = info;
            //p.Start();
            //
            //using (StreamWriter sw = p.StandardInput)
            //{
            //    if (sw.BaseStream.CanWrite)
            //    {
            //        sw.WriteLine($@"set video=""{videoFilePath}""");
            //        sw.WriteLine($@"set audio=""{tempAudioFile}""");
            //        sw.WriteLine($@"{ffmpegExeFile} -i %video% -vn -c:a copy %audio%");
            //    }
            //}
            //
            //string output = p.StandardOutput.ReadToEnd();
            //p.WaitForExit(3000);
            //p.Start();
            return tempAudioFile;
        }

        CoubCoub InitUpload()
        {
            var request = new RestRequest(Consts.Method.CoubsInitUpload, Method.POST);
            AddParameters(request);

            var coub = retry.SafeCall(() => restClient.Execute<CoubCoub>(request));
            return coub.Data;
        }

        void UploadVideoToCoub(int coubId, string videoPath)
        {
            var request = new RestRequest(String.Format(Consts.Method.CoubsUploadVideo, coubId), Method.POST);
            AddParameters(request);

            request.AddHeader("Content-Type", "video/mp4");
            request.AddParameter("video/mp4", File.ReadAllBytes(videoPath), ParameterType.RequestBody);

            //request.AddFile("video.mp4", videoPath, "video/mp4");

            retry.SafeCall(() => restClient.Execute(request));
        }

        void UploadAudioToCoub(int coubId, string audioFile)
        {
            var request = new RestRequest(String.Format(Consts.Method.CoubsUploadAudio, coubId), Method.POST);
            AddParameters(request);

            request.AddHeader("Content-Type", "audio/x-m4a");
            request.AddParameter("audio/x-m4a", File.ReadAllBytes(audioFile), ParameterType.RequestBody);

            //request.AddFile("audio.m4a", audioFile, "audio/aac");

            retry.SafeCall(() => restClient.Execute(request));
        }

        void FinalizeUpload(int coubId, string title, string hashtags)
        {
            var request = new RestRequest(String.Format(Consts.Method.CoubsFinalizeUpload, coubId), Method.POST);
            AddParameters(request);

            request.AddParameter(Consts.Param.SoundEnabled, "true", ParameterType.QueryString);
            request.AddParameter(Consts.Param.Title, title, ParameterType.QueryString);
            request.AddParameter(Consts.Param.OriginalVisibilityType, "public", ParameterType.QueryString);
            request.AddParameter(Consts.Param.Tags, hashtags, ParameterType.QueryString);

            retry.SafeCall(() => restClient.Execute(request));
        }

        #endregion
    }
}