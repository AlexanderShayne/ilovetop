﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Contracts.Crosscutting.RetryInvocation;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Services;
using RestSharp;

namespace ILT.Coub.Infrastructure.CoubApis.Services
{
    public class CoubChannels : ICoubChannels
    {
        private static class Consts
        {
            public static class Method
            {
                public const string Channels = "v2/channels";
            }
        }

        readonly RestClient restClient;
        readonly IRetry retry;

        public CoubChannels(IRetry retry)
        {
            this.retry = retry;
            restClient = new RestClient(CoubConsts.Url);
        }

        #region Public Methods

        public IEnumerable<CoubChannel> GetChannels(IEnumerable<long> ids)
        {
            var result = new List<CoubChannel>(ids.Count());

            foreach (var id in ids)
            {
                var request = new RestRequest($"{Consts.Method.Channels}/{id}", Method.GET);

                var response = retry.SafeCall(() => restClient.Execute<CoubChannel>(request));

                if (response.StatusCode == HttpStatusCode.OK && response.Data != null)
                {
                    result.Add(response.Data);
                }
            }

            return result;
        }

        #endregion
    }
}
