﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.Entities.Images;
using ILT.Contracts.Data.Reports.Repositories;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Services;
using ILT.Domain.Posts;
using ILT.Domain.PublicPages;
using ILT.Infrastructure.Reports.Services.Base;
using ILT.Infrastructure.Reports.Services.IdsChunk;

namespace ILT.Coub.Infrastructure.Reports.Services
{
    public class UpdateCoubsService : DataLookupServiceBase<IEnumerable<Post>, Page>
    {
        private static class Consts
        {
            public const int PageCount = 1000;
        }

        readonly ICoubApi coubApi;
        readonly IIdsChunkService idsChunkService;
        readonly ISystemReportsRepository systemReportsRepository;

        public UpdateCoubsService(ICoubApi coubApi, IIdsChunkService idsChunkService, ISystemReportsRepository systemReportsRepository)
        {
            this.coubApi = coubApi;
            this.idsChunkService = idsChunkService;
            this.systemReportsRepository = systemReportsRepository;
        }

        #region ProtectedMethods

        protected override IEnumerable<Page> GetItems()
        {
            return idsChunkService.GetIdsChunk().SelectMany(idsChunk => systemReportsRepository.GetPages(idsChunk, SocialNetwork.Coub));
        }

        protected override void ProcessItem(Page item)
        {
            var coubsPage = coubApi.ChannelCoubs.GetCoubs(item.Id, 1, Consts.PageCount);
            if (!coubsPage.Coubs.Any()) return;

            AddPosts(coubsPage, item);

            if (coubsPage.TotalPages <= 1) return;

            for (int i = 1; i < coubsPage.TotalPages; i++)
            {
                coubsPage = coubApi.ChannelCoubs.GetCoubs(item.Id, i + 1, Consts.PageCount);
                AddPosts(coubsPage, item);
            }
        }

        #endregion

        #region Private Methods

        void AddPosts(CoubChannelCoubsPage coubsPage, Page item)
        {
            if (!coubsPage.Coubs.Any()) return;
            var posts = coubsPage.Coubs.Select(c => Map(c, item));

            foreach (var reportGenerator in ReportGenerators)
            {
                reportGenerator.AddData(posts);
            }
        }

        Post Map(CoubCoub coub, Page page)
        {
            var result = new Post
            {
                CreatedAt = coub.CreatedAt,
                Id = coub.Id,
                Text = coub.Title,
                PageId = page.Id,
                SocialNetwork = SocialNetwork.Coub,
                Page = page,
                LikesCount = coub.LikesCount,
                RepostsCount = coub.RecoubsCount,
                CommentsCount = coub.CommentsCount,
                ViewsCount = coub.ViewsCount,
                Permalink = $"http://coub.com/view/" + coub.Permalink
            };

            if (!String.IsNullOrEmpty(coub.ImageVersions?.Template))
            {
                result.Avatar = new Image
                {
                    UrlTemplate = coub.ImageVersions.Template.Replace("%{version}", "{0}")
                };

                result.Avatar.AddVersion(130, "tiny");
                result.Avatar.AddVersion(604, "age_restricted");
            }

            return result;
        }

        #endregion
    }
}