﻿using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.Entities.Images;
using ILT.Contracts.Data.Reports.Repositories;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Models.View;
using ILT.Coub.Contracts.Infrastructure.CoubApis.Services;
using ILT.Domain.PublicPages;
using ILT.Infrastructure.Reports.Services.Base;
using ILT.Infrastructure.Reports.Services.IdsChunk;

namespace ILT.Coub.Infrastructure.Reports.Services
{
    public class UpdateChannelsService : DataLookupServiceBase<IEnumerable<Page>, IEnumerable<long>>
    {
        private static class Consts
        {
            public const int MinFollowerCount = 50;
        }

        readonly ICoubApi coubApi;
        readonly IIdsChunkService idsChunkService;
        readonly ISystemReportsRepository reportsRepository;

        public UpdateChannelsService(ICoubApi coubApi, IIdsChunkService idsChunkService, ISystemReportsRepository reportsRepository)
        {
            this.coubApi = coubApi;
            this.idsChunkService = idsChunkService;
            this.reportsRepository = reportsRepository;
        }

        #region Protected Methods

        protected override IEnumerable<IEnumerable<long>> GetItems()
        {
            return idsChunkService.GetIdsChunk();
        }

        protected override void ProcessItem(IEnumerable<long> item)
        {
            var pages = GetPages(item);

            if (!pages.Any()) return;

            foreach (var reportGenerator in ReportGenerators)
            {
                reportGenerator.AddData(pages);
            }
        }

        #endregion

        #region Private Methods

        Page[] GetPages(IEnumerable<long> ids)
        {
            Page[] items;

            if (LookupType == ILT.Contracts.Infrastructure.Reports.Services.LookupType.Api)
            {
                items =
                    coubApi
                        .Channels
                        .GetChannels(ids)
                        .Where(x => x.FollowersCount >= Consts.MinFollowerCount)
                        .Select(Map)
                        .ToArray();

                reportsRepository.MergePages(items);
                reportsRepository.MergePageFollowers(items);
                reportsRepository.MergePageLikes(items);
            }
            else
            {
                items = reportsRepository.GetPages(ids, SocialNetwork.Coub);
            }

            return items;
        }

        static Page Map(CoubChannel x)
        {
            var result = new Page
            {
                SocialNetwork = SocialNetwork.Coub,
                Name = x.Title,
                Id = x.Id,
                ScreenName = x.Permalink,
                Permalink = $"http://coub.com/{x.Permalink}",
                Avatar = new Image
                {
                    UrlTemplate = x.AvatarVersions.Template.Replace("%{version}", "{0}")
                }
            };

            result.Avatar.AddVersion(50, "medium");
            result.Avatar.AddVersion(200, "profile_pic_new_2x");

            result.AddFollowersInfo(x.FollowersCount);
            result.AddLikesInfo(x.LikesCount);

            return result;
        }

        #endregion
    }
}
