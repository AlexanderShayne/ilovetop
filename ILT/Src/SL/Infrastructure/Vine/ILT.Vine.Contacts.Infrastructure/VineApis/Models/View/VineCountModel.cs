using System.Runtime.Serialization;

namespace ILT.Vine.Contacts.Infrastructure.VineApis.Models.View
{
    [DataContract]
    public class VineCountModel
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }
    }
}