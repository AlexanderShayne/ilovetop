﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ILT.Vine.Contacts.Infrastructure.VineApis.Models.View
{
    [DataContract]
    public class VineUsersData
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }

        [DataMember(Name = "records")]
        public List<VineUser> Records { get; set; }
    }
}