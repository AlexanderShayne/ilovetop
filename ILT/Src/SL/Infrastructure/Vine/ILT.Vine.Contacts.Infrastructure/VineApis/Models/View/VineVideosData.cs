﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ILT.Vine.Contacts.Infrastructure.VineApis.Models.View
{
    [DataContract]
    public class VineVideosData
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }

        [DataMember(Name = "records")]
        public List<VineVideo> Records { get; set; }
    }
}