﻿using System;
using System.Runtime.Serialization;

namespace ILT.Vine.Contacts.Infrastructure.VineApis.Models.View
{
    [DataContract]
    public class VineVideo
    {
        [DataMember(Name = "postId")]
        public long PostId { get; set; }

        [DataMember(Name = "avatarUrl")]
        public string AvatarUrl { get; set; }

        [DataMember(Name = "comments")]
        public VineCountModel Comments { get; set; }

        [DataMember(Name = "created")]
        public DateTime Created { get; set; }

        [DataMember(Name = "likes")]
        public VineCountModel Likes { get; set; }

        [DataMember(Name = "reposts")]
        public VineCountModel Reposts { get; set; }

        [DataMember(Name = "permalinkUrl")]
        public string PermalinkUrl { get; set; }

        [DataMember(Name = "thumbnailUrl")]
        public string ThumbnailUrl { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "videoUrl")]
        public string VideoUrl { get; set; }
    }
}