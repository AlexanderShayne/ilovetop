﻿using System.Runtime.Serialization;

namespace ILT.Vine.Contacts.Infrastructure.VineApis.Models.View
{
    [DataContract]
    public class VineUser
    {
        [DataMember(Name = "avatarUrl")]
        public string AvatarUrl { get; set; }

        [DataMember(Name = "followerCount")]
        public int FollowerCount { get; set; }

        [DataMember(Name = "userId")]
        public long UserId { get; set; }

        [DataMember(Name = "username")]
        public string Username { get; set; }

        [DataMember(Name = "likeCount")]
        public int LikeCount { get; set; }

        [DataMember(Name = "shareUrl")]
        public string ShareUrl { get; set; }
    }
}
