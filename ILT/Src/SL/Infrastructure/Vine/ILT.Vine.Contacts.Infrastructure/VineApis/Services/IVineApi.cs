﻿namespace ILT.Vine.Contacts.Infrastructure.VineApis.Services
{
    public interface IVineApi
    {
        IVineUsers Users { get; }
        IVineVideos Videos { get; }
    }
}
