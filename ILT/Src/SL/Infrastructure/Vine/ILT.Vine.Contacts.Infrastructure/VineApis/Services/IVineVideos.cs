﻿using ILT.Vine.Contacts.Infrastructure.VineApis.Models.View;

namespace ILT.Vine.Contacts.Infrastructure.VineApis.Services
{
    public interface IVineVideos
    {
        VineVideosData GetVineVideos(long userId, int page, int pageSize);
        VineVideo GetVineVideo(long id);
    }
}