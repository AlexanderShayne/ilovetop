﻿using ILT.Vine.Contacts.Infrastructure.VineApis.Models.View;

namespace ILT.Vine.Contacts.Infrastructure.VineApis.Services
{
    public interface IVineUsers
    {
        VineUsersData GetUsers(string query, int page, int pageSize);
        VineUser GetProfile(long userId);
    }
}