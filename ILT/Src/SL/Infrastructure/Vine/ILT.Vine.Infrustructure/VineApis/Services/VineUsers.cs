﻿using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;
using Contracts.Crosscutting.RetryInvocation;
using ILT.Vine.Contacts.Infrastructure.VineApis.Models.View;
using ILT.Vine.Contacts.Infrastructure.VineApis.Services;
using RestSharp;

namespace ILT.Vine.Infrustructure.VineApis.Services
{
    public class VineUsers : IVineUsers
    {
        private static class Consts
        {
            public static class Method
            {
                public const string UsersSearch = "users/search/";
                public const string UserProfile = "users/profiles/";
            }

            public static class Param
            {
                public const string Page = "page";
                public const string PerPage = "size";
            }
        }

        readonly RestClient restClient;
        readonly IRetry retry;

        public VineUsers(IRetry retry)
        {
            this.retry = retry;
            restClient = new RestClient(VineConsts.Url);
        }

        #region Public Methods

        public VineUsersData GetUsers(string query, int page, int pageSize)
        {
            var request = new RestRequest(Consts.Method.UsersSearch + query, Method.GET);
            AddParametersGetUsers(request, page, pageSize);

            var response = retry.SafeCall(() => restClient.Execute<VineUsersPage>(request));

            if (response.StatusCode != HttpStatusCode.OK || response.Data == null)
            {
                return new VineUsersData
                {
                    Records = new List<VineUser>()
                };
            }
            return response.Data.Data;
        }

        public VineUser GetProfile(long userId)
        {
            var request = new RestRequest(Consts.Method.UserProfile + userId, Method.GET);

            var response = retry.SafeCall(() => restClient.Execute<VineProfileData>(request));
            return response.Data.Data;
        }

        #endregion

        #region Private Methods

        void AddParametersGetUsers(RestRequest request, int page, int pageSize)
        {
            request.AddParameter(Consts.Param.Page, page);
            request.AddParameter(Consts.Param.PerPage, pageSize);
        }

        #endregion

        [DataContract]
        private class VineProfileData
        {
            [DataMember(Name = "data")]
            public VineUser Data { get; set; }
        }

        [DataContract]
        private class VineUsersPage
        {
            [DataMember(Name = "data")]
            public VineUsersData Data { get; set; }
        }
    }
}