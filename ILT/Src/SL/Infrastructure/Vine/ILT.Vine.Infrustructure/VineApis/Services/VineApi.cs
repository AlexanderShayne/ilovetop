﻿using ILT.Vine.Contacts.Infrastructure.VineApis.Services;

namespace ILT.Vine.Infrustructure.VineApis.Services
{
    public class VineApi : IVineApi
    {
        public VineApi(IVineUsers users, IVineVideos videos)
        {
            Users = users;
            Videos = videos;
        }

        public IVineUsers Users { get; }
        public IVineVideos Videos { get; }
    }
}
