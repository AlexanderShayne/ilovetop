﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using Contracts.Crosscutting.RetryInvocation;
using ILT.Vine.Contacts.Infrastructure.VineApis.Models.View;
using ILT.Vine.Contacts.Infrastructure.VineApis.Services;
using RestSharp;

namespace ILT.Vine.Infrustructure.VineApis.Services
{
    public class VineVideos : IVineVideos
    {
        private static class Consts
        {
            public static class Method
            {
                public const string TimelinesUsers = "timelines/users/";
                public const string TimelinesPosts = "timelines/posts/";
            }

            public static class Param
            {
                public const string Page = "page";
                public const string PerPage = "size";
            }
        }

        readonly RestClient restClient;
        readonly IRetry retry;

        public VineVideos(IRetry retry)
        {
            this.retry = retry;
            restClient = new RestClient(VineConsts.Url);
        }

        #region Public Methods

        public VineVideosData GetVineVideos(long userId, int page, int pageSize)
        {
            var request = new RestRequest(Consts.Method.TimelinesUsers + userId, Method.GET);
            AddParametersGetUsers(request, page, pageSize);

            var response = retry.SafeCall(() => restClient.Execute<VineVideosPage>(request));

            if (response.StatusCode != HttpStatusCode.OK || response.Data?.Data == null)
            {
                return new VineVideosData
                {
                    Records = new List<VineVideo>(0)
                };
            }

            return response.Data.Data;
        }

        public VineVideo GetVineVideo(long id)
        {
            var request = new RestRequest(Consts.Method.TimelinesPosts + id, Method.GET);

            var response = retry.SafeCall(() => restClient.Execute<VineVideosPage>(request));
            return response.Data.Data.Records.FirstOrDefault();
        }

        #endregion

        #region Private Methods

        void AddParametersGetUsers(RestRequest request, int page, int pageSize)
        {
            request.AddParameter(Consts.Param.Page, page);
            request.AddParameter(Consts.Param.PerPage, pageSize);
        }

        #endregion

        [DataContract]
        private class VineVideosPage
        {
            [DataMember(Name = "data")]
            public VineVideosData Data { get; set; }
        }
    }
}