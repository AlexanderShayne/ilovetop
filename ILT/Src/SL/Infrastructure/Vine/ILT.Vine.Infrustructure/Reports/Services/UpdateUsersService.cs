﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.Entities.Images;
using ILT.Contracts.Data.Reports.Repositories;
using ILT.Contracts.Infrastructure.Reports.Services;
using ILT.Domain.PublicPages;
using ILT.Infrastructure.Reports.Services.Base;
using ILT.Infrastructure.Reports.Services.IdsChunk;
using ILT.Vine.Contacts.Infrastructure.VineApis.Models.View;
using ILT.Vine.Contacts.Infrastructure.VineApis.Services;

namespace ILT.Vine.Infrustructure.Reports.Services
{
    public class UpdateUsersService : DataLookupServiceBase<IEnumerable<Page>, Tuple<string, IEnumerable<long>>>
    {
        private static class Consts
        {
            public const int PageCount = 100;
        }

        readonly IVineApi vineApi;
        readonly ISystemReportsRepository reportsRepository;
        readonly IIdsChunkService idsChunkService;

        public UpdateUsersService(IVineApi vineApi, IIdsChunkService idsChunkService, ISystemReportsRepository reportsRepository)
        {
            this.vineApi = vineApi;
            this.reportsRepository = reportsRepository;
            this.idsChunkService = idsChunkService;
        }

        #region Protected Methods

        protected override IEnumerable<Tuple<string, IEnumerable<long>>> GetItems()
        {
            if (idsChunkService == null)
            {
                var abc = "abcdefghijklmnopqrstuvwxz";

                //for (int i = 0; i < 25; i++)
                //{
                //    for (int j = 0; j < 25; j++)
                //    {
                //        yield return Tuple.Create<string, IEnumerable<long>>($"{abc[i]} {abc[j]}", null);
                //    }
                //}

                for (int i = 0; i < 25; i++)
                {
                    for (int j = 0; j < 25; j++)
                    {
                        yield return Tuple.Create<string, IEnumerable<long>>($"{abc[i]} {abc[j]}", null);
                    }
                }

                //for (int i = 0; i < 25; i++)
                //{
                //    for (int j = 0; j < 25; j++)
                //    {
                //        for (int k = 0; k < 25; k++)
                //        {
                //            yield return Tuple.Create<string, IEnumerable<long>>($"{abc[i]}{abc[j]}{abc[k]}", null);
                //        }
                //    }
                //}
            }
            else
            {
                foreach (var chunk in idsChunkService.GetIdsChunk())
                {
                    yield return Tuple.Create<string, IEnumerable<long>>(null, chunk);
                }
            }
        }

        protected override void ProcessItem(Tuple<string, IEnumerable<long>> item)
        {
            var pages = GetPages(item);

            if (!pages.Any()) return;

            foreach (var reportGenerator in ReportGenerators)
            {
                reportGenerator.AddData(pages);
            }
        }

        #endregion

        #region Private Methods

        Page[] GetPages(Tuple<string, IEnumerable<long>> query)
        {
            Page[] items;

            if (idsChunkService == null)
            {
                var usersData = vineApi.Users.GetUsers(query.Item1, 1, Consts.PageCount);
                var users = usersData.Records.Where(x => x.FollowerCount > 300000).ToList();
                var pagesCount = GetPageCount(usersData.Count);

                for (int i = 1; i < pagesCount; i++)
                {
                    users = users.Concat(vineApi.Users.GetUsers(query.Item1, i + 1, Consts.PageCount).Records.Where(x => x.FollowerCount > 300000)).ToList();
                }

                items =
                    users.Select(Map).ToArray();

                reportsRepository.MergePages(items);
                reportsRepository.MergePageFollowers(items);
                reportsRepository.MergePageLikes(items);
            }
            else
            {
                if (LookupType == LookupType.Api)
                {
                    var result = new List<Page>();

                    foreach (var id in query.Item2)
                    {
                        var profile = vineApi.Users.GetProfile(id);

                        var page = new Page
                        {
                            SocialNetwork = SocialNetwork.Vine,
                            Name = profile.Username,
                            Id = profile.UserId,
                            ScreenName = String.Empty,
                            Permalink = profile.ShareUrl,
                            Avatar = new Image
                            {
                                UrlTemplate = profile.AvatarUrl
                            }
                        };

                        page.Avatar.AddVersion(50, "");
                        page.Avatar.AddVersion(200, "");

                        page.AddFollowersInfo(profile.FollowerCount);
                        page.AddLikesInfo(profile.LikeCount);

                        result.Add(page);
                    }

                    reportsRepository.MergePages(result);
                    reportsRepository.MergePageFollowers(result);
                    reportsRepository.MergePageLikes(result);

                    items = result.ToArray();
                }
                else
                {
                    items = reportsRepository.GetPages(query.Item2, SocialNetwork.Vine);
                }
            }

            return items;
        }

        Page Map(VineUser x)
        {
            var profile = vineApi.Users.GetProfile(x.UserId);

            var result = new Page
            {
                SocialNetwork = SocialNetwork.Vine,
                Name = x.Username,
                Id = x.UserId,
                ScreenName = String.Empty,
                Permalink = profile.ShareUrl,
                Avatar = new Image
                {
                    UrlTemplate = x.AvatarUrl
                }
            };

            result.Avatar.AddVersion(50, "");
            result.Avatar.AddVersion(200, "");

            result.AddFollowersInfo(profile.FollowerCount);
            result.AddLikesInfo(profile.LikeCount);

            return result;
        }

        int GetPageCount(int total)
        {
            var pageCount = (total / Consts.PageCount) + (total % Consts.PageCount > 0 ? 1 : 0);

            if (pageCount > 10)
            {
                pageCount = 10;
            }

            return pageCount;
        }

        #endregion
    }
}
