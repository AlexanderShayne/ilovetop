﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.Entities.Images;
using ILT.Contracts.Data.Reports.Repositories;
using ILT.Domain.Posts;
using ILT.Domain.PublicPages;
using ILT.Infrastructure.Reports.Services.Base;
using ILT.Infrastructure.Reports.Services.IdsChunk;
using ILT.Vine.Contacts.Infrastructure.VineApis.Models.View;
using ILT.Vine.Contacts.Infrastructure.VineApis.Services;

namespace ILT.Vine.Infrustructure.Reports.Services
{
    public class UpdateVinesService : DataLookupServiceBase<IEnumerable<Post>, Page>
    {
        private static class Consts
        {
            public const int PageCount = 100;
        }

        readonly IVineApi vineApi;
        readonly IIdsChunkService idsChunkService;
        readonly ISystemReportsRepository systemReportsRepository;

        public UpdateVinesService(IVineApi vineApi, IIdsChunkService idsChunkService, ISystemReportsRepository systemReportsRepository)
        {
            this.vineApi = vineApi;
            this.idsChunkService = idsChunkService;
            this.systemReportsRepository = systemReportsRepository;
        }

        #region ProtectedMethods

        protected override IEnumerable<Page> GetItems()
        {
            return idsChunkService.GetIdsChunk().SelectMany(idsChunk => systemReportsRepository.GetPages(idsChunk, SocialNetwork.Vine));
        }

        protected override void ProcessItem(Page item)
        {
            var vinePage = vineApi.Videos.GetVineVideos(item.Id, 1, Consts.PageCount);
            if (!vinePage.Records.Any()) return;

            var totalPages = GetPageCount(vinePage.Count);

            AddPosts(vinePage, item);

            if (totalPages <= 1) return;

            for (int i = 1; i < totalPages; i++)
            {
                vinePage = vineApi.Videos.GetVineVideos(item.Id, i + 1, Consts.PageCount);
                AddPosts(vinePage, item);
            }
        }

        #endregion

        #region Private Methods

        void AddPosts(VineVideosData vineVideosData, Page item)
        {
            if (!vineVideosData.Records.Any()) return;

            var posts = vineVideosData.Records.Select(c => Map(c, item));

            foreach (var reportGenerator in ReportGenerators)
            {
                reportGenerator.AddData(posts);
            }
        }

        Post Map(VineVideo video, Page page)
        {
            var result = new Post
            {
                CreatedAt = video.Created,
                Id = video.PostId,
                Text = video.Description,
                PageId = page.Id,
                SocialNetwork = SocialNetwork.Vine,
                Page = page,
                LikesCount = video.Likes.Count,
                RepostsCount = video.Reposts.Count,
                CommentsCount = video.Comments.Count,
                Permalink = video.PermalinkUrl,
                Avatar = new Image
                {
                    UrlTemplate = video.ThumbnailUrl
                }
            };

            result.Avatar.AddVersion(130, String.Empty);
            result.Avatar.AddVersion(604, String.Empty);

            return result;
        }

        int GetPageCount(int total)
        {
            var pageCount = (total / Consts.PageCount) + (total % Consts.PageCount > 0 ? 1 : 0);

            if (pageCount > 10)
            {
                pageCount = 10;
            }

            return pageCount;
        }

        #endregion
    }
}