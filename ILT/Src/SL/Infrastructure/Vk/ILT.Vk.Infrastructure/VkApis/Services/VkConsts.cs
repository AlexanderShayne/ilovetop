namespace ILT.Vk.Infrastructure.VkApis.Services
{
    public static class VkConsts
    {
        public const string Url = "https://api.vk.com/method/";

        public const string VersionKey = "v";
        public const string VersionValue = "5.52";
    }
}