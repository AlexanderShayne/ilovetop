using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Contracts.Crosscutting.RetryInvocation;
using ILT.Vk.Contracts.Infrastructure.VkApis.Models.View;
using ILT.Vk.Contracts.Infrastructure.VkApis.Services;
using RestSharp;

namespace ILT.Vk.Infrastructure.VkApis.Services
{
    public class VkGroups : IVkGroups
    {
        private static class Consts
        {
            public static class Method
            {
                public const string GetById = "groups.getById";
            }

            public static class Param
            {
                public const string GroupIds = "group_ids";
                public const string Fields = "fields";
            }
        }

        readonly RestClient restClient;
        readonly IRetry retry;

        public VkGroups(IRetry retry)
        {
            this.retry = retry;
            restClient = new RestClient(VkConsts.Url);
        }

        public IEnumerable<VkGroup> GetById(IEnumerable<long> ids)
        {
            var request = new RestRequest(Consts.Method.GetById, Method.POST);

            AddParametersToGetById(request, ids);

            var response = retry.SafeCall(() => restClient.Execute<VkResponse>(request));

            return response.Data.Response;
        }

        #region Private Methods

        Parameter[] Parameters { get; } =
            {
                new Parameter
                {
                    Name = Consts.Param.Fields,
                    Value = "members_count",
                    Type = ParameterType.GetOrPost
                },

                new Parameter
                {
                    Name = VkConsts.VersionKey,
                    Value = VkConsts.VersionValue,
                    Type = ParameterType.GetOrPost
                }
            };

        void AddParametersToGetById(RestRequest request, IEnumerable<long> ids)
        {
            request.Parameters.AddRange(Parameters);
            request.AddParameter(Consts.Param.GroupIds, String.Join(",", ids));
        }

        #endregion

        [DataContract]
        private class VkResponse
        {
            [DataMember(Name = "response")]
            public List<VkGroup> Response { get; set; }
        }
    }
}