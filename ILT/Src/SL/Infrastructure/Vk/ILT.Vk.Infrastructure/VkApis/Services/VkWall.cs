using System.Collections.Generic;
using System.Net;
using System.Runtime.Serialization;
using Contracts.Crosscutting.RetryInvocation;
using ILT.Vk.Contracts.Infrastructure.VkApis.Models.View;
using ILT.Vk.Contracts.Infrastructure.VkApis.Services;
using RestSharp;

namespace ILT.Vk.Infrastructure.VkApis.Services
{
    public class VkWall : IVkWall
    {
        private static class Consts
        {
            public static class Method
            {
                public const string Get = "wall.get";
            }

            public static class Param
            {
                public const string OwnerId = "owner_id";
                public const string Offset = "offset";
                public const string Count = "count";
            }
        }

        readonly RestClient restClient;
        readonly IRetry retry;

        public VkWall(IRetry retry)
        {
            this.retry = retry;
            this.restClient = new RestClient(VkConsts.Url);
        }

        public VkWallPostsPage Get(long groupId, int offset, int count)
        {
            var request = new RestRequest(Consts.Method.Get, Method.POST);

            AddParametersToGet(request, groupId, offset, count);

            var result = retry.SafeCall(() => restClient.Execute<VkResponse>(request));

            if (result.StatusCode != HttpStatusCode.OK || result.Data?.Response == null)
            {
                return new VkWallPostsPage
                {
                    Items = new List<VkPost>(0)
                };
            }

            return result.Data.Response;
        }

        #region Private Methos

        Parameter VersionParameter { get; } =
            new Parameter
            {
                Name = VkConsts.VersionKey,
                Value = VkConsts.VersionValue,
                Type = ParameterType.GetOrPost
            };
            

        void AddParametersToGet(RestRequest request, long groupId, int offset, int count)
        {
            request.AddParameter(VersionParameter);
            request.AddParameter(Consts.Param.OwnerId, -groupId);
            request.AddParameter(Consts.Param.Offset, offset);
            request.AddParameter(Consts.Param.Count, count);
        }

        #endregion

        [DataContract]
        private class VkResponse
        {
            [DataMember(Name = "response")]
            public VkWallPostsPage Response { get; set; }
        }
    }
}