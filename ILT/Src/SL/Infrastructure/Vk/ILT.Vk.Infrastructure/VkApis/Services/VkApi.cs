using ILT.Vk.Contracts.Infrastructure.VkApis.Services;

namespace ILT.Vk.Infrastructure.VkApis.Services
{
    public class VkApi : IVkApi
    {
        public VkApi(IVkGroups vkGroups, IVkWall vkWall)
        {
            Groups = vkGroups;
            Wall = vkWall;
        }

        public IVkGroups Groups { get; }
        public IVkWall Wall { get; }
    }
}