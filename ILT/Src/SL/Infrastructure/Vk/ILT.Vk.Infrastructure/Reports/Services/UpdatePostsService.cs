﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.Entities.Images;
using ILT.Contracts.Data.Reports.Repositories;
using ILT.Domain.Posts;
using ILT.Domain.PublicPages;
using ILT.Infrastructure.Reports.Services.Base;
using ILT.Infrastructure.Reports.Services.IdsChunk;
using ILT.Vk.Contracts.Infrastructure.VkApis.Models.View;
using ILT.Vk.Contracts.Infrastructure.VkApis.Services;

namespace ILT.Vk.Infrastructure.Reports.Services
{
    public class UpdatePostsService : DataLookupServiceBase<IEnumerable<Post>, Page>
    {
        private static class Consts
        {
            public const int PageMax = 100;
            public const int PageSize = 100;
        }

        readonly IVkApi vkApi;
        readonly IIdsChunkService idsChunkService;
        readonly ISystemReportsRepository reportsRepository;
        readonly Image image;

        public UpdatePostsService(IVkApi vkApi, ISystemReportsRepository reportsRepository, IIdsChunkService idsChunkService)
        {
            this.vkApi = vkApi;
            this.reportsRepository = reportsRepository;
            this.idsChunkService = idsChunkService;

            this.image = new Image
            {
                UrlTemplate = "http://ilovetop.net/app/assets/vk-communities/top-posts-per-day-200.png"
            };

            image.AddVersion(130, String.Empty);
            image.AddVersion(604, String.Empty);
        }

        #region ProtectedMethods

        protected override IEnumerable<Page> GetItems()
        {
            return idsChunkService.GetIdsChunk().SelectMany(idsChunk => reportsRepository.GetPages(idsChunk, SocialNetwork.Vk));
        }

        protected override void ProcessChunk(IEnumerable<Page> chunk)
        {
            base.ProcessChunk(chunk);

            reportsRepository.MergePageLikes(chunk);
        }

        protected override void ProcessItem(Page item)
        {
            int total;
            Console.WriteLine(1);

            AddPosts(item, 0, out total);

            var pageCount = GetPageCount(total);

            for (int i = 1; i < pageCount; i++)
            {
                AddPosts(item, i * Consts.PageSize, out total);
            }

            if (item.LikesInfo != null)
            {
                item.AddLikesInfo(item.LikesInfo.Count);
            }
        }

        #endregion

        #region Private Methods

        int GetPageCount(int total)
        {
            var pageCount = (total / Consts.PageSize) + (total % Consts.PageSize > 0 ? 1 : 0);

            if (pageCount > Consts.PageMax) pageCount = Consts.PageMax;

            return pageCount;
        }

        void AddPosts(Page community, int offset, out int total)
        {
            var response = vkApi.Wall.Get(community.Id, offset, Consts.PageSize);

            if (response == null)
            {
                total = 0;
                return;
            }

            if (community.LikesInfo == null)
            {
                community.LikesInfo = new PageInfo {PageId = community.Id, SocialNetwork = community.SocialNetwork};
            }

            var posts = response.Items.Select(x => Map(x, community)).ToList();

            if (!posts.Any())
            {
                total = 0;
                return;
            }

            foreach (var reportGenerator in ReportGenerators)
            {
                reportGenerator.AddData(posts);
            }

            total = response.Count;
        }

        Post Map(VkPost vkPost, Page community)
        {
            var result = new Post
            {
                CreatedAt = vkPost.Date,
                Id = vkPost.Id,
                Text = vkPost.Text,
                PageId = community.Id,
                SocialNetwork = SocialNetwork.Vk,
                Page = community,
                Permalink = $"https://vk.com/wall-{community.Id}_{vkPost.Id}"
            };

            if (vkPost.Likes != null)
            {
                result.LikesCount = vkPost.Likes.Count;
                community.LikesInfo.Count += vkPost.Likes.Count;
            }

            if (vkPost.Comments != null)
            {
                result.CommentsCount = vkPost.Comments.Count;
            }

            if (vkPost.Reposts != null)
            {
                result.RepostsCount = vkPost.Reposts.Count;
            }

            var attachment = vkPost.Attachments?.FirstOrDefault();

            if (attachment == null)
            {
                //TODO: Remove hack
                result.Avatar = image;
                return result;
            }

            if (attachment.Photo != null)
            {
                var photo = attachment.Photo;

                result.Avatar = new Image
                {
                    UrlTemplate = new Uri(photo.Photo_130).GetLeftPart(UriPartial.Authority) + "{0}"
                };

                if (photo.Photo_130 != null)
                {
                    result.Avatar.AddVersion(130, new Uri(photo.Photo_130).PathAndQuery);
                }

                if (photo.Photo_604 != null)
                {
                    result.Avatar.AddVersion(604, new Uri(photo.Photo_604).PathAndQuery);
                }
            }

            if (attachment.Video != null)
            {
                var video = attachment.Video;

                result.Avatar = new Image
                {
                    UrlTemplate = new Uri(video.Photo_130).GetLeftPart(UriPartial.Authority) + "{0}"
                };

                if (video.Photo_130 != null)
                {
                    result.Avatar.AddVersion(130, new Uri(video.Photo_130).PathAndQuery);
                }

                if (video.Photo_800 != null)
                {
                    result.Avatar.AddVersion(604, new Uri(video.Photo_800).PathAndQuery);
                }
            }

            return result;
        }

        #endregion
    }
}