﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.Entities.Images;
using ILT.Contracts.Data.Reports.Repositories;
using ILT.Domain.PublicPages;
using ILT.Infrastructure.Reports.Services.Base;
using ILT.Infrastructure.Reports.Services.IdsChunk;
using ILT.Vk.Contracts.Infrastructure.VkApis.Models.View;
using ILT.Vk.Contracts.Infrastructure.VkApis.Services;

namespace ILT.Vk.Infrastructure.Reports.Services
{
    public class UpdateGroupsService : DataLookupServiceBase<IEnumerable<Page>, IEnumerable<long>>
    {
        private static class Consts
        {
            public const int MinMembersCount = 900000;
        }

        readonly IVkApi vkApi;
        readonly IIdsChunkService idsChunkService;
        readonly ISystemReportsRepository reportsRepository;

        public UpdateGroupsService(IVkApi vkApi, IIdsChunkService idsChunkService, ISystemReportsRepository reportsRepository)
        {
            this.vkApi = vkApi;
            this.idsChunkService = idsChunkService;
            this.reportsRepository = reportsRepository;
        }

        #region Protected Methods

        protected override IEnumerable<IEnumerable<long>> GetItems()
        {
            return idsChunkService.GetIdsChunk();
        }

        protected override void ProcessItem(IEnumerable<long> item)
        {
            var groups = GetGroups(item);

            if (!groups.Any()) return;

            foreach (var reportGenerator in ReportGenerators)
            {
                reportGenerator.AddData(groups);
            }
        }

        #endregion

        #region Private Methods

        Page[] GetGroups(IEnumerable<long> ids)
        {
            Page[] items;

            if (LookupType == ILT.Contracts.Infrastructure.Reports.Services.LookupType.Api)
            {
                items =
                    vkApi
                        .Groups
                        .GetById(ids)
                        .Where(x => !x.IsClosed && x.MembersCount >= Consts.MinMembersCount)
                        .Select(Map)
                        .ToArray();

                reportsRepository.MergePages(items);
                reportsRepository.MergePageFollowers(items);
            }
            else
            {
                items = reportsRepository.GetPages(ids, SocialNetwork.Vk);
            }

            return items;
        }

        static Page Map(VkGroup x)
        {
            var avatar = new Image
            {
                UrlTemplate = new Uri(x.Photo_50).GetLeftPart(UriPartial.Authority) + "{0}"
            };

            var result = new Page
            {
                SocialNetwork = SocialNetwork.Vk,
                Name = x.Name,
                Id = x.Id,
                ScreenName = x.ScreenName,
                Permalink = $"https://vk.com/{x.ScreenName}",
                Avatar = avatar
            };

            result.Avatar.AddVersion(50, new Uri(x.Photo_50).PathAndQuery);
            result.Avatar.AddVersion(100, new Uri(x.Photo_100).PathAndQuery);
            result.Avatar.AddVersion(200, new Uri(x.Photo_200).PathAndQuery);

            result.AddFollowersInfo(x.MembersCount);

            return result;
        }

        #endregion
    }
}