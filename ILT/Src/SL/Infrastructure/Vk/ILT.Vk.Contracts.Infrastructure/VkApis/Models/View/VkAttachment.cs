using System.Runtime.Serialization;

namespace ILT.Vk.Contracts.Infrastructure.VkApis.Models.View
{
    [DataContract]
    public class VkAttachment
    {
        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "photo")]
        public VkPhoto Photo { get; set; }

        [DataMember(Name = "video")]
        public VkVideo Video { get; set; }
    }
}