using System.Runtime.Serialization;

namespace ILT.Vk.Contracts.Infrastructure.VkApis.Models.View
{
    [DataContract]
    public class VkGroup
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "screen_name")]
        public string ScreenName { get; set; }

        [DataMember(Name = "is_closed")]
        public bool IsClosed { get; set; }

        [DataMember(Name = "type")]
        public string Type { get; set; }

        [DataMember(Name = "members_count")]
        public int MembersCount { get; set; }

        [DataMember(Name = "photo_50")]
        public string Photo_50 { get; set; }

        [DataMember(Name = "photo_100")]
        public string Photo_100 { get; set; }

        [DataMember(Name = "photo_200")]
        public string Photo_200 { get; set; }
    }
}