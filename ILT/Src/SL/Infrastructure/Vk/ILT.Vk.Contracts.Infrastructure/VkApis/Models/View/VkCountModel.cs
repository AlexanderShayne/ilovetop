using System.Runtime.Serialization;

namespace ILT.Vk.Contracts.Infrastructure.VkApis.Models.View
{
    [DataContract]
    public class VkCountModel
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }
    }
}