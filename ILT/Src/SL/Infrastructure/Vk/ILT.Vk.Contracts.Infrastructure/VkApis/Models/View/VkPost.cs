using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ILT.Vk.Contracts.Infrastructure.VkApis.Models.View
{
    [DataContract]
    public class VkPost
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "owner_id")]
        public int OwnerId { get; set; }

        [DataMember(Name = "date")]
        public DateTime Date { get; set; }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "comments")]
        public VkCountModel Comments { get; set; }

        [DataMember(Name = "likes")]
        public VkCountModel Likes { get; set; }

        [DataMember(Name = "reposts")]
        public VkCountModel Reposts { get; set; }

        [DataMember(Name = "attachments")]
        public List<VkAttachment> Attachments { get; set; }
    }
}