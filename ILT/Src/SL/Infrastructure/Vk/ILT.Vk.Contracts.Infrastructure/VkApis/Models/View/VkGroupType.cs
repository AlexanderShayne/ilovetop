namespace ILT.Vk.Contracts.Infrastructure.VkApis.Models.View
{
    public enum VkGroupType
    {
        Page,
        Group,
        Event
    }
}