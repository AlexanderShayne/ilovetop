using System.Runtime.Serialization;

namespace ILT.Vk.Contracts.Infrastructure.VkApis.Models.View
{
    [DataContract]
    public class VkVideo
    {
        [DataMember(Name = "photo_130")]
        public string Photo_130 { get; set; }

        [DataMember(Name = "photo_807")]
        public string Photo_800 { get; set; }
    }
}