using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ILT.Vk.Contracts.Infrastructure.VkApis.Models.View
{
    [DataContract]
    public class VkWallPostsPage
    {
        [DataMember(Name = "count")]
        public int Count { get; set; }

        [DataMember(Name = "items")]
        public List<VkPost> Items { get; set; }
    }
}