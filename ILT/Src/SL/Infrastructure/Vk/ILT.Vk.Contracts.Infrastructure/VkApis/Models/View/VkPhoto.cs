using System.Runtime.Serialization;

namespace ILT.Vk.Contracts.Infrastructure.VkApis.Models.View
{
    [DataContract]
    public class VkPhoto
    {
        [DataMember(Name = "photo_130")]
        public string Photo_130 { get; set; }

        [DataMember(Name = "photo_604")]
        public string Photo_604 { get; set; }
    }
}