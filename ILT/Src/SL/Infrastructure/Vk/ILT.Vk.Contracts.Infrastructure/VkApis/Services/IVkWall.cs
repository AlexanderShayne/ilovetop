﻿using ILT.Vk.Contracts.Infrastructure.VkApis.Models.View;

namespace ILT.Vk.Contracts.Infrastructure.VkApis.Services
{
    public interface IVkWall
    {
        VkWallPostsPage Get(long groupId, int offset, int count);
    }
}