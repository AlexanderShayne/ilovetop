﻿using System.Collections.Generic;
using ILT.Vk.Contracts.Infrastructure.VkApis.Models.View;

namespace ILT.Vk.Contracts.Infrastructure.VkApis.Services
{
    public interface IVkGroups
    {
        IEnumerable<VkGroup> GetById(IEnumerable<long> ids);
    }
}