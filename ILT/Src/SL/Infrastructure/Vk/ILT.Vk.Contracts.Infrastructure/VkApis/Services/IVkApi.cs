namespace ILT.Vk.Contracts.Infrastructure.VkApis.Services
{
    public interface IVkApi
    {
        IVkGroups Groups { get; }
        IVkWall Wall { get; }
    }
}