﻿using System.Collections.Generic;
using System.Web.Hosting;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Contracts.Crosscutting.Configuration;
using Contracts.Crosscutting.IoC;
using Contracts.Crosscutting.Logging;
using Contracts.Domain.Events;
using Contracts.Services.Application;
using Contracts.Services.Infrastructure.Emails;
using Contracts.Services.Infrastructure.Files.Services;
using Contracts.Services.Infrastructure.Files.Services.Validation;
using Contracts.Services.Infrastructure.Preview.Services;
using Crosscutting.Infrastructure.Configuration;
using Crosscutting.IoC;
using Crosscutting.Logging.Log4Net;
using Services.Infrastructure.Emails;
using Services.Infrastructure.Files;
using Services.Infrastructure.Files.Validation;
using Services.Infrastructure.Preview.Services;
using Component = Castle.MicroKernel.Registration.Component;

namespace Crosscutting.Configuration
{
    public class Module : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(

                Component
                    .For<ILoggerFactory>()
                    .ImplementedBy<Log4NetLoggerFactory>()
                    .LifeStyle
                    .Singleton,

                Component
                    .For<IConfig>()
                    .ImplementedBy<WebConfig>()
                    .LifeStyle
                    .Transient,

                Component.For<IGuard>()
                    .ImplementedBy<Guard>()
                    .LifeStyle
                    .PerWebRequest,

                 Component.For<IEmailService>()
                    .ImplementedBy<EmailService>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<IHandle<SendEmailMessage>>()
                    .ImplementedBy<SendEmailMessageHandler>()
                    .LifeStyle
                    .Transient,

                Component
                    .For<IFileService>()
                    .ImplementedBy<LocalStorageFileService>()
                    .DependsOn(Dependency.OnValue("ServerPath", HostingEnvironment.MapPath("~/")))
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<IContainer>()
                    .ImplementedBy<ContainerWrapper>()
                    .LifeStyle
                    .Singleton,

                Component
                    .For<IDictionary<Restrictions, IFileValidator>>()
                    .UsingFactoryMethod(() => new Dictionary<Restrictions, IFileValidator>
                    {
                        { Restrictions.Image, new IsImageValidator() },
                        { Restrictions.MaxSize50Mb, new IsLessThan50MbValidator() },
                        { Restrictions.Pdf, new IsPdfValidator() }
                    })
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<IPreviewService>()
                    .ImplementedBy<PreviewService>()
                    .LifeStyle
                    .PerWebRequest
                );
        }
    }
}
