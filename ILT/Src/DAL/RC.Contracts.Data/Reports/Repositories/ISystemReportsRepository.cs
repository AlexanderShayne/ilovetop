﻿using System.Collections.Generic;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Domain.PublicPages;
using ILT.Domain.Reports;

namespace ILT.Contracts.Data.Reports.Repositories
{
    public interface ISystemReportsRepository
    {
        void MergeReports(IEnumerable<Report> reports);
        void MergePages(IEnumerable<Page> pages);
        void MergePageLikes(IEnumerable<Page> pages);
        void MergePageFollowers(IEnumerable<Page> pages);

        void LogicallyDeleteAllReports(SocialNetwork socialNetwork);

        void UpdateIsPostedInfo(Report report);

        Report GetReport(string reportKey, SocialNetwork socialNetwork);
        Page[] GetPages(IEnumerable<long> ids, SocialNetwork socialNetwork);
    }
}
