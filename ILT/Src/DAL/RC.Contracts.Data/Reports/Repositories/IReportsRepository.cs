﻿using System;
using System.Collections.Generic;
using Contracts.Dal;
using Contracts.Dal.DomainStack;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Domain.Reports;

namespace ILT.Contracts.Data.Reports.Repositories
{
    public interface IReportsRepository : IRepository<Report, Guid>
    {
        IEnumerable<Report> ReadReportsByPostId(SocialNetwork socialNetwork, long pageId, long postId);
    }
}