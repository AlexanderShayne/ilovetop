﻿using Contracts.Dal;
using Contracts.Dal.DomainStack;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Domain.PublicPages;

namespace ILT.Contracts.Data.PublicPages.Repositories
{
    public interface IPagesRepository : IRepository<Page, long>
    {
        Page Read(SocialNetwork socialNetwork, long id);
        void ExcludeFromReports(SocialNetwork socialNetwork, long pageId);
    }
}
