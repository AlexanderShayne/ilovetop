﻿using System;
using Contracts.Dal;
using Contracts.Dal.DomainStack;
using ILT.Domain.Hashtags;

namespace ILT.Contracts.Data.Hashtags.Repositories
{
    public interface IHashtagsRepository : IRepository<Hashtag, Guid>
    {
    }
}
