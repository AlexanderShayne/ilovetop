﻿using System.Data.Entity;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Dal.DomainStack.Ef;
using Dal.DomainStack.Ef.Context;
using ILT.Contracts.Data.PublicPages.Repositories;
using ILT.Domain.PublicPages;

namespace ILT.Data.PublicPages.Repositories
{
    public class PagesRepository : RepositoryBase<Page, long>, IPagesRepository
    {
        public PagesRepository(IDataContext dataContext) : base(dataContext)
        {
        }

        #region Public Methods

        public Page Read(SocialNetwork socialNetwork, long id)
        {
            var result =
                GetDbSet()
                    .Include(x => x.Hashtags)
                    .FirstOrDefault(x => x.SocialNetwork == socialNetwork && x.Id == id);

            return result;
        }

        public void ExcludeFromReports(SocialNetwork socialNetwork, long pageId)
        {
            ((DataContext) dataContext).Database.ExecuteSqlCommand(
                $@"
                UPDATE
                    [dbo].[Page]
                SET
                    [ExcludeFromReports] = 1
                WHERE
                    [SocialNetwork] = {(int)socialNetwork} AND [Id] = {pageId};
                ");
        }

        #endregion
    }
}
