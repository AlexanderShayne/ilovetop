﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Dal.QueryStack.Dapper;
using Dapper;
using ILT.Contracts.Dal.Query.PublicPages.Dtos.View;
using ILT.Contracts.Dal.Query.PublicPages.QueryRepositories;
using ILT.Contracts.Dal.Query.Shared.Dtos.View;

namespace ILT.Data.PublicPages.QueryRepositories
{
    public class PagesQueryRepository : QueryRepositoryBase, IPagesQueryRepository
    {
        #region Public Methods

        public IEnumerable<PageSummaryDto> Query(SocialNetwork socialNetwork, int page, int pageSize)
        {
            var offset = GetOffset(page, pageSize);

            var sql = @"
                
                DECLARE @TempId TABLE
                (
                    Id BIGINT
                );

                INSERT INTO
                    @TempId
                SELECT
                    [c].[Id]
                FROM
                    [dbo].[Page] [c]
                OUTER APPLY
                    (SELECT TOP(1) * FROM [dbo].[FollowersInfo] [mi] WHERE [mi].[PageId] = [c].[Id] AND [mi].[SocialNetwork] = @SocialNetwork ORDER BY [mi].[CreatedAt] DESC) [mi]
                WHERE
                    [c].[SocialNetwork] = @SocialNetwork
                ORDER BY
                    [mi].[Count] DESC
                OFFSET
                    @Offset
                ROWS FETCH NEXT @PageSize ROWS ONLY;

                --COMMUNITIES
                SELECT
                    [c].[Id]
                   ,[c].[Name]
                   ,[c].[ScreenName]
                   ,[c].[ExcludeFromReports]
                   ,[c].[Permalink]
                   ,[c].[Photo50]
                   ,[c].[Photo200]
                   ,[c].[Id]
                   ,[Count] = [c].[FollowersCount]
                   ,[DayChanges] = [c].[FollowersDayChanges]
                   ,[WeekChanges] = [c].[FollowersWeekChanges]
                   ,[MonthChanges] = [c].[FollowersMonthChanges]
                   ,[CreatedAt] = [c].[FollowersCreatedAt]
                   ,[c].[Id]
                   ,[Count] = [c].[LikesCount]
                   ,[DayChanges] = [c].[LikesDayChanges]
                   ,[WeekChanges] = [c].[LikesWeekChanges]
                   ,[MonthChanges] = [c].[LikesMonthChanges]
                   ,[CreatedAt] = [c].[LikesCreatedAt]
                FROM
                    @TempId [Temp]
                JOIN
                    [dbo].[PageView] [c] ON [c].[Id] = [Temp].[Id] AND [c].[SocialNetwork] = @SocialNetwork;

                  --COMMUNITY REPORTS
                SELECT
                    [PageId] = [Temp].[Id]
                   ,[r].[Key]
                   ,[r].[Photo50]
                FROM
                    @TempId [Temp]
                JOIN
                    [dbo].[ReportPage] [rc] ON [rc].[PageId] = [Temp].[Id] AND [rc].[SocialNetwork] = @SocialNetwork
                JOIN
                    [dbo].[ReportView] [r] ON [r].[Id] = [rc].[ReportId];

                --COMMUNITY HASHTAGS
                SELECT
                    [PageId] = [Temp].[Id]
                   ,[Name] = [h].[Name]
                FROM
                    @TempId [Temp]
                JOIN
                    [dbo].[PageHashtag] [ch] ON [ch].[PageId] = [Temp].[Id] AND [ch].[SocialNetwork] = @SocialNetwork
                JOIN
                    [dbo].[Hashtag] [h] ON [h].[Id] = [ch].[HashtagId];
            ";

            using (var dbCon = GetDbConnection())
            {
                dbCon.Open();

                using (var multi = dbCon.QueryMultiple(sql, new {Offset = offset, PageSize = pageSize, SocialNetwork = socialNetwork }, commandTimeout: 120))
                {
                    var result =
                        multi
                            .Read<PageSummaryDto, InfoSummaryDto, InfoSummaryDto,
                                PageSummaryDto>((c, mi, li) =>
                                {
                                    if (mi?.CreatedAt != DateTime.MinValue)
                                    {
                                        c.Followers = mi;
                                    }

                                    if (li?.CreatedAt != DateTime.MinValue)
                                    {
                                        c.Likes = li;
                                    }

                                    return c;
                                })
                            .ToList();

                    var pageReports = multi.Read<dynamic>();

                    foreach (var pageReport in pageReports)
                    {
                        var publicPage = result.First(x => x.Id == pageReport.PageId.ToString());
                        publicPage.Reports.Add(new SharedReportSummaryDto
                        {
                            Key = pageReport.Key,
                            Photo50 = pageReport.Photo50
                        });
                    }

                    var pageHashtags = multi.Read<dynamic>();

                    foreach (var pageHashtag in pageHashtags)
                    {
                        var publicPage = result.First(x => x.Id == pageHashtag.PageId.ToString());
                        publicPage.Hashtags.Add(pageHashtag.Name);
                    }

                    return result;
                }
            }
        }

        public long GetTotal(SocialNetwork socialNetwork)
        {
            var sql = "SELECT COUNT(*) FROM[dbo].[Page] [c] WHERE [c].[SocialNetwork] = @SocialNetwork";

            var result = GetFilteredAsync<long>(sql, new { SocialNetwork = socialNetwork }).Result;
            return result;
        }

        #endregion
    }
}
