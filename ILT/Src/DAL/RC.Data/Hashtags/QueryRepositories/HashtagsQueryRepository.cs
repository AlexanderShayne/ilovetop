﻿using System;
using Dal.QueryStack.Dapper;
using ILT.Contracts.Dal.Query.Hashtags.QueryRepositories;

namespace ILT.Data.Hashtags.QueryRepositories
{
    public class HashtagsQueryRepository : QueryRepositoryBase, IHashtagsQueryRepository
    {
        public bool Exists(string name)
        {
            var sql = "SELECT TOP(1) [h].[Name] FROM [dbo].[Hashtag] [h] WHERE [h].[Name] = @Name;";

            name = GetFilteredAsync<string>(sql, new {Name = name}).Result;

            return !String.IsNullOrEmpty(name);
        }
    }
}
