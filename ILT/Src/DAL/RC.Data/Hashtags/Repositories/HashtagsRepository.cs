﻿using System;
using Dal.DomainStack.Ef;
using Dal.DomainStack.Ef.Context;
using ILT.Contracts.Data.Hashtags.Repositories;
using ILT.Domain.Hashtags;

namespace ILT.Data.Hashtags.Repositories
{
    public class HashtagsRepository : RepositoryBase<Hashtag, Guid>, IHashtagsRepository
    {
        public HashtagsRepository(IDataContext dataContext) : base(dataContext)
        {
        }
    }
}
