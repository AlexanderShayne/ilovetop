﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Dal.QueryStack.Dapper;
using Dapper;
using Domain.Entities.Images;
using ILT.Contracts.Data.Reports.Repositories;
using ILT.Domain.Hashtags;
using ILT.Domain.Posts;
using ILT.Domain.PublicPages;
using ILT.Domain.Reports;
using Z.Dapper.Plus;

namespace ILT.Data.Reports.Repositories
{
    public class SystemReportsRepository : QueryRepositoryBase, ISystemReportsRepository
    {
        string deleteUnusedData = @"
            DELETE [dbo].[Post] FROM [dbo].[Post]
            LEFT JOIN [dbo].[ReportPost] ON [ReportPost].[PostId] = [Post].[Id] AND [ReportPost].[SocialNetwork] = [Post].[SocialNetwork]
            WHERE [ReportPost].[PostId] IS NULL;

            DELETE [dbo].[Image] FROM [dbo].[Image]
            LEFT JOIN [dbo].[Page] ON [dbo].[Page].[AvatarId] = [dbo].[Image].[Id]
            LEFT JOIN [dbo].[Report] ON [dbo].[Report].[AvatarId] = [dbo].[Image].[Id]
            LEFT JOIN [dbo].[Post] ON [dbo].[Post].[AvatarId] = [dbo].[Image].[Id]
            WHERE [dbo].[Page].[AvatarId] IS NULL AND [dbo].[Report].[AvatarId] IS NULL AND [dbo].[Post].[AvatarId] IS NULL;

            DELETE FROM [dbo].[LikesInfo] WHERE [CreatedAt] <= DATEADD(DAY, -32, GETDATE());
            DELETE FROM [dbo].[FollowersInfo] WHERE [CreatedAt] <= DATEADD(DAY, -32, GETDATE());";

        static readonly Object obj = new Object();

        #region Public Methods

        public void MergeReports(IEnumerable<Report> reports)
        {
            var deleteReport = @"
                IF @IsUnique = 1
                    DELETE FROM [dbo].[Report] WHERE [Key] = @Key AND [SocialNetwork] = @SocialNetwork
                ELSE
                    DELETE FROM [dbo].[Report] WHERE [Key] = @Key AND [SocialNetwork] = @SocialNetwork AND [PeriodStart] = @PeriodStart AND [PeriodEnd] = @PeriodEnd;";

            var posts = reports.SelectMany(x => x.Posts).Distinct(new Post.EqualityComparer()).ToList();
            var images =
                reports.Where(x => x.Avatar != null).Select(x => x.Avatar).Concat(posts.Where(p => p.Avatar != null).Select(p => p.Avatar)).Distinct(new Image.EqualityComparer()).ToList();

            //TODO: remove  hack or VK
            foreach (var post in posts.Where(x => x.Avatar == null))
            {
                post.Avatar = images.First();
            }

            using (var dbCon = GetDbConnection())
            {
                dbCon.Open();
                dbCon.Execute(deleteReport, reports.Select(x => new { x.IsUnique, x.Key, PeriodStart = x.Period.Start, PeriodEnd = x.Period.End, x.SocialNetwork }));
                dbCon.BulkMerge(images);
                dbCon.BulkMerge(images.SelectMany(x => x.Versions));
                dbCon.BulkInsert("zzz_null;ILT.Domain.Reports.Report", reports);
                dbCon.BulkInsert(reports.SelectMany(x => x.ReportPages));
                dbCon.BulkMerge(posts);
                dbCon.BulkMerge(reports.SelectMany(x => x.ReportPosts));
                dbCon.Execute(deleteUnusedData);
            }
        }

        public void MergePages(IEnumerable<Page> pages)
        {
            lock (obj)
            {
                using (var dbCon = GetDbConnection())
                {
                    dbCon.Open();
                    dbCon.BulkInsert(pages.Select(x => x.Avatar));
                    dbCon.BulkMerge(pages);
                    dbCon.BulkInsert(pages.SelectMany(x => x.Avatar.Versions));
                }
            }
        }

        public void LogicallyDeleteAllReports(SocialNetwork socialNetwork)
        {
            var sql = "UPDATE [dbo].[Report] SET [IsDeleted] = 1 WHERE [SocialNetwork] = @SocialNetwork;";
            using (var dbCon = GetDbConnection())
            {
                dbCon.Open();
                dbCon.Execute(sql, new { SocialNetwork  = socialNetwork });
            }
        }

        public void UpdateIsPostedInfo(Report report)
        {
            using (var dbCon = GetDbConnection())
            {
                dbCon.Open();
                dbCon.BulkUpdate(report.ReportPosts);
            }
        }

        public Report GetReport(string reportKey, SocialNetwork socialNetwork)
        {
            var sql = @"
                DECLARE @ReportId UNIQUEIDENTIFIER = (SELECT [r].[Id] FROM [dbo].[Report] [r] WHERE [r].[SocialNetwork] = @SocialNetwork AND [r].[Key] = @ReportKey AND [r].[IsDeleted] = 0);
                
                SELECT [r].[Id]
                      ,[r].[Key]
                      ,[r].[SocialNetwork]
                      ,[r].[Order]
                      ,[r].[PeriodType]
                      ,[r].[CreatedAt]
                      ,[r].[ModifiedAt]
                  FROM [dbo].[Report] [r]
                  WHERE [r].[Id] = @ReportId;

                SELECT
                    [rp].[PageId]
                    ,[rp].[ReportId]
                    ,[rp].[PostId]
                    ,[rp].[Top]
                    ,[rp].[SocialNetwork]
                    ,[rp].[IsPosted]
                    ,[p].[Id]
                    ,[p].[Permalink]
                    ,[p].[Text]
                FROM
                    [dbo].[ReportPost] [rp]
                JOIN
                    [dbo].[Post] [p] ON [p].[Id] = [rp].[PostId] AND [p].[SocialNetwork] = [rp].[SocialNetwork]
                  WHERE [rp].[ReportId] = @ReportId;

                SELECT [Name] = [h].[Name] FROM [ReportHashtag] [rh] JOIN [Hashtag] [h] ON [h].[Id] = [rh].[HashtagId] WHERE ([rh].[ReportPattern] = '*' OR [rh].[ReportPattern] = @ReportKey) AND [rh].[SocialNetwork] = @SocialNetwork;

                SELECT [PostId] = [p].[Id]
                      ,[Name] = [h].[Name]
                  FROM [dbo].[ReportPost] [rp]
                  LEFT JOIN [dbo].[Post] [p] ON [p].[Id] = [rp].[PostId]
                  LEFT JOIN [dbo].[PageHashtag] [ch] ON [ch].[PageId] = [p].[PageId]
                  JOIN [dbo].[Hashtag] [h] ON [h].[Id] = [ch].[HashtagId]
                  WHERE [rp].[ReportId] = @ReportId;
            ";

            Report result;

            using (var dbCon = GetDbConnection())
            {
                dbCon.Open();

                var multiQuery = dbCon.QueryMultiple(sql, new { ReportKey = reportKey, SocialNetwork = socialNetwork });

                result = multiQuery.Read<Report>().FirstOrDefault();
                if (result == null) return null;

                result.ReportPosts = multiQuery.Read<ReportPost, Post, ReportPost>((rp, p) =>
                {
                    rp.Post = p;
                    return rp;
                }).ToList();

                result.Hashtags = multiQuery.Read<Hashtag>().ToList();

                var postHashtags = multiQuery.Read<dynamic>();

                foreach (var postHashtag in postHashtags)
                {
                    var reportPost = result.ReportPosts.First(x => x.PostId == postHashtag.PostId);

                    if (reportPost.Post == null)
                    {
                        reportPost.Post = new Post
                        {
                            Id = reportPost.PostId,
                            Hashtags = new List<Hashtag>()
                        };
                    }

                    reportPost.Post.Hashtags.Add(new Hashtag
                    {
                        Name = postHashtag.Name
                    });
                }
            }

            return result;
        }

        public Page[] GetPages(IEnumerable<long> ids, SocialNetwork socialNetwork)
        {
            var sql = $@"
                SELECT
                    [c].[Id]
                   ,[c].[Name]
                   ,[c].[ScreenName]
                   ,[c].[SocialNetwork]
                   ,[c].[ExcludeFromReports]
                   ,[li].[Id]
                   ,[li].[PageId]
                   ,[li].[Count]
                   ,[li].[CreatedAt]
                   ,[fi].[Id]
                   ,[fi].[PageId]
                   ,[fi].[Count]
                   ,[fi].[CreatedAt]
                   ,[img].[Id]
                   ,[img].[UrlTemplate]
                FROM
                    [dbo].[Page] [c]
                CROSS APPLY(
                    SELECT TOP(1)
                    *
                    FROM
                    [dbo].[LikesInfo] [li]
                    WHERE [li].[SocialNetwork] = @SocialNetwork AND [li].[PageId] = [c].[Id]
                    ORDER BY
                    [li].[CreatedAt]
                    DESC
                ) li
                CROSS APPLY(
                    SELECT TOP(1)
                    *
                    FROM [dbo].[FollowersInfo] [fi]
                    WHERE
                    [fi].[SocialNetwork] = @SocialNetwork AND [fi].[PageId] = [c].[Id]
                    ORDER BY
                    [fi].[CreatedAt]
                    DESC
                ) fi
                JOIN [dbo].[Image] [img] ON [img].[Id] = [c].[AvatarId]
                WHERE [c].[SocialNetwork] = @SocialNetwork AND [c].[Id] IN ({String.Join(",", ids)});";

            using (var dbCon = GetDbConnection())
            {
                dbCon.Open();

                IEnumerable<Page> response =
                    dbCon
                        .Query
                        <Page, PageInfo, PageInfo, Image, Page>(sql,
                                (c, li, fi, img) =>
                                {
                                    c.LikesInfo = li;
                                    c.FollowersInfo = fi;
                                    c.Avatar = img;
                                    return c;
                                },
                                new { SocialNetwork = socialNetwork });

                return response.ToArray();
            }
        }

        public void MergePageLikes(IEnumerable<Page> pages)
        {
            lock (obj)
            {
                using (var dbCon = GetDbConnection())
                {
                    dbCon.Open();
                    dbCon.Execute(GetPageInfoInsertOrUpdateSql("LikesInfo"),
                        pages.Where(x => x.LikesInfo != null).Select(x => x.LikesInfo));
                }
            }
        }

        public void MergePageFollowers(IEnumerable<Page> pages)
        {
            lock (obj)
            {
                using (var dbCon = GetDbConnection())
                {
                    dbCon.Open();
                    dbCon.Execute(GetPageInfoInsertOrUpdateSql("FollowersInfo"),
                        pages.Where(x => x.FollowersInfo != null).Select(x => x.FollowersInfo));
                }
            }
        }

        #endregion

        #region Private Methods

        static string GetPageInfoInsertOrUpdateSql(string pageInfoTableName)
        {
            var insertOrUpdatePageInfo = $@"
                BEGIN TRAN

                    UPDATE [dbo].[{pageInfoTableName}]
                    SET [Count] = @Count,[DayChanges] = @DayChanges,[WeekChanges] = @WeekChanges,[MonthChanges] = @MonthChanges
                    WHERE [PageId] = @PageId AND [SocialNetwork] = @SocialNetwork AND [CreatedAt] = @CreatedAt

               IF @@ROWCOUNT = 0
                   BEGIN
                    INSERT INTO [dbo].[{pageInfoTableName}]
                                ([Id]
                                ,[PageId]
                                ,[SocialNetwork]
                                ,[Count]
                                ,[CreatedAt]
                                ,[DayChanges]
                                ,[WeekChanges]
                                ,[MonthChanges])
                            VALUES
                                (@Id
                                ,@PageId
                                ,@SocialNetwork
                                ,@Count
                                ,@CreatedAt
                                ,@DayChanges
                                ,@WeekChanges
                                ,@MonthChanges)
                   END
                COMMIT TRAN";

            return insertOrUpdatePageInfo;
        }

        #endregion
    }
}