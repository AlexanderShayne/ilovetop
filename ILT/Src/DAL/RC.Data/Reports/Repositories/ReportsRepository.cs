using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Dal.DomainStack.Ef;
using Dal.DomainStack.Ef.Context;
using ILT.Contracts.Data.Reports.Repositories;
using ILT.Domain.Reports;

namespace ILT.Data.Reports.Repositories
{
    public class ReportsRepository : RepositoryBase<Report, Guid>, IReportsRepository
    {
        public ReportsRepository(IDataContext dataContext) : base(dataContext)
        {
        }

        #region Public Methods

        public IEnumerable<Report> ReadReportsByPostId(SocialNetwork socialNetwork, long pageId, long postId)
        {
            var reports =
                GetDbSet()
                    .Include(x => x.ReportPosts)
                    .Where(x => !x.IsDeleted && x.SocialNetwork == socialNetwork && x.ReportPosts.Any(p => p.PostId == postId && p.PageId == pageId));

            return reports;
        }

        #endregion
    }
}