﻿using System;
using System.Collections.Generic;
using Contracts.Services.Infrastructure.SocialNetworks;
using Dal.QueryStack.Dapper;
using ILT.Contracts.Dal.Query.Reports.QueryRepositories;

namespace ILT.Data.Reports.QueryRepositories
{
    public class SystemReportsQueryRepository : QueryRepositoryBase, ISystemReportsQueryRepository
    {
        #region Public Methods

        public IEnumerable<long> GetPublicPageIds(SocialNetwork socialNetwork, int page, int pageSize)
        {
            var sql = @"
                SELECT [Id]
                FROM [dbo].[Page]
                WHERE [SocialNetwork] = @SocialNetwork
                ORDER BY [Id] ASC
                OFFSET @Offset ROWS FETCH NEXT @Next ROWS ONLY";

            var offset = GetOffset(page, pageSize);

            var result = GetFilteredListAsync<long>(sql, new { Offset = offset, Next = pageSize, SocialNetwork = socialNetwork }).Result;
            return result;
        }

        public long GetPublicPagesTotal(SocialNetwork socialNetwork)
        {
            var sql = "SELECT COUNT(*) FROM [dbo].[Page] WHERE [SocialNetwork] = @SocialNetwork";

            var result = GetFilteredAsync<long>(sql, new { SocialNetwork = socialNetwork }).Result;
            return result;
        }

        public int? GetFollowersCount(long pageId, SocialNetwork socialNetwork, DateTime date)
        {
            var sql = @"
                SELECT [mi].[Count]
                FROM [dbo].[FollowersInfo] [mi]
                WHERE [mi].[PageId] = @Id AND [mi].[SocialNetwork] = @SocialNetwork AND [mi].[CreatedAt] = @Date
                ";

            var result = GetFilteredAsync<int?>(sql, new { Id = pageId, Date = date, SocialNetwork = socialNetwork }).Result;
            return result;
        }

        public int? GetLikesCount(long pageId, SocialNetwork socialNetwork, DateTime date)
        {
            var sql = @"
                SELECT [li].[Count]
                FROM [dbo].[LikesInfo] [li]
                WHERE [li].[PageId] = @Id AND [li].[SocialNetwork] = @SocialNetwork AND [li].[CreatedAt] = @Date
                ";

            var result = GetFilteredAsync<int?>(sql, new { Id = pageId, Date = date, SocialNetwork = socialNetwork }).Result;
            return result;
        }

        public int? GetPageTop(SocialNetwork socialNetwork, string reportKey, long pageId, DateTime end)
        {
            var sql = @"
                SELECT [rc].[Top]
                  FROM [dbo].[Report] [r]
                  JOIN [dbo].[ReportPage] [rc] on [rc].[ReportId] = [r].[Id] AND [rc].[PageId] = @PageId
                  WHERE [r].[Key] = @ReportKey AND @SocialNetwork = @SocialNetwork AND [r].[PeriodEnd] = @PeriodEnd";

            var result = GetFilteredAsync<int?>(sql, new { PageId = pageId, PeriodEnd = end, ReportKey = reportKey, SocialNetwork = socialNetwork }).Result;
            return result;
        }

        #endregion
    }
}
