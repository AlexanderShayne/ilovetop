using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Dal.QueryStack.Dapper;
using Dapper;
using Domain.ValueObjects.DateTimeRanges;
using ILT.Contracts.Dal.Query.Reports.Dtos.View;
using ILT.Contracts.Dal.Query.Reports.QueryRepositories;
using ILT.Contracts.Dal.Query.Shared.Dtos.View;
using ILT.Contracts.Domain.Reports;

namespace ILT.Data.Reports.QueryRepositories
{
    public class ReportsQueryRepository : QueryRepositoryBase, IReportsQueryRepository
    {
        readonly IDateTimeRangeFactory periodFactory;

        public ReportsQueryRepository(IDateTimeRangeFactory periodFactory)
        {
            this.periodFactory = periodFactory;
        }

        #region Public Methods

        public IEnumerable<ReportSummaryDto> Query(SocialNetwork socialNetwork, int page, int pageSize, ReportCategory? reportCategoryFilter)
        {
            var offset = GetOffset(page, pageSize);
            var condition = GetCondition(reportCategoryFilter);

            var getReportsSql =
                $@"
                DECLARE @TempId TABLE
                (
                    Id UNIQUEIDENTIFIER
                );

                INSERT INTO
                    @TempId
                SELECT
                    [r].[Id]
                FROM
                    [dbo].[ReportView] [r]
                WHERE
                    [r].[SocialNetwork] = @SocialNetwork {condition}
                ORDER BY
                    [r].[ReportCategory] ASC, [r].[Order] ASC
                OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY;

                SELECT
                     [r].[Id]
                    ,[r].[Key]
                    ,[r].[SocialNetwork]
                    ,[r].[CreatedAt]
                    ,[r].[Order]
                    ,[r].[Photo50]
                    ,[r].[Photo200]
                    ,[r].[Total]
                    ,[r].[PeriodStart]
                    ,[r].[PeriodEnd]
                    ,[r].[ReportCategory]
                FROM
                    @TempId [Temp]
                JOIN
                    [dbo].[ReportView] [r] ON [r].[Id] = [Temp].[Id];

                SELECT
                     [rc].[ReportId]
                    ,[pv].[Id]
                    ,[pv].[Name]
                    ,[pv].[ScreenName]
                    ,[pv].[Permalink]
                    ,[pv].[Photo50]
                    ,[pv].[Photo200]
                    ,[rc].[Top]
                    ,[rc].[TopChanges]
                    ,[Id] = [pv].[Id]
                    ,[Count] = [pv].[FollowersCount]
                    ,[DayChanges] = [pv].[FollowersDayChanges]
                    ,[WeekChanges] = [pv].[FollowersWeekChanges]
                    ,[MonthChanges] = [pv].[FollowersMonthChanges]
                    ,[Id] = [pv].[Id]
                    ,[Count] = [pv].[LikesCount]
                    ,[DayChanges] = [pv].[LikesDayChanges]
                    ,[WeekChanges] = [pv].[LikesWeekChanges]
                    ,[MonthChanges] = [pv].[LikesMonthChanges]
                FROM
                (
                    SELECT
                         [ReportId] = [Temp].[Id]
                        ,[rc].[PageId]
                        ,[rc].[Top]
                        ,[rc].[TopChanges]
                        ,Rank() OVER (PARTITION BY [Temp].[Id] ORDER BY [rc].[Top] ASC ) AS Rank
                    FROM
                        @TempId [Temp]
                    JOIN
                        [dbo].[ReportPage] [rc] ON [rc].[ReportId] = [Temp].[Id]
                ) [rc]
                JOIN
                    [dbo].[PageView] [pv] ON [pv].[Id] = [rc].[PageId] AND [pv].[SocialNetwork] = @SocialNetwork
                WHERE
                    Rank <= 12;

                SELECT
                     [p].[ReportId]
                    ,[p].[Id]
                    ,[p].[PageId]
                    ,[p].[LikesCount]
                    ,[p].[CommentsCount]
                    ,[p].[RepostsCount]
                    ,[p].[Text]
                    ,[p].[Permalink]
                    ,[p].[Top]
                    ,[Photo130] = REPLACE([p].[UrlTemplate], '{"{0}"}', (SELECT [TemplateValue] FROM [dbo].[ImageVersion] WHERE ([ImageId] = [p].[AvatarId]) AND ([Size] = 130)))
                    ,[Photo604] = REPLACE([p].[UrlTemplate], '{"{0}"}', (SELECT [TemplateValue] FROM [dbo].[ImageVersion] WHERE ([ImageId] = [p].[AvatarId]) AND ([Size] = 604)))
                FROM
                (
                    SELECT
                         [ReportId] = [Temp].[Id]
                        ,[p].[Id]
                        ,[p].[PageId]
                        ,[p].[LikesCount]
                        ,[p].[CommentsCount]
                        ,[p].[RepostsCount]
                        ,[p].[Text]
                        ,[p].[Permalink]
                        ,[p].[AvatarId]
                        ,[img].[UrlTemplate]
                        ,[rp].[Top]
                        ,Rank() OVER (PARTITION BY [Temp].[Id] ORDER BY [rp].[Top] ASC) AS Rank
                    FROM
                        @TempId [Temp]
                    JOIN
                        [dbo].[ReportPost] [rp] ON [rp].[ReportId] = [Temp].[Id]
                    JOIN
                        [dbo].[Post] [p] ON [p].[Id] = [rp].[PostId] AND [p].[PageId] = [rp].[PageId] AND [p].[SocialNetwork] = @SocialNetwork
                    JOIN
                        [dbo].[Image] [img] ON [img].[Id] = [p].[AvatarId]
                ) [p]
                WHERE
                    Rank <= 12;

                SELECT [rh].[ReportPattern], [h].[Name] AS [Name] FROM [ReportHashtag] [rh] JOIN [Hashtag] [h] ON [h].[Id] = [rh].[HashtagId] WHERE [rh].[SocialNetwork] = @SocialNetwork;
                ";

            IEnumerable<ReportSummaryDto> result;

            using (var dbCon = GetDbConnection())
            {
                dbCon.Open();

                using (var multi = dbCon.QueryMultiple(getReportsSql, new { Offset = offset, PageSize = pageSize, SocialNetwork = socialNetwork }, commandTimeout: 120))
                {
                    result = multi.Read<ReportSummaryDto>().ToList();

                    multi
                        .Read<ReportPageSummaryDto, InfoSummaryDto, InfoSummaryDto, ReportPageSummaryDto>(
                        (c, mi, li) =>
                        {
                            c.Followers = mi;
                            c.Likes = li;

                            result.First(x => x.Id == c.ReportId).Pages.Add(c);

                            return c;
                        })
                        .ToList();

                    var posts = multi
                        .Read<ReportPostSummaryDto>().ToList();

                    foreach (var post in posts)
                    {
                        result.First(x => x.Id == post.ReportId).Posts.Add(post);
                    }

                    var hashtags = multi.Read<dynamic>();

                    foreach (var hashtag in hashtags)
                    {
                        if (String.Equals(hashtag.ReportPattern, "*", StringComparison.OrdinalIgnoreCase))
                        {
                            foreach (var reportSummaryDto in result)
                            {
                                reportSummaryDto.Hashtags.Add(hashtag.Name);
                            }
                        }
                        else
                        {
                            var report = result.FirstOrDefault(x => String.Equals(x.Key, hashtag.ReportPattern, StringComparison.OrdinalIgnoreCase));
                            report?.Hashtags.Add(hashtag.Name);
                        }
                    }
                }
            }

            return result;
        }

        public long GetTotal(SocialNetwork socialNetwork, ReportCategory? reportCategoryFilter)
        {
            var condition = GetCondition(reportCategoryFilter);
            var sql = $"SELECT COUNT(*) FROM[dbo].[Report] [r] WHERE [r].[IsDeleted] = 0 AND [r].[SocialNetwork] = @SocialNetwork {condition}";

            var result = GetFilteredAsync<long>(sql, new { SocialNetwork = socialNetwork }).Result;
            return result;
        }

        public ReportSummaryDto Read(SocialNetwork socialNetwork, string key, int page, int pageSize, int periodsAgo)
        {
            DateTime periodEnd = DateTime.Today;

            if (periodsAgo > 0)
            {
                var periodType =
                    GetFilteredAsync<DateTimeRangeType>(
                        "SELECT PeriodType FROM [dbo].[Report] WHERE [IsDeleted] = 0 AND [Key] = @Key AND [SocialNetwork] = @SocialNetwork", new { Key = key, SocialNetwork = socialNetwork }).Result;

                if (periodType == DateTimeRangeType.Day)
                {
                    periodsAgo += 1;
                }

                periodEnd = periodFactory.Create(periodType, periodsAgo).End;
            }

            var offset = (page - 1) * pageSize;

            string condition;

            if (periodsAgo > 0)
            {
                condition = "WHERE [IsDeleted] = 1 AND [SocialNetwork] = @SocialNetwork AND [Key] = @Key AND PeriodEnd = @PeriodEnd";
            }
            else
            {
                condition = "WHERE [IsDeleted] = 0 AND [SocialNetwork] = @SocialNetwork AND [Key] = @Key";
            }

            var sql = $@"
                DECLARE @ReportId UNIQUEIDENTIFIER;

                SELECT @ReportId = [Id] FROM [dbo].[Report] {condition};

                --REPORT
                SELECT [r].[Id]
                      ,[r].[Key]
                      ,[r].[CreatedAt]
                      ,[r].[Order]
                      ,[Photo50] = REPLACE([img].[UrlTemplate], '{"{0}"}', (SELECT [TemplateValue] FROM [dbo].[ImageVersion] WHERE [ImageId] = [img].[Id] AND [ImageVersion].[Size] = 50))
                      ,[Photo200] = REPLACE([img].[UrlTemplate], '{"{0}"}', (SELECT [TemplateValue] FROM [dbo].[ImageVersion] WHERE [ImageId] = [img].[Id] AND [ImageVersion].[Size] = 200))
                      ,[r].[ReportCategory]
                      ,[Total] = (SELECT MAX([t].[Total]) FROM (
                          SELECT COUNT(*) AS [Total] FROM [dbo].[ReportPost] WHERE [ReportPost].[ReportId] = [r].[Id]
                          UNION
                          SELECT COUNT(*) AS [Total] FROM [dbo].[ReportPage] WHERE [ReportPage].[ReportId] = [r].[Id]) [t])
                      ,[r].[Id]
                      ,[r].[PeriodStart]
                      ,[r].[PeriodEnd]
                  FROM [dbo].[Report] [r]
                  JOIN [dbo].[Image] [img] ON [img].[Id] = [r].[AvatarId]
                 WHERE [r].[Id] = @ReportId;

                 --REPORT HASHTAGS
                SELECT [h].[Name] FROM [ReportHashtag] [rh] JOIN [Hashtag] [h] ON [h].[Id] = [rh].[HashtagId] WHERE ([rh].[ReportPattern] = '*' OR [rh].[ReportPattern] = @Key) AND [rh].[SocialNetwork] = @SocialNetwork;

                --COMMUNITIES
                  SELECT [c].[Id]
                        ,[c].[Name]
                        ,[c].[ScreenName]
                        ,[c].[Permalink]
                        ,[c].[Photo50]
                        ,[c].[Photo200]
                        ,[rc].[Top]
                        ,[rc].[TopChanges]
                        ,[c].[Id]
                        ,[Count] = [c].[FollowersCount]
                        ,[DayChanges] = [c].[FollowersDayChanges]
                        ,[WeekChanges] = [c].[FollowersWeekChanges]
                        ,[MonthChanges] = [c].[FollowersMonthChanges]
                        ,[CreatedAt] = [c].[FollowersCreatedAt]
                        ,[c].[Id]
                        ,[Count] = [c].[LikesCount]
                        ,[DayChanges] = [c].[LikesDayChanges]
                        ,[WeekChanges] = [c].[LikesWeekChanges]
                        ,[MonthChanges] = [c].[LikesMonthChanges]
                        ,[CreatedAt] = [c].[LikesCreatedAt]
                    FROM [dbo].[PageView] [c]
                    JOIN [dbo].[ReportPage] [rc] ON [rc].[PageId] = [c].[Id] AND [rc].[SocialNetwork] = @SocialNetwork
                   WHERE [rc].[ReportId] = @ReportId
                ORDER BY [rc].[Top] ASC
                  OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY;


                  --COMMUNITY REPORTS
                  SELECT [PageId] = [c].[Id]
                        ,[r].[Key]
                        ,[r].[Photo50]
                        FROM
                  (SELECT [Id] = [rc].[PageId] 
                    FROM [dbo].[ReportPage] [rc]
                   WHERE [rc].[ReportId] = @ReportId
                ORDER BY [rc].[Top] ASC
                  OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY) [c]
                  JOIN [dbo].[ReportPage] [rc] ON [rc].[PageId] = [c].[Id] AND [rc].[SocialNetwork] = @SocialNetwork
                  JOIN [dbo].[ReportView] [r] ON [r].[Id] = [rc].[ReportId];

                --COMMUNITY HASHTAGS
                  SELECT [PageId] = [c].[Id]
                        ,[Name] = [h].[Name]
                        FROM
                  (SELECT [Id] = [rc].[PageId] 
                    FROM [dbo].[ReportPage] [rc]
                   WHERE [rc].[ReportId] = @ReportId
                ORDER BY [rc].[Top] ASC
                  OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY) [c]
                  JOIN [dbo].[PageHashtag] [ch] ON [ch].[PageId] = [c].[Id] AND [ch].[SocialNetwork] = @SocialNetwork
                  JOIN [dbo].[Hashtag] [h] ON [h].[Id] = [ch].[HashtagId];

                  --POSTS
                      SELECT [p].[Id]
                            ,[p].[LikesCount]
                            ,[p].[CommentsCount]
                            ,[p].[RepostsCount]
                            ,[p].[Text]
                            ,[p].[Permalink]
                            ,[p].[CreatedAt]
                            ,[p].[Top]
                            ,[Photo130] = REPLACE([img].[UrlTemplate], '{"{0}"}', (SELECT [TemplateValue] FROM [dbo].[ImageVersion] WHERE [ImageId] = [img].[Id] AND [ImageVersion].[Size] = 130))
                            ,[Photo604] = REPLACE([img].[UrlTemplate], '{"{0}"}', (SELECT [TemplateValue] FROM [dbo].[ImageVersion] WHERE [ImageId] = [img].[Id] AND [ImageVersion].[Size] = 604))
                            ,[pc].[Id]
                            ,[pc].[Photo50]
                            ,[pc].[Name]
                            ,[pc].[ScreenName]
                        FROM [dbo].[Report] [r]

                 OUTER APPLY (SELECT [p].[Id]
                                    ,[p].[LikesCount]
                                    ,[p].[CommentsCount]
                                    ,[p].[RepostsCount]
                                    ,[p].[Text]
                                    ,[p].[Permalink]
                                    ,[p].[CreatedAt]
                                    ,[p].[PageId]
                                    ,[p].[AvatarId]
                                    ,[rp].[Top]
                                FROM [dbo].[ReportPost] [rp] 
                                JOIN [dbo].[Post] [p] ON [rp].[PostId] = [p].[Id] AND [rp].[PageId] = [p].[PageId] AND [rp].[SocialNetwork] = @SocialNetwork
                               WHERE [rp].[ReportId] = [r].[Id]
                            ORDER BY [rp].[Top] ASC
                               OFFSET @Offset ROWS FETCH NEXT @PageSize ROWS ONLY) [p]

                    LEFT JOIN [dbo].[Image] [img] ON [img].[Id] = [p].[AvatarId]
                    LEFT JOIN [dbo].[PageView] [pc] ON [pc].[Id] = [p].[PageId] AND [pc].[SocialNetwork] = @SocialNetwork

                    WHERE [r].[Id] = @ReportId

                ORDER BY [p].[Top] ASC;
            ";

            ReportSummaryDto result;

            using (var dbCon = GetDbConnection())
            {
                dbCon.Open();

                using (var multi = dbCon.QueryMultiple(sql, new { Key = key, Offset = offset, PageSize = pageSize, PeriodEnd = periodEnd, SocialNetwork = socialNetwork }, commandTimeout: 120))
                {
                    result = multi.Read<ReportSummaryDto>().FirstOrDefault();

                    if (result == null) return null;

                    result.Hashtags = multi.Read<string>().ToArray();
                    result.Pages =
                        multi
                        .Read<ReportPageSummaryDto, InfoSummaryDto, InfoSummaryDto, ReportPageSummaryDto>(
                            (c, mi, li) =>
                            {
                                c.Followers = mi;
                                c.Likes = li;
                                return c;
                            })
                        .ToList();

                    var pageReports = multi.Read<dynamic>();

                    foreach (var pageReport in pageReports)
                    {
                        var publicPage = result.Pages.First(x => x.Id == pageReport.PageId.ToString());

                        publicPage.Reports = publicPage.Reports ?? new List<SharedReportSummaryDto>();

                        publicPage.Reports.Add(new ReportSummaryDto
                        {
                            Key = pageReport.Key,
                            Photo50 = pageReport.Photo50
                        });
                    }

                    var pageHashtags = multi.Read<dynamic>();

                    foreach (var pageHashtag in pageHashtags)
                    {
                        var publicPage = result.Pages.First(x => x.Id == pageHashtag.PageId.ToString());
                        publicPage.Hashtags = publicPage.Hashtags ?? new List<string>();
                        publicPage.Hashtags.Add(pageHashtag.Name);
                    }

                    multi
                        .Read
                        <ReportPostSummaryDto, SharedPageSummaryDto,
                            ReportPostSummaryDto>(
                                (p, c) =>
                                {
                                    var pos = result.Posts.FirstOrDefault(x => x.Id == p.Id);

                                    if (pos == null)
                                    {
                                        p.Page = c;
                                        result.Posts.Add(p);
                                    }
                                    else
                                    {
                                        p = pos;
                                    }

                                    if (!String.IsNullOrEmpty(c?.Id))
                                    {
                                        p.Page = c;
                                    }

                                    return p;
                                })
                                .ToList();
                }
            }

            return result;
        }

        #endregion

        #region Private Methods

        static string GetCondition(ReportCategory? reportCategoryFilter)
        {
            var condition = String.Empty;
            if (reportCategoryFilter.HasValue)
            {
                condition = $"AND [r].[ReportCategory] = {(int)reportCategoryFilter}";
            }

            return condition;
        }

        #endregion
    }
}