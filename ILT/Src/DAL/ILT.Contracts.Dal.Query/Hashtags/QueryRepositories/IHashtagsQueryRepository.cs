﻿using Contracts.Dal;
using Contracts.Dal.QueryStack;

namespace ILT.Contracts.Dal.Query.Hashtags.QueryRepositories
{
    public interface IHashtagsQueryRepository : IQueryRepository
    {
        bool Exists(string name);
    }
}
