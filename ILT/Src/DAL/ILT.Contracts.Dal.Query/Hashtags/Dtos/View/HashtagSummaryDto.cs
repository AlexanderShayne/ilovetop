﻿using System;
using System.Collections.Generic;
using ILT.Contracts.Dal.Query.Shared.Dtos.View;

namespace ILT.Contracts.Dal.Query.Hashtags.Dtos.View
{
    public class HashtagSummaryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public bool ContainsAll { get; set; }

        public IEnumerable<SharedReportSummaryDto> Reports { get; set; }
    }
}
