﻿using System;
using System.Collections.Generic;
using Contracts.Services.Infrastructure.SocialNetworks;

namespace ILT.Contracts.Dal.Query.Reports.QueryRepositories
{
    public interface ISystemReportsQueryRepository
    {
        IEnumerable<long> GetPublicPageIds(SocialNetwork socialNetwork, int page, int pageSize);
        long GetPublicPagesTotal(SocialNetwork socialNetwork);
        int? GetFollowersCount(long pageId, SocialNetwork socialNetwork, DateTime date);
        int? GetLikesCount(long pageId, SocialNetwork socialNetwork, DateTime date);
        int? GetPageTop(SocialNetwork socialNetwork, string reportKey, long pageId, DateTime end);
    }
}