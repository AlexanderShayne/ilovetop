﻿using System.Collections.Generic;
using Contracts.Dal;
using Contracts.Dal.QueryStack;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Dal.Query.Reports.Dtos.View;
using ILT.Contracts.Domain.Reports;

namespace ILT.Contracts.Dal.Query.Reports.QueryRepositories
{
    public interface IReportsQueryRepository : IQueryRepository
    {
        IEnumerable<ReportSummaryDto> Query(SocialNetwork socialNetwork, int page, int pageSize, ReportCategory? reportCategoryFilter);
        long GetTotal(SocialNetwork socialNetwork, ReportCategory? reportCategoryFilter);
        ReportSummaryDto Read(SocialNetwork socialNetwork, string key, int page, int pageSize, int periodsAgo);
    }
}
