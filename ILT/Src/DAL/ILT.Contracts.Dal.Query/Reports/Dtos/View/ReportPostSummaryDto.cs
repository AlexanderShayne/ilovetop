using System;

namespace ILT.Contracts.Dal.Query.Reports.Dtos.View
{
    public class ReportPostSummaryDto : PostSummaryDto
    {
        public Guid ReportId { get; set; }
        public int Top { get; set; }
    }
}