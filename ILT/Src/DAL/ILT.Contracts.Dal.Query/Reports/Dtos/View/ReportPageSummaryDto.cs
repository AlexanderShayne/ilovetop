using System;
using ILT.Contracts.Dal.Query.Shared.Dtos.View;

namespace ILT.Contracts.Dal.Query.Reports.Dtos.View
{
    public class ReportPageSummaryDto : SharedPageSummaryDto
    {
        public Guid ReportId { get; set; }
        public int Top { get; set; }
        public int TopChanges { get; set; }
    }
}