﻿using System;
using ILT.Contracts.Dal.Query.Shared.Dtos.View;

namespace ILT.Contracts.Dal.Query.Reports.Dtos.View
{
    public class PostSummaryDto
    {
        public string Id { get; set; }
        public string PageId { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Text { get; set; }
        public string Permalink { get; set; }

        public int CommentsCount { get; set; }
        public int LikesCount { get; set; }
        public int RepostsCount { get; set; }

        public string Photo130 { get; set; }
        public string Photo604 { get; set; }

        public SharedPageSummaryDto Page { get; set; }
    }
}