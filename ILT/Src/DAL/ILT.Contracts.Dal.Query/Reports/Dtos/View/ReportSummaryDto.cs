﻿using System;
using System.Collections.Generic;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Dal.Query.Shared.Dtos.View;
using ILT.Contracts.Domain.Reports;

namespace ILT.Contracts.Dal.Query.Reports.Dtos.View
{
    public class ReportSummaryDto : SharedReportSummaryDto
    {
        public ReportSummaryDto()
        {
            Pages = new List<ReportPageSummaryDto>();
            Posts = new List<ReportPostSummaryDto>();
            Hashtags = new List<string>();
        }

        public DateTime CreatedAt { get; set; }
        public int Total { get; set; }
        public int Order { get; set; }
        public ReportCategory ReportCategory { get; set; }
        public SocialNetwork SocialNetwork { get; set; }

        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }

        public ICollection<string> Hashtags { get; set; }
        public ICollection<ReportPageSummaryDto> Pages { get; set; }
        public ICollection<ReportPostSummaryDto> Posts { get; set; }
    }
}
