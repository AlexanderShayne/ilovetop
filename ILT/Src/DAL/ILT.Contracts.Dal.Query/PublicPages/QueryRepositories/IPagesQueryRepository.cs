﻿using System.Collections.Generic;
using Contracts.Dal;
using Contracts.Dal.QueryStack;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Dal.Query.PublicPages.Dtos.View;

namespace ILT.Contracts.Dal.Query.PublicPages.QueryRepositories
{
    public interface IPagesQueryRepository : IQueryRepository
    {
        IEnumerable<PageSummaryDto> Query(SocialNetwork socialNetwork, int page, int pageSize);
        long GetTotal(SocialNetwork socialNetwork);
    }
}
