using System.Collections.Generic;

namespace ILT.Contracts.Dal.Query.Shared.Dtos.View
{
    public class SharedPageSummaryDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ScreenName { get; set; }
        public string Permalink { get; set; }
        public bool ExcludeFromReports { get; set; }

        public string Photo50 { get; set; }
        public string Photo200 { get; set; }

        public ICollection<string> Hashtags { get; set; } = new List<string>();

        public InfoSummaryDto Followers { get; set; }
        public InfoSummaryDto Likes { get; set; }

        public ICollection<SharedReportSummaryDto> Reports { get; set; } = new List<SharedReportSummaryDto>();
    }
}