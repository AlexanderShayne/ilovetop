using System;

namespace ILT.Contracts.Dal.Query.Shared.Dtos.View
{
    public class SharedReportSummaryDto
    {
        public Guid Id { get; set; }
        public string Key { get; set; }
        public string Permalink { get; set; }
        public string Photo50 { get; set; }
        public string Photo200 { get; set; }
    }
}