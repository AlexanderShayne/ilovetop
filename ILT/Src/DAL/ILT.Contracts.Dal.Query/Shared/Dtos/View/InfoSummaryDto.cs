using System;

namespace ILT.Contracts.Dal.Query.Shared.Dtos.View
{
    public class InfoSummaryDto
    {
        public int Count { get; set; }
        public int? DayChanges { get; set; }
        public int? WeekChanges { get; set; }
        public int? MonthChanges { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}