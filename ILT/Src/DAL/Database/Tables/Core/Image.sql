﻿CREATE TABLE [dbo].[Image]
(
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [UrlTemplate]       NVARCHAR(4000) NOT NULL DEFAULT('{0}')
    CONSTRAINT [PK_Image_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
)
