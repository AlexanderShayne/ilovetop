﻿CREATE TABLE [dbo].[Log] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [Date]         DATETIME2 (7)  NOT NULL,
    [Thread]       VARCHAR (8000) NOT NULL,
    [Level]        VARCHAR (50)   NOT NULL,
    [Logger]       VARCHAR (255)  NOT NULL,
    [Message]      VARCHAR (8000) NOT NULL,
    [Exception]    VARCHAR (8000) NOT NULL,
    CONSTRAINT [PK_Log_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

