﻿CREATE TABLE [dbo].[ImageVersion]
(
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [ImageId]           UNIQUEIDENTIFIER,
    [Size]              INT NOT NULL DEFAULT(0),
    [TemplateValue]     NVARCHAR(4000) NOT NULL DEFAULT('')
    CONSTRAINT [PK_ImageVersion_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ImageVersion_ImageId_Image_Id] FOREIGN KEY ([ImageId]) REFERENCES [dbo].[Image] ([Id]) ON DELETE CASCADE
)
