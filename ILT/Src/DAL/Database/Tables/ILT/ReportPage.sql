﻿CREATE TABLE [dbo].[ReportPage]
(
    [ReportId]      UNIQUEIDENTIFIER NOT NULL,

    [PageId]        BIGINT NOT NULL,
    [SocialNetwork] INT NOT NULL,

    [Top]           INT NOT NULL DEFAULT(0),
    [TopChanges]    INT NOT NULL DEFAULT(0),
    CONSTRAINT [PK_ReportPage_Id] PRIMARY KEY ([ReportId], [PageId], [SocialNetwork]),
    CONSTRAINT [FK_ReportPage_ReportId_Report_Id] FOREIGN KEY ([ReportId]) REFERENCES [dbo].[Report] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ReportPage_PageId_SocialNetwork_Page_Id] FOREIGN KEY ([PageId], [SocialNetwork]) REFERENCES [dbo].[Page] ([Id], [SocialNetwork]) ON DELETE CASCADE
)
