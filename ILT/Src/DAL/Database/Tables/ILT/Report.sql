﻿CREATE TABLE [dbo].[Report]
(
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [AvatarId]          UNIQUEIDENTIFIER NOT NULL,
    [Key]               NVARCHAR (250) NOT NULL DEFAULT '',
    [Order]             INT NOT NULL DEFAULT 0,
    [PeriodType]        INT NOT NULL DEFAULT 0,
    [SocialNetwork]     INT NOT NULL,
    [ReportCategory]    INT NOT NULL DEFAULT 0,
    [Permalink]         NVARCHAR (4000) NOT NULL DEFAULT '',

    [PeriodStart]       DATE NOT NULL DEFAULT GETDATE(),
    [PeriodEnd]         DATE NOT NULL DEFAULT GETDATE(),

    [IsPosted]          BIT NOT NULL DEFAULT 0,

    [CreatedAt]         DATE NOT NULL DEFAULT GETDATE(),
    [ModifiedAt]        DATE NOT NULL DEFAULT GETDATE(),
    [IsDeleted]         BIT NOT NULL DEFAULT 0,
    [Ts]                ROWVERSION NOT NULL,
    CONSTRAINT [PK_Report_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Report_AvatarId_Image_Id] FOREIGN KEY ([AvatarId]) REFERENCES [dbo].[Image] ([Id])
)
