﻿CREATE VIEW [dbo].[PageView]
	AS
	SELECT        dbo.Page.Id, dbo.Page.SocialNetwork, dbo.Page.ExcludeFromReports, dbo.Page.[Permalink], dbo.Page.Name, dbo.Page.ScreenName, dbo.Page.CreatedAt
	,REPLACE(dbo.Image.UrlTemplate, '{0}', (SELECT TemplateValue FROM dbo.ImageVersion WHERE (ImageId = dbo.Image.Id) AND (Size = 50))) AS Photo50
	,REPLACE(dbo.Image.UrlTemplate, '{0}', (SELECT TemplateValue FROM dbo.ImageVersion WHERE (ImageId = dbo.Image.Id) AND (Size = 200))) AS Photo200

                    ,[mi].[Count] AS FollowersCount
                    ,[mi].[DayChanges] AS FollowersDayChanges
                    ,[mi].[WeekChanges] AS FollowersWeekChanges
                    ,[mi].[MonthChanges] AS FollowersMonthChanges
					,[mi].CreatedAt AS FollowersCreatedAt

                    ,[li].[Count] AS LikesCount
                    ,[li].[DayChanges] AS LikesDayChanges
                    ,[li].[WeekChanges] AS LikesWeekChanges
                    ,[li].[MonthChanges] AS LikesMonthChanges
					,[li].CreatedAt AS LikesCreatedAt

FROM            dbo.[Page] INNER JOIN
                         dbo.Image ON dbo.Page.AvatarId = dbo.Image.Id

                OUTER APPLY
                    (SELECT TOP(1) * FROM [dbo].[FollowersInfo] [mi] WHERE [mi].[PageId] = [Page].[Id] AND [mi].[SocialNetwork] = [Page].SocialNetwork ORDER BY [mi].[CreatedAt] DESC) [mi]
                OUTER APPLY
                    (SELECT TOP(1) * FROM [dbo].[LikesInfo] [li] WHERE [li].[PageId] = [Page].[Id] AND [li].[SocialNetwork] = [Page].SocialNetwork ORDER BY [li].[CreatedAt] DESC) [li]

					WHERE [Page].[IsDeleted] = 0
