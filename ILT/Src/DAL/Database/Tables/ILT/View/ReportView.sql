﻿CREATE VIEW [dbo].[ReportView]
	AS
	SELECT        dbo.Report.Id, dbo.Report.[Key],dbo.Report.[Permalink], dbo.Report.[Order], dbo.Report.PeriodType, REPLACE(dbo.Image.UrlTemplate, '{0}',
                             (SELECT        TemplateValue
                               FROM            dbo.ImageVersion
                               WHERE        (ImageId = dbo.Image.Id) AND (Size = 50))) AS Photo50,

							   REPLACE(dbo.Image.UrlTemplate, '{0}',
                             (SELECT        TemplateValue
                               FROM            dbo.ImageVersion
                               WHERE        (ImageId = dbo.Image.Id) AND (Size = 200))) AS Photo200,
							   
							    dbo.Report.SocialNetwork, dbo.Report.ReportCategory, dbo.Report.PeriodStart, dbo.Report.PeriodEnd, dbo.Report.CreatedAt,
                             (SELECT        MAX(Total) AS Expr1
                               FROM            (SELECT        COUNT(*) AS Total
                                                         FROM            dbo.ReportPost
                                                         WHERE        (ReportId = dbo.Report.Id)
                                                         UNION
                                                         SELECT        COUNT(*) AS Total
                                                         FROM            dbo.ReportPage
                                                         WHERE        (ReportId = dbo.Report.Id)) AS t) AS Total
FROM            dbo.Report INNER JOIN
                         dbo.Image ON dbo.Report.AvatarId = dbo.Image.Id

						 WHERE Report.[IsDeleted] = 0
