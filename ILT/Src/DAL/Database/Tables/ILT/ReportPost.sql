﻿CREATE TABLE [dbo].[ReportPost]
(
    [ReportId]      UNIQUEIDENTIFIER NOT NULL,
    [PostId]        BIGINT NOT NULL,
    [PageId]        BIGINT NOT NULL,
    [SocialNetwork] INT NOT NULL,

    [Top]           INT NOT NULL DEFAULT(0),

    [IsPosted]      BIT NOT NULL DEFAULT 0,

    CONSTRAINT [PK_ReportPost_Id] PRIMARY KEY ([ReportId], [PostId], [PageId], [SocialNetwork]),
    CONSTRAINT [FK_ReportPost_ReportId_Report_Id] FOREIGN KEY ([ReportId]) REFERENCES [dbo].[Report] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_ReportPost_PostId_PageId_SocialNetwork_Post_Id] FOREIGN KEY ([PostId], [PageId], [SocialNetwork]) REFERENCES [dbo].[Post] ([Id], [PageId], [SocialNetwork]) ON DELETE CASCADE
)