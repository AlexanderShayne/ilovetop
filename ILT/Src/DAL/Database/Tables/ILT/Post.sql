﻿CREATE TABLE [dbo].[Post]
(
    [Id]                BIGINT NOT NULL,
    [PageId]            BIGINT NOT NULL,
    [SocialNetwork]     INT NOT NULL,
    [AvatarId]          UNIQUEIDENTIFIER NULL,

    [Permalink]         NVARCHAR (4000) NOT NULL DEFAULT '',
    [Text]              NVARCHAR (MAX) NOT NULL DEFAULT '',

    [CommentsCount]     INT NOT NULL,
    [LikesCount]        INT NOT NULL,
    [RepostsCount]      INT NOT NULL,

    [CreatedAt]         DATETIME NOT NULL DEFAULT GETDATE(),
    CONSTRAINT [PK_Post_Id] PRIMARY KEY CLUSTERED ([Id] ASC, [PageId] ASC, [SocialNetwork] ASC),
    CONSTRAINT [FK_Post_PageId_SocialNetwork_Page_Id] FOREIGN KEY ([PageId], [SocialNetwork]) REFERENCES [dbo].[Page] ([Id], [SocialNetwork]),
	CONSTRAINT [FK_Post_AvatarId_Image_Id] FOREIGN KEY ([AvatarId]) REFERENCES [dbo].[Image] ([Id])
)
