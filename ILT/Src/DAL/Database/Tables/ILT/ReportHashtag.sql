﻿CREATE TABLE [dbo].[ReportHashtag]
(
    [ReportPattern]     NVARCHAR (250) NOT NULL,
    [HashtagId]         UNIQUEIDENTIFIER NOT NULL,
    [SocialNetwork]     INT NOT NULL,
    CONSTRAINT [PK_ReportHashtag_Id] PRIMARY KEY ([ReportPattern], [HashtagId], [SocialNetwork]),
    CONSTRAINT [FK_ReportHashtag_HashtagId_Hashtag_Id] FOREIGN KEY ([HashtagId]) REFERENCES [dbo].[Hashtag] ([Id]) ON DELETE CASCADE
)
