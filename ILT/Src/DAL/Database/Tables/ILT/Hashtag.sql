﻿CREATE TABLE [dbo].[Hashtag]
(
    [Id]                UNIQUEIDENTIFIER NOT NULL,
    [Name]              NVARCHAR (4000) NOT NULL DEFAULT '',
    [CreatedAt]         DATE NOT NULL DEFAULT GETDATE(),
    [ModifiedAt]        DATE NOT NULL DEFAULT GETDATE(),
    [IsDeleted]         BIT NOT NULL DEFAULT 0,
    [Ts]                ROWVERSION NOT NULL,
    CONSTRAINT [PK_Hashtag_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UC_Hashtag_Name] UNIQUE([Name])
)
