﻿CREATE TABLE [dbo].[LikesInfo]
(
    [Id]                UNIQUEIDENTIFIER NOT NULL,

    [PageId]            BIGINT NOT NULL,
    [SocialNetwork]     INT NOT NULL,

    [Count]             INT NOT NULL,

    [DayChanges]        INT NULL,
    [WeekChanges]       INT NULL,
    [MonthChanges]      INT NULL,

    [CreatedAt]         DATE NOT NULL DEFAULT GETDATE(),
    CONSTRAINT [PK_LikesInfo_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_LikesInfo_PageId_SocialNetwork_Page_Id] FOREIGN KEY ([PageId], [SocialNetwork]) REFERENCES [dbo].[Page] ([Id], [SocialNetwork]) ON DELETE CASCADE,
    CONSTRAINT [UC_LikesInfo_PageId_SocialNetwork_CreatedAt] UNIQUE([PageId], [SocialNetwork], [CreatedAt])
)
