﻿CREATE TABLE [dbo].[Page]
(
    [Id]                BIGINT NOT NULL,
    [SocialNetwork]     INT NOT NULL,

    [AvatarId]          UNIQUEIDENTIFIER NOT NULL,
    [Name]              NVARCHAR (250) NOT NULL DEFAULT '',
    [ScreenName]        NVARCHAR (250) NOT NULL DEFAULT '',
    [Permalink]         NVARCHAR (4000) NOT NULL DEFAULT '',
    [ExcludeFromReports] BIT NOT NULL DEFAULT 0,

    [CreatedAt]         DATE NOT NULL DEFAULT GETDATE(),
    [ModifiedAt]        DATE NOT NULL DEFAULT GETDATE(),
    [IsDeleted]         BIT NOT NULL DEFAULT 0,
    [Ts]                ROWVERSION NOT NULL,
    CONSTRAINT [PK_Page_Id] PRIMARY KEY CLUSTERED ([Id] ASC, [SocialNetwork] ASC),
    CONSTRAINT [FK_Page_AvatarId_Image_Id] FOREIGN KEY ([AvatarId]) REFERENCES [dbo].[Image] ([Id])

)
