﻿CREATE TABLE [dbo].[PageHashtag]
(
    [PageId]         BIGINT NOT NULL,
    [SocialNetwork]  INT NOT NULL,

    [HashtagId]     UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_PageHashtag_Id] PRIMARY KEY ([PageId], [SocialNetwork], [HashtagId]),
    CONSTRAINT [FK_PageHashtag_PageId_SocialNetwork_Page_Id] FOREIGN KEY ([PageId], [SocialNetwork]) REFERENCES [dbo].[Page] ([Id], [SocialNetwork]) ON DELETE CASCADE,
    CONSTRAINT [FK_PageHashtag_HashtagId_Hashtag_Id] FOREIGN KEY ([HashtagId]) REFERENCES [dbo].[Hashtag] ([Id]) ON DELETE CASCADE
)