﻿CREATE TABLE [dbo].[FollowersInfo]
(
    [Id]                UNIQUEIDENTIFIER NOT NULL,

    [PageId]            BIGINT NOT NULL,
    [SocialNetwork]     INT NOT NULL,

    [Count]             INT NOT NULL,

    [DayChanges]        INT NULL,
    [WeekChanges]       INT NULL,
    [MonthChanges]      INT NULL,

    [CreatedAt]         DATE NOT NULL DEFAULT GETDATE(),
    CONSTRAINT [PK_FollowersInfo_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_FollowersInfo_PageId_SocialNetwork_Page_Id] FOREIGN KEY ([PageId], [SocialNetwork]) REFERENCES [dbo].[Page] ([Id], [SocialNetwork]) ON DELETE CASCADE,
    CONSTRAINT [UC_FollowersInfo_PageId_SocialNetwork_CreatedAt] UNIQUE([PageId], [SocialNetwork], [CreatedAt])
)
