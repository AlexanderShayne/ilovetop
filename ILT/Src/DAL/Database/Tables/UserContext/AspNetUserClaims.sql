﻿CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [UserId]     UNIQUEIDENTIFIER            NOT NULL,
    [ClaimType]  NVARCHAR (MAX) NULL,
    [ClaimValue] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_AspNetUserClaims_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AspNetUserClaims_UserId_AspNetUser_Id] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);

