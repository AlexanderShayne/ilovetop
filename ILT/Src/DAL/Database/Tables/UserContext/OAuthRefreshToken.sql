﻿CREATE TABLE [dbo].[OAuthRefreshToken] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [Subject]         NVARCHAR (50)  NOT NULL,
    [ClientId]        NVARCHAR (50)  NOT NULL,
    [IssuedUtc]       DATETIME2 (7)  NOT NULL,
    [ExpiresUtc]      DATETIME2 (7)  NOT NULL,
    [ProtectedTicket] NVARCHAR (600) NOT NULL,
    CONSTRAINT [PK_OAuthRefreshToken_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

