﻿CREATE TABLE [dbo].[AspNetRoles] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [Name] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_AspNetRoles_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT UQ_AspNetRoles_Name UNIQUE(Name)
);

