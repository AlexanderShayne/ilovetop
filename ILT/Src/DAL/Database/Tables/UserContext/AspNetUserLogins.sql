﻿CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] NVARCHAR (128)      NOT NULL,
    [ProviderKey]   NVARCHAR (128)      NOT NULL,
    [UserId]        UNIQUEIDENTIFIER    NOT NULL,
    [AccessToken]   NVARCHAR (4000)     NOT NULL DEFAULT '',
    CONSTRAINT [PK_AspNetUserLogins_Id] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC, [UserId] ASC),
    CONSTRAINT [FK_AspNetUserLogins_UserId_AspNetUser_Id] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);

