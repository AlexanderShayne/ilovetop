﻿CREATE TABLE [dbo].[OAuthClient] (
    [Id]           INT IDENTITY(1, 1) NOT NULL,
    [Secret]               NVARCHAR (300) NOT NULL,
    [Name]                 NVARCHAR (100) NOT NULL,
    [ApplicationType]      INT            NOT NULL,
    [Active]               BIT            NOT NULL,
    [RefreshTokenLifeTime] INT            NOT NULL,
    [AllowedOrigin]        NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_OAuthClient_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

