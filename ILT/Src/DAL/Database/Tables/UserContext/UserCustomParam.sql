﻿CREATE TABLE [dbo].[UserCustomParam]
(
    [UserId] UNIQUEIDENTIFIER NOT NULL,
    [Key] VARCHAR(50) NOT NULL,
    [Value] NVARCHAR(2048) NOT NULL,
    PRIMARY KEY ([UserId], [Key]),
    CONSTRAINT [FK_UserCustomParam_UserId_AspNetUsers_Id] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
)