﻿MERGE [dbo].[OAuthClient] ed
USING (
VALUES
    (N'Secret', N'hello', 0, 1, 30, N'*')
) nd ([Secret], [Name], [ApplicationType], [Active], [RefreshTokenLifeTime], [AllowedOrigin]) ON ed.[Name] = nd.[Name] AND ed.[Secret] = nd.[Secret]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Secret], [Name], [ApplicationType], [Active], [RefreshTokenLifeTime], [AllowedOrigin])
    VALUES (nd.[Secret], nd.[Name], nd.[ApplicationType], nd.[Active], nd.[RefreshTokenLifeTime], nd.[AllowedOrigin]);