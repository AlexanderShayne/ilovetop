﻿DECLARE @Email NVARCHAR(100) = N'admin@rilovetop.com'
DECLARE @InsertedAspNetUserId INT
DECLARE @RoleId uniqueidentifier = (SELECT Id from AspNetRoles WHERE Name = N'Admin')
DECLARE @Id uniqueidentifier = NEWID()

MERGE [dbo].[AspNetUsers] [AspNetUsers]
USING (
VALUES
    (@Id, @Email, 1, N'AKeyG/EtZwP1xRg0eo5lsIKreYYiBn3CA2ehKEHkNjHvCdI0DS8J+86ibyTK8QGptQ==', N'8d843e77-1c5a-4039-ba41-5ec0d924d743', 0, 0, 0, 0, @Email)
) [NewAspNetUsers] (Id, Email, EmailConfirmed, PasswordHash, SecurityStamp, PhoneNumberConfirmed, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, UserName) ON [AspNetUsers].[UserName] = [NewAspNetUsers].[UserName]
WHEN NOT MATCHED BY TARGET THEN
    INSERT (Id, Email, EmailConfirmed, PasswordHash, SecurityStamp, PhoneNumberConfirmed, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, UserName)
    VALUES ([NewAspNetUsers].Id,
            [NewAspNetUsers].Email,
            [NewAspNetUsers].EmailConfirmed,
            [NewAspNetUsers].PasswordHash,
            [NewAspNetUsers].SecurityStamp, 
            [NewAspNetUsers].PhoneNumberConfirmed, 
            [NewAspNetUsers].TwoFactorEnabled, 
            [NewAspNetUsers].LockoutEnabled, 
            [NewAspNetUsers].AccessFailedCount, 
            [NewAspNetUsers].UserName);

--AspNetUserRoles
INSERT INTO [dbo].[AspNetUserRoles] (UserId, RoleId)
VALUES (@Id, @RoleId)