﻿MERGE [dbo].[AspNetRoles] ed
USING (
VALUES
    (N'ManageReports', NEWID())
) nd ([Name], [Id]) ON ed.[Name] = nd.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id], [Name])
    VALUES (nd.[Id], nd.[Name]);