using System.Data.Entity.ModelConfiguration;
using Domain.ValueObjects.DateTimeRanges;

namespace Dal.Configuration.Mappings
{
    public class PeriodMap : ComplexTypeConfiguration<DateTimeRange>
    {
        public PeriodMap()
        {
            Property(x => x.Start);
            Property(x => x.End);
        }
    }
}