﻿using System.Data.Entity;
using System.Linq;
using Dal.DomainStack.Ef.Mapping;
using Domain.Entities.Images;
using ILT.Domain.Posts;
using ILT.Domain.PublicPages;
using ILT.Domain.Reports;
using Z.Dapper.Plus;

namespace Dal.Configuration.Mappings
{
    public class MappingModule : IMappingModule
    {
        static MappingModule()
        {
            DapperPlusManager
                .Entity<Page>()
                .Table("Page")
                .Key(x => new { x.Id, x.SocialNetwork })
                .Map(x => x.Name)
                .Map(x => x.ScreenName)
                .Map(x => x.Permalink)
                .Map(x => x.Avatar.Id, "AvatarId")
                .Ignore(x => x.ExcludeFromReports)
                .Ignore(x => x.Avatar)
                .Ignore(x => x.LikesInfo)
                .Ignore(x => x.FollowersInfo)
                .Ignore(x => x.CrudState)
                .Ignore(x => x.IsValid)
                .Ignore(x => x.ModifiedAt)
                .Ignore(x => x.Ts)
                .Ignore(x => x.IsDeleted)
                .Ignore(x => x.IsNotValid)
                .Ignore(x => x.ValidationErrors)
                .Ignore(x => x.CreatedAt);

            DapperPlusManager
                .Entity<Image>()
                .Table("Image")
                .Key(x => x.Id)
                .Map(x => x.UrlTemplate)
                .Ignore(x => x.CrudState);

            DapperPlusManager
                .Entity<ImageVersion>()
                .Table("ImageVersion")
                .Key(x => x.Id)
                .Map(x => x.Size)
                .Map(x => x.TemplateValue)
                .Map(x => x.ImageId)
                .Ignore(x => x.CrudState);

            DapperPlusManager
                .Entity<ReportPage>()
                .Table("ReportPage")
                .Key(x => new { x.ReportId, x.PageId, x.SocialNetwork })
                .Map(x => x.Top)
                .Map(x => x.TopChanges)
                .Ignore(x => x.Page);

            DapperPlusManager
                .Entity<ReportPost>()
                .Table("ReportPost")
                .Key(x => new { x.ReportId, x.PostId, x.PageId, x.SocialNetwork })
                .Map(x => x.Top)
                .Map(x => x.IsPosted)
                .Ignore(x => x.Post)
                .Ignore(x => x.IsPosted);

            DapperPlusManager
                .Entity<Report>()
                .Table("Report")
                .Key(x => x.Id)
                .Map(x => x.Period.Start, "PeriodStart")
                .Map(x => x.Period.End, "PeriodEnd")
                .Map(x => x.Key)
                .Map(x => x.Order)
                .Map(x => x.PeriodType)
                .Map(x => x.ReportCategory)
                .Map(x => x.CreatedAt)
                .Map(x => x.ModifiedAt)
                .Map(x => x.IsDeleted)
                .Map(x => x.Avatar.Id, "AvatarId")
                .Map(x => x.SocialNetwork)
                .Map(x => x.Permalink)
                .Ignore(x => x.Avatar)
                .Ignore(x => x.CrudState)
                .Ignore(x => x.IsValid)
                .Identity(x => x.Ts)
                .Ignore(x => x.IsNotValid)
                .Ignore(x => x.ValidationErrors);

            DapperPlusManager
                .Entity<Post>()
                .Table("Post")
                .Key(x => new { x.Id, x.PageId, x.SocialNetwork })
                .Map(x => x.Text)
                .Map(x => x.CommentsCount)
                .Map(x => x.LikesCount)
                .Map(x => x.RepostsCount)
                .Map(x => x.CreatedAt)
                .Map(x => x.Permalink)
                .Map(x => x.Avatar.Id, "AvatarId")
                .Ignore(x => x.Avatar)
                .Ignore(x => x.Page)
                .Ignore(x => x.ModifiedAt)
                .Ignore(x => x.ViewsCount)
                .Ignore(x => x.CrudState);
        }

        public void Map(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PageMap());
            modelBuilder.Configurations.Add(new PostMap());
            modelBuilder.Configurations.Add(new HashtagMap());
            modelBuilder.Configurations.Add(new ReportMap());
            modelBuilder.Configurations.Add(new ReportPostMap());
            modelBuilder.Configurations.Add(new ReportPageMap());
            modelBuilder.Configurations.Add(new PeriodMap());

            var derivedType = typeof(Report);

            var allReportTypes = derivedType.Assembly
                .GetTypes()
                .Where(t =>
                    t != derivedType &&
                    derivedType.IsAssignableFrom(t)
                    );

            modelBuilder.Ignore(allReportTypes);
        }
    }
}
