using System.Data.Entity.ModelConfiguration;
using ILT.Domain.Reports;

namespace Dal.Configuration.Mappings
{
    public class ReportPostMap : EntityTypeConfiguration<ReportPost>
    {
        public ReportPostMap()
        {
            HasKey(x => new {x.ReportId, x.PageId, x.PostId});
            Ignore(x => x.CrudState);
        }
    }
}