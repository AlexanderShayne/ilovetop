using Dal.DomainStack.Ef.Mapping;
using ILT.Domain.Posts;

namespace Dal.Configuration.Mappings
{
    public class PostMap : AggregateRootEntityBaseMap<Post, long>
    {
        public PostMap()
        {
        }
    }
}