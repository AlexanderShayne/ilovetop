﻿using Dal.DomainStack.Ef.Mapping;
using ILT.Domain.PublicPages;

namespace Dal.Configuration.Mappings
{
    public class PageMap : AggregateRootEntityBaseMap<Page, long>
    {
        public PageMap()
        {
            HasMany(x => x.Hashtags)
                .WithMany()
                .Map(cs =>
                {
                    cs.MapLeftKey("PageId");
                    cs.MapRightKey("HashtagId");
                    cs.ToTable("PageHashtag");
                });

            Ignore(x => x.LikesInfo);
            Ignore(x => x.FollowersInfo);
            Ignore(x => x.Avatar);
        }
    }
}