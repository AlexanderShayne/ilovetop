﻿using System;
using Dal.DomainStack.Ef.Mapping;
using ILT.Domain.Hashtags;

namespace Dal.Configuration.Mappings
{
    public class HashtagMap : AggregateRootEntityBaseMap<Hashtag, Guid>
    {
    }
}