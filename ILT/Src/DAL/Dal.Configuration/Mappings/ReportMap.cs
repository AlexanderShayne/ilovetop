﻿using System;
using Dal.DomainStack.Ef.Mapping;
using ILT.Domain.Reports;

namespace Dal.Configuration.Mappings
{
    public class ReportMap : AggregateRootEntityBaseMap<Report, Guid>
    {
        public ReportMap()
        {
            HasMany(x => x.ReportPosts)
                .WithRequired()
                .HasForeignKey(x => x.ReportId);

            HasMany(x => x.ReportPages)
                .WithRequired()
                .HasForeignKey(x => x.ReportId);

            Property(x => x.Period.Start)
                .HasColumnName("PeriodStart");

            Property(x => x.Period.End)
                .HasColumnName("PeriodEnd");

            Ignore(x => x.IsUnique);
            Ignore(x => x.Avatar);
        }
    }
}
