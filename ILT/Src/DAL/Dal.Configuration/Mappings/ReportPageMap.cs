using System.Data.Entity.ModelConfiguration;
using ILT.Domain.Reports;

namespace Dal.Configuration.Mappings
{
    public class ReportPageMap : EntityTypeConfiguration<ReportPage>
    {
        public ReportPageMap()
        {
            HasKey(x => new { x.ReportId, x.PageId });
            Ignore(x => x.CrudState);
        }
    }
}