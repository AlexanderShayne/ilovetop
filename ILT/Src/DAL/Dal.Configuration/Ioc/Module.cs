using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Contracts.Dal.DomainStack;
using Dal.DomainStack.Ef;
using Dal.DomainStack.Ef.Context;
using Dal.QueryStack.Dapper;
using ILT.Data.PublicPages.QueryRepositories;

namespace Dal.Configuration.Ioc
{
    public class Module : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IDataContext, IUnitOfWork>()
                    .ImplementedBy<DataContext>()
                    .LifeStyle
                    .PerWebRequest,

                Classes
                    .FromAssemblyContaining<PagesQueryRepository>()
                    .BasedOn<QueryRepositoryBase>()
                    .WithServiceDefaultInterfaces()
                    .LifestylePerWebRequest(),

                Classes
                    .FromAssemblyContaining<PagesQueryRepository>()
                    .BasedOn(typeof(RepositoryBase<,>))
                    .WithServiceDefaultInterfaces()
                    .LifestylePerWebRequest()
                );
        }
    }
}