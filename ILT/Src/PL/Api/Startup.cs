﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Api;
using Crosscutting.IoC.CastleWindsor;
using Crosscutting.IoC.WebApi.CastleWindsor;
using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Owin;
using UserContext.Application;

[assembly: OwinStartup(typeof(Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            var config = GlobalConfiguration.Configuration;
            
            var serializerSettings = config.Formatters.JsonFormatter.SerializerSettings;
            serializerSettings.Formatting = Formatting.Indented;
            serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            serializerSettings.NullValueHandling = NullValueHandling.Ignore;

            app.Use(async (context, next) =>
            {
                if (context.Request.QueryString.HasValue)
                {
                    if (string.IsNullOrWhiteSpace(context.Request.Headers.Get("Authorization")))
                    {
                        var queryString = HttpUtility.ParseQueryString(context.Request.QueryString.Value);
                        string token = queryString.Get("access_token");

                        if (!string.IsNullOrWhiteSpace(token))
                        {
                            context.Request.Headers.Add("Authorization", new[] {$"Bearer {token}"});
                        }
                    }
                }

                await next.Invoke();
            });

            new WindsorBootstrapper()
                .Bootstrap(
                    new Domain.Configuration.Ioc.Module(),
                    new UserContext.Application.Module(),
                    new Module(),
                    new Dal.Configuration.Ioc.Module(),
                    new Crosscutting.Configuration.Module(),
                    new ILT.Application.Module(),
                    new ILT.Application.Reports.Module(),
                    new ILT.Application.PublicPages.Module(),
                    new ILT.Application.Hashtags.Module()
                    )
                .SetupWebApi(config);

            OAuthConfig.Register(app);

            GlobalConfiguration.Configuration.EnsureInitialized();
            log4net.Config.XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();
        }
    }
}
