﻿using System;
using System.Web.Http;
using ILT.Contracts.Application.Hashtags.Models.Input;
using ILT.Contracts.Application.Hashtags.Services;

namespace Api.Controllers.Hashtags
{
    [RoutePrefix("hashtags")]
    public class HashtagsController : ApiController
    {
        readonly IHashtagsService hashtagsService;

        public HashtagsController(IHashtagsService hashtagsService)
        {
            this.hashtagsService = hashtagsService;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Query(int page, int pageSize)
        {
            return Ok();
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Create(CreateHashtagDetails im)
        {
            var vm = hashtagsService.Create(im);
            return Ok(vm);
        }

        [HttpDelete]
        [Route("{key}")]
        public IHttpActionResult Delete(Guid key)
        {
            hashtagsService.Delete(key);
            return Ok();
        }

        [HttpPut]
        [Route("{key}/reportPatterns/{reportPattern}")]
        public IHttpActionResult AddReportPattern(Guid key, string reportPattern)
        {
            var vm = hashtagsService.AddReportPattern(key, reportPattern);
            return Ok(vm);
        }
    }
}