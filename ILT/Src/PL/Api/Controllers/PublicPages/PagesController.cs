﻿using System;
using System.Web.Http;
using Contracts.Services.Application.QueryStack.Pagging.Models;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Application.PublicPages.Models.View;
using ILT.Contracts.Application.PublicPages.Services;

namespace Api.Controllers.PublicPages
{
    [RoutePrefix("{socialNetwork}/pages")]
    public class PagesController : ApiController
    {
        readonly IPagesService pagesService;

        public PagesController(IPagesService pagesService)
        {
            this.pagesService = pagesService;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Query(SocialNetwork socialNetwork, int page = 1, int pageSize = 10)
        {
            PaggedResult<PageSummary> result = pagesService.Query(socialNetwork, page, pageSize);
            return Ok(result);
        }

        [HttpPut]
        [Route("{pageId}/hashtags/{hashtagId}")]
        public IHttpActionResult AddHashtag(SocialNetwork socialNetwork, long pageId, Guid hashtagId)
        {
            var vm = pagesService.AddHashtag(socialNetwork, pageId, hashtagId);
            return Ok(vm);
        }

        [Authorize(Roles = "ManageReports")]
        [HttpPatch]
        [Route("{pageId}")]
        public IHttpActionResult ExcludeFromReports(SocialNetwork socialNetwork, long pageId)
        {
            var vm = pagesService.ExcludeFromReports(socialNetwork, pageId);
            return Ok(vm);
        }
    }
}