﻿using System.IO;
using System.Net;
using System.Text;
using System.Web.Http;
using Contracts.Services.Infrastructure.SocialNetworks;

namespace Api.Controllers.Posts
{
    [RoutePrefix("{socialNetwork}/posts")]
    public class PostsController : ApiController
    {
        [HttpGet]
        [Route("{pageId}_{postId}/hash")]
        public IHttpActionResult GetHash(SocialNetwork socialNetwork, long pageId, long postId)
        {
            WebRequest request = WebRequest.Create("https://vk.com/dev.php");
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            ASCIIEncoding encoding = new ASCIIEncoding();
            string stringData = $"act=a_get_post_hash&al=1&post=-{pageId}_{postId}"; //place body here
            byte[] data = encoding.GetBytes(stringData);

            request.ContentLength = data.Length;
            request.Headers.Add("Cookie", "remixstid=1811105472_8c88bc284588ebee94; remixdt=0; remixtst=fbf8f15d; remixrt=0; audio_vol=16; remixlang=3; remixseenads=2; remixsid=1ef09a183355aff99c78d71c813ac85faf277dd24f533c5ed5fe0; remixsslsid=1; remixrefkey=56461ab439e36eace4; remixflash=22.0.0; remixscreen_depth=24");

            Stream newStream = request.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            string responseFromServer = reader.ReadToEnd();

            var tokenIndex = responseFromServer.LastIndexOf('>');

            string token = responseFromServer.Substring(tokenIndex + 1);

            return Ok(new HashSummary
            {
                Hash = token
            });
        }
    }

    public class HashSummary
    {
        public string Hash { get; set; }
    }
}
