﻿using System.Web.Http;
using Contracts.Services.Application.QueryStack.Pagging.Models;
using Contracts.Services.Infrastructure.SocialNetworks;
using ILT.Contracts.Application.Reports.Models.View;
using ILT.Contracts.Application.Reports.Services;
using ILT.Contracts.Domain.Reports;

namespace Api.Controllers.Reports
{
    [RoutePrefix("{socialNetwork}/reports")]
    public class ReportsController : ApiController
    {
        readonly IReportsService reportsService;

        public ReportsController(IReportsService reportsService)
        {
            this.reportsService = reportsService;
        }

        [HttpGet]
        [Route("")]
        public IHttpActionResult Query(SocialNetwork socialNetwork, int page = 1, int pageSize = 10, ReportCategory? reportCategoryFilter = null)
        {
            PaggedResult<ReportSummary> result = reportsService.Query(socialNetwork, page, pageSize, reportCategoryFilter);
            return Ok(result);
        }

        [HttpGet]
        [Route("{key}")]
        public IHttpActionResult Read(SocialNetwork socialNetwork, string key, int page, int pageSize, int periodsAgo = 0)
        {
            ReportSummary result = reportsService.Read(socialNetwork, key, page, pageSize, periodsAgo);

            if (result == null)
            {
                return NotFound();
            }
            return Ok(result);
        }

        [Authorize(Roles = "ManageReports")]
        [HttpDelete]
        [Route("posts/{pageId}_{postId}")]
        public IHttpActionResult Delete(SocialNetwork socialNetwork, long pageId, long postId)
        {
            reportsService.Delete(socialNetwork, pageId, postId);
            return Ok();
        }
    }
}
