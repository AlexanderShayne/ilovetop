﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using UserContext.Contracts.Application.Models.Input;
using UserContext.Contracts.Application.Services;

namespace Api.Controllers.UserContext
{
    [RoutePrefix("users")]
    public class UserContextController : ApiController
    {
        //public IUserPasswordService UserPasswordService { get; set; }
        public IUserContextService UserContextService { get; set; }

        [Route("")]
        [HttpPost]
        public IHttpActionResult Create(UserDetails m)
        {
            var result = UserContextService.Create(m);
            return Ok(result);
        }

        [Route("")]
        [HttpHead]
        public IHttpActionResult Exists(string email, string userName)
        {
            bool result = false;

            if (!String.IsNullOrEmpty(email))
            {
                result = UserContextService.IsEmailUnique(email);
            }

            if (!String.IsNullOrEmpty(userName))
            {
                result = UserContextService.IsUserNameUnique(userName);
            }

            return result ? (IHttpActionResult) Ok() : NotFound();
        }

        [OverrideAuthentication]
        //[HostAuthentication(DefaultAuthenticationTypes.ExternalCookie)]
        [Route("ExternalLogin", Name = "ExternalLogin")]
        public IHttpActionResult GetExternalLogin(string provider, string redirectUrl)
        {
            return new ChallengeResult(provider, redirectUrl);
        }

        [Route("{id:Guid}")]
        [HttpPatch]
        [Authorize(Roles = "Admin")]
        public IHttpActionResult ChangePassword(UserDetails vm)
        {
            UserContextService.ChangePassword(vm);
            return Ok();
        }

        [Route("ForgottenPassword")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult ForgottenPassword(ForgottenPasswordDetails vm)
        {
            UserContextService.SendResetPasswordEmail(vm);
            return Ok();
        }

        [Route("{id}/ForgottenPassword")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult ResetPassword(Guid id, ResetPasswordDetails vm)
        {
            UserContextService.ResetPassword(id, vm);
            return Ok();
        }
    }

    class ChallengeResult : IHttpActionResult
    {
        readonly string _provider;
        readonly string _redirectUri;

        public ChallengeResult(string provider, string redirectUri)
        {
            _provider = provider;
            _redirectUri = redirectUri;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var properties = new AuthenticationProperties
            {
                RedirectUri = _redirectUri
            };

            HttpContext.Current.GetOwinContext().Authentication.Challenge(properties, _provider);

            return Task.FromResult(new HttpResponseMessage(HttpStatusCode.Unauthorized));
        }
    }
}
