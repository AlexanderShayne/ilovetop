﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Web.Mvc;
using Api.Areas.Seo.Configuration;
using Api.Areas.Seo.Infrastructure;

namespace Api.Areas.Seo.Controllers
{
    public class SitemapController : Controller
    {
        private readonly SeoSection seoSection;
        public SitemapController()
        {
            seoSection = ConfigurationManager.GetSection("seo") as SeoSection;
        }

        public ActionResult Index()
        {
            var baseUri = Request.Url.Scheme + Uri.SchemeDelimiter + Request.Url.Host;

            var sitemapItems = new List<SitemapItem>();
            foreach (SitemapElement sitemapElement in seoSection.SitemapCollection)
            {
                sitemapItems.Add(new SitemapItem
                {
                    Name = sitemapElement.Name,
                    Url = $"{baseUri}/{sitemapElement.Url}",
                    ChangePeriod = sitemapElement.ChangePeriod.HasValue ? (ChangePeriod?)sitemapElement.ChangePeriod.Value : null,
                    Priority = sitemapElement.Priority.HasValue ? (double?)sitemapElement.Priority.Value : null,
                    LastModified = string.IsNullOrEmpty(sitemapElement.LastModified) ? null : (DateTime?)DateTime.ParseExact(sitemapElement.LastModified, "yyyy-MM-dd", CultureInfo.InvariantCulture)
                });
            }

            return new SitemapResult(new SitemapBuilder(), sitemapItems);
        }
    }
}