﻿using System.Configuration;

namespace Api.Areas.Seo.Configuration
{
    public class SitemapElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("url", IsRequired = true)]
        public string Url
        {
            get { return (string)this["url"]; }
            set { this["url"] = value; }
        }

        [ConfigurationProperty("changePeriod", IsRequired = false)]
        public int? ChangePeriod
        {
            get { return (int?) this["changePeriod"]; }
            set { this["changePeriod"] = value; }
        }

        [ConfigurationProperty("lastModified", IsRequired = false)]
        public string LastModified
        {
            get { return (string)this["lastModified"]; }
            set { this["lastModified"] = value; }
        }

        [ConfigurationProperty("priority", IsRequired = false)]
        public double? Priority
        {
            get { return (double?)this["priority"]; }
            set { this["priority"] = value; }
        }
    }
}