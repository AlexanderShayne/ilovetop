﻿using System.Configuration;

namespace Api.Areas.Seo.Configuration
{
    public class SeoSection : ConfigurationSection
    {
        [ConfigurationProperty("sitemap", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof (SitemapElementCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public SitemapElementCollection SitemapCollection => (SitemapElementCollection) base["sitemap"];
    }
}