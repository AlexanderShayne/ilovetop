﻿using System.Configuration;

namespace Api.Areas.Seo.Configuration
{
    [ConfigurationCollection(typeof(SitemapElement))]
    public class SitemapElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new SitemapElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SitemapElement)element).Name;
        }
    }
}