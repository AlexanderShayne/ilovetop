﻿using System;

namespace Api.Areas.Seo.Infrastructure
{
    public class SitemapItem
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public double? Priority { get; set; }
        public ChangePeriod? ChangePeriod { get; set; }
        public DateTime? LastModified { get; set; }
    }
}