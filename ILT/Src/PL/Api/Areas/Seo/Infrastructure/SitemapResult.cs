﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Xml;

namespace Api.Areas.Seo.Infrastructure
{
    public class SitemapResult : ActionResult
    {
        private readonly IEnumerable<SitemapItem> items;
        private readonly SitemapBuilder sitemapBuilder;

        public SitemapResult(SitemapBuilder sitemapBuilder, IEnumerable<SitemapItem> items)
        {
            if (items == null) throw new ArgumentNullException("items");
            if (sitemapBuilder == null) throw new ArgumentNullException("sitemapBuilder");

            this.items = items;
            this.sitemapBuilder = sitemapBuilder;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;

            response.ContentType = "text/xml";
            response.ContentEncoding = Encoding.UTF8;

            using (var writer = new XmlTextWriter(response.Output))
            {
                writer.Formatting = Formatting.Indented;
                var sitemap = sitemapBuilder.Build(items);

                sitemap.WriteTo(writer);
            }
        }
    }
}