﻿using System;

namespace Api.Areas.Seo.Infrastructure
{
    [Flags]
    public enum ChangePeriod
    {
        Always = 0,
        Hourly,
        Daily,
        Weekly,
        Monthly,
        Yearly,
        Never
    }
}