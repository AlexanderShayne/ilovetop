﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace Api.Areas.Seo.Infrastructure
{
    public class SitemapBuilder : ISitemapBuilder
    {
        private static readonly XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
        private static readonly XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";

        public virtual XDocument Build(IEnumerable<SitemapItem> items)
        {
            if (items == null) throw new ArgumentNullException("items");

            var sitemap = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement(xmlns + "urlset", new XAttribute("xmlns", xmlns), items.Select(CreateItemElement)));

            return sitemap;
        }

        private XElement CreateItemElement(SitemapItem item)
        {
            var itemElement = new XElement(xmlns + "url", new XElement(xmlns + "loc", item.Url.ToLowerInvariant()));

            if (item.ChangePeriod.HasValue)
                itemElement.Add(new XElement(xmlns + "changefreq", item.ChangePeriod.Value.ToString().ToLower()));

            if (item.LastModified.HasValue)
                itemElement.Add(new XElement(xmlns + "lastmod", item.LastModified.Value.ToString("yyyy-MM-dd")));

            if (item.Priority.HasValue)
                    itemElement.Add(new XElement(xmlns + "priority", item.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));

            return itemElement;
        }
    }
}