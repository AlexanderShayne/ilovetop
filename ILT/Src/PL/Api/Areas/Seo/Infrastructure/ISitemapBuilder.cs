﻿using System.Collections.Generic;
using System.Xml.Linq;

namespace Api.Areas.Seo.Infrastructure
{
    public interface ISitemapBuilder
    {
        XDocument Build(IEnumerable<SitemapItem> items);
    }
}