﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Domain.Entities.Model;
using Contracts.Services.Infrastructure.SocialNetworks;
using Crosscutting.Infrastructure;
using Domain.Entities.GuidEntities;
using Domain.Entities.Images;
using Domain.ValueObjects.DateTimeRanges;
using FluentValidation;
using ILT.Contracts.Domain.Reports;
using ILT.Domain.Hashtags;
using ILT.Domain.Posts;
using ILT.Domain.PublicPages;

namespace ILT.Domain.Reports
{
    public class Report : GuidAggregateRootEntityBase
    {
        public Report()
        {
            CreatedAt = DateTime.Today;
            ModifiedAt = DateTime.Today;
        }

        public string Key { get; set; }
        public SocialNetwork SocialNetwork { get; set; }
        public ReportCategory ReportCategory { get; set; }
        public int Order { get; set; }
        public DateTimeRange Period { get; set; }
        public bool IsUnique { get; set; }
        public bool IsPosted { get; set; }
        public Image Avatar { get; set; }

        DateTimeRangeType periodType;
        public DateTimeRangeType PeriodType
        {
            get
            {
                return periodType;
            }
            set
            {
                periodType = value;
                Period = new DateTimeRangeFactory().Create(value, DateTime.Today.AddDays(-1));
            }
        }

        string permalink;
        public string Permalink
        {
            get
            {
                if (String.IsNullOrEmpty(permalink))
                {
                    permalink = $"http://ilovetop.net/{SocialNetwork.GetDescription()}/reports/{Key}";
                }

                return permalink;
            }
            set { permalink = value; }
        }

        public IEnumerable<Page> Pages
        {
            get { return ReportPages.Select(x => x.Page); }
        }

        public IEnumerable<Post> Posts
        {
            get { return ReportPosts.Select(x => x.Post); }
        }

        public ICollection<ReportPost> ReportPosts { get; set; } = new List<ReportPost>();
        public ICollection<ReportPage> ReportPages { get; set; } = new List<ReportPage>();
        public ICollection<Hashtag> Hashtags { get; set; } = new List<Hashtag>();

        protected readonly object InstanceLocker = new object();

        #region Validation

        public class Validator : AbstractValidator<Report>
        {
            public Validator()
            {
                RuleFor(x => x.Id)
                    .NotEqual(Guid.Empty);

                RuleFor(x => x.Key)
                    .NotEmpty()
                    .Length(0, 250);

                RuleFor(x => x.Order)
                    .GreaterThanOrEqualTo(0);

                RuleFor(x => x.Permalink)
                    .NotEmpty()
                    .Length(0, 4000);

                RuleFor(x => x.Period)
                    .NotNull();

                RuleFor(x => x.IsDeleted)
                    .Equal(false)
                    .When(x => x.CrudState != CrudState.Deleted);

                RuleFor(x => x.CreatedAt)
                    .NotEqual(DateTime.MinValue);

                RuleFor(x => x.ModifiedAt)
                    .NotEqual(DateTime.MinValue);

                RuleFor(x => x.ReportPosts)
                    .Must(HasCorrectTopRange)
                    .WithMessage("Incorrect posts top range.");
                
                RuleFor(x => x.ReportPages)
                    .Must(HasCorrectTopRange)
                    .WithMessage("Incorrect pages top range.");

                RuleFor(x => x.ReportPages)
                    .Must(HasUniquePages)
                    .WithMessage("Dublicate pages.");

                RuleFor(x => x.ReportPosts)
                    .Must(HasUniquePosts)
                    .WithMessage("Dublicate posts.");
            }

            static bool HasCorrectTopRange(IEnumerable<IHasTop> reportItems)
            {
                var count = reportItems.Count();

                try
                {
                    for (int i = 0; i < count; ++i)
                    {
                        var reportPost = reportItems.Single(x => x.Top == i);
                    }
                }
                catch (Exception)
                {
                    return false;
                }

                return true;
            }

            static bool HasUniquePages(IEnumerable<ReportPage> reportPages)
            {
                if (!reportPages.Any()) return true;

                var hashset = new HashSet<long>();
                return reportPages.All(x => hashset.Add(x.PageId));
            }

            static bool HasUniquePosts(IEnumerable<ReportPost> reportPosts)
            {
                if (reportPosts.IsNullOrEmpty()) return true;
                var sn =
                    reportPosts
                        .First()
                        .SocialNetwork;

                if (sn == SocialNetwork.Vk)
                {
                    var hashset = new HashSet<string>();
                    return reportPosts.All(x => hashset.Add($"{x.PageId}_{x.PostId}"));
                }
                else
                {
                    var hashset = new HashSet<long>();
                    return reportPosts.All(x => hashset.Add(x.PostId));
                }
            }
        }

        #endregion
    }
}