using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.ValueObjects.DateTimeRanges;
using ILT.Contracts.Domain.Reports;
using ILT.Domain.PublicPages;

namespace ILT.Domain.Reports.PageReports
{
    public class AntiTopPagesByFollowersPerWeekReport : PageReportBase
    {
        private static class Consts
        {
            public const int Count = 100;
            public const string ReportKey = "anti-top-pages-by-followers-per-week";
            public const DateTimeRangeType PeriodType = DateTimeRangeType.Week;
            public const int Order = 2;
            public const string Photo50 = "http://ilovetop.net/app/assets/vk-communities/top-100-50.png";
            public const string Photo100 = "http://ilovetop.net/app/assets/vk-communities/top-100-100.png";
            public const string Photo200 = "http://ilovetop.net/app/assets/vk-communities/top-100-200.png";
            public const ReportCategory ReportCategory = Contracts.Domain.Reports.ReportCategory.AntiTopPages;
        }

        public AntiTopPagesByFollowersPerWeekReport(SocialNetwork socialNetwork) : base(socialNetwork, Consts.ReportKey, Consts.PeriodType, Consts.Order, Consts.Photo50, Consts.Photo100, Consts.Photo200, Consts.ReportCategory)
        {
        }

        public override void AddData(IEnumerable<Page> data)
        {
            lock (InstanceLocker)
            {
                var pages =
                    Pages
                        .Concat(data.Where(x => x.FollowersInfo.WeekChanges.HasValue))
                        .Distinct(new Page.EqualityComparer())
                        .OrderBy(x => x.FollowersInfo.WeekChanges)
                        .Take(Consts.Count);

                SetPages(pages);
            }
        }
    }
}