using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Crosscutting.IoC;
using Domain.Entities.Images;
using Domain.ValueObjects.DateTimeRanges;
using ILT.Contracts.Dal.Query.Reports.QueryRepositories;
using ILT.Contracts.Domain.Reports;
using ILT.Domain.PublicPages;

namespace ILT.Domain.Reports.PageReports
{
    public abstract class PageReportBase : ReportBase<IEnumerable<Page>>
    {
        #region Private Members

        ISystemReportsQueryRepository SystemReportsQueryRepository => ContainerHolder.Container.Resolve<ISystemReportsQueryRepository>();

        #endregion

        #region Public Methods

        public static ReportBase<IEnumerable<Page>>[] CreateAllPageReports(SocialNetwork socialNetwork)
        {
            return new ReportBase<IEnumerable<Page>>[]
            {
                new MoreThan1000000FollowersReport(socialNetwork),
                new TopPagesReport(socialNetwork),
                new TopPagesByFollowersPerDayReport(socialNetwork),
                new TopPagesByFollowersPerWeekReport(socialNetwork),
                new TopPagesByFollowersPerMonthReport(socialNetwork),
                new AntiTopPagesByFollowersPerDayReport(socialNetwork),
                new AntiTopPagesByFollowersPerWeekReport(socialNetwork),
                new AntiTopPagesByFollowersPerMonthReport(socialNetwork),
                new TopPagesByLikesPerDayReport(socialNetwork),
                new TopPagesByLikesPerWeekReport(socialNetwork),
                new TopPagesByLikesPerMonthReport(socialNetwork)
            };
        }

        #endregion

        #region Protected Methods

        protected PageReportBase(SocialNetwork socialNetwork, string key, DateTimeRangeType periodType, int order, string photo50, string photo100, string photo200, ReportCategory reportCategory)
        {
            SocialNetwork = socialNetwork;
            Key = key;
            PeriodType = periodType;
            Order = order;
            ReportCategory = reportCategory;
            IsUnique = true;

            Avatar = new Image
            {
                UrlTemplate = new Uri(photo50).GetLeftPart(UriPartial.Authority) + "{0}"
            };

            Avatar.AddVersion(50, new Uri(photo50).PathAndQuery);
            Avatar.AddVersion(100, new Uri(photo100).PathAndQuery);
            Avatar.AddVersion(200, new Uri(photo200).PathAndQuery);
        }

        protected void SetPages(IEnumerable<Page> pages)
        {
            ReportPages = pages.Select((x, i) => new ReportPage
                {
                    PageId = x.Id,
                    SocialNetwork = x.SocialNetwork,
                    Top = i + 1,
                    TopChanges = TopChanges(x.Id, i + 1),
                    Page = x,
                    ReportId = Id
                })
                .ToList();
        }

        #endregion

        #region Private Methods

        int TopChanges(long pageId, int currentPlace)
        {
            return 0;
            //int? prevTop =
            //    SystemReportsQueryRepository
            //        .GetPageTop(
            //            SocialNetwork,
            //            Key,
            //            pageId,
            //            new DateTimeRangeFactory()
            //                .Create(PeriodType, 1)
            //                .End);
            //
            //if (!prevTop.HasValue) return 0;
            //
            //return prevTop.Value - currentPlace;
        }

        #endregion
    }
}