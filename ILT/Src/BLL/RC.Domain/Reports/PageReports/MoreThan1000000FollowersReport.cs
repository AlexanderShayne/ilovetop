using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.ValueObjects.DateTimeRanges;
using ILT.Contracts.Domain.Reports;
using ILT.Domain.PublicPages;

namespace ILT.Domain.Reports.PageReports
{
    public class MoreThan1000000FollowersReport : PageReportBase
    {
        private static class Consts
        {
            public const int Count = 1000000;
            public const string ReportKey = "more-than-1000000-followers";
            public const DateTimeRangeType PeriodType = DateTimeRangeType.Day;
            public const int Order = 2;
            public const string Photo50 = "http://ilovetop.net/app/assets/vk-communities/more-than-1000000-members-50.png";
            public const string Photo100 = "http://ilovetop.net/app/assets/vk-communities/more-than-1000000-members-100.png";
            public const string Photo200 = "http://ilovetop.net/app/assets/vk-communities/more-than-1000000-members-200.png";
            public const ReportCategory ReportCategory = Contracts.Domain.Reports.ReportCategory.TopPages;
        }

        public MoreThan1000000FollowersReport(SocialNetwork socialNetwork) : base(socialNetwork, Consts.ReportKey, Consts.PeriodType, Consts.Order, Consts.Photo50, Consts.Photo100, Consts.Photo200, Consts.ReportCategory)
        {
        }

        public override void AddData(IEnumerable<Page> data)
        {
            lock (InstanceLocker)
            {
                var pages =
                    Pages
                        .Concat(data.Where(x => x.FollowersInfo.Count >= Consts.Count))
                        .Distinct(new Page.EqualityComparer())
                        .OrderByDescending(x => x.FollowersInfo.Count);

                SetPages(pages);
            }
        }
    }
}