﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.ValueObjects.DateTimeRanges;
using ILT.Domain.Posts;
using ILT.Domain.PublicPages;

namespace ILT.Domain.Reports.PostReports
{
    public class TopPostsInPageReport : ReportBase<IEnumerable<Post>>
    {
        private static class Consts
        {
            public const int Count = 300;
            public const string ReportKeyTmpl = "top-posts-in-{0}";
        }

        readonly long pageId;

        public TopPostsInPageReport(SocialNetwork socialNetwork, Page page)
        {
            SocialNetwork = socialNetwork;
            pageId = page.Id;

            ReportPages.Add(new ReportPage
            {
                PageId = page.Id,
                SocialNetwork = page.SocialNetwork,
                ReportId = Id
            });

            Key = String.Format(Consts.ReportKeyTmpl, pageId);
            PeriodType = DateTimeRangeType.Day;
            Avatar = page.Avatar;

            ReportCategory = Contracts.Domain.Reports.ReportCategory.TopPagePosts;
            IsUnique = true;
        }

        public override void AddData(IEnumerable<Post> data)
        {
            if (!data.Any()) return;

            var community = data.First().Page;

            if (community.Id == pageId)
            {
                lock (InstanceLocker)
                {
                    SetPosts(
                        Posts
                            .Concat(data)
                            .Distinct(new Post.EqualityComparer())
                            .OrderByDescending(x => x.LikesCount)
                            .Take(Consts.Count)
                            .ToList()
                        );
                }
            }
        }

        public static IEnumerable<TopPostsInPageReport> Create(SocialNetwork socialNetwork, IEnumerable<Page> pages)
        {
            return pages.Select(x => new TopPostsInPageReport(socialNetwork, x));
        }

        #region Private Methods

        void SetPosts(IEnumerable<Post> posts)
        {
            ReportPosts = posts.Select((x, i) => new ReportPost
            {
                PageId = x.PageId,
                SocialNetwork = x.SocialNetwork,
                PostId = x.Id,
                ReportId = Id,
                Post = x,
                Top = i + 1
            })
            .ToList();
        }

        #endregion
    }
}