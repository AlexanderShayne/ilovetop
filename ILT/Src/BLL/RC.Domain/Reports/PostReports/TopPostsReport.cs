using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.Entities.Images;
using Domain.ValueObjects.DateTimeRanges;
using ILT.Domain.Posts;
using ILT.Domain.PublicPages;

namespace ILT.Domain.Reports.PostReports
{
    public class TopPostsReport : ReportBase<IEnumerable<Post>>
    {
        private static class Consts
        {
            public const int Count = 300;
            public const int MaxPostsFromPage = 5;
        }

        public TopPostsReport(SocialNetwork socialNetwork, DateTimeRangeType periodType, string reportKey, string photo50, string photo100, string photo200, int order)
        {
            SocialNetwork = socialNetwork;
            PeriodType = periodType;
            Key = reportKey;
            Order = order;
            ReportCategory = Contracts.Domain.Reports.ReportCategory.TopPosts;

            Avatar = new Image
            {
                UrlTemplate = new Uri(photo50).GetLeftPart(UriPartial.Authority) + "{0}"
            };

            Avatar.AddVersion(50, new Uri(photo50).PathAndQuery);
            Avatar.AddVersion(100, new Uri(photo100).PathAndQuery);
            Avatar.AddVersion(200, new Uri(photo200).PathAndQuery);
        }

        #region Public Methods

        public override void AddData(IEnumerable<Post> data)
        {
            var posts = data.Where(x => !x.Page.ExcludeFromReports && Period.IsInDateRange(x.CreatedAt.Date));

            if (!posts.Any()) return;

            lock (InstanceLocker)
            {
                SetPosts(
                    Posts
                        .Concat(posts)
                        .Distinct(new Post.EqualityComparer())
                        .Distinct(new Post.TextEqualityComparer())
                        .GroupBy(x => x.Page.Id)
                        .SelectMany(x => x.OrderByDescending(c => c.LikesCount).Take(Consts.MaxPostsFromPage))
                        .OrderByDescending(c => c.LikesCount)
                        .Take(Consts.Count)
                        .ToList()
                    );
            }
        }

        public static ReportBase<IEnumerable<Post>>[] CreateAllPostReports(SocialNetwork socialNetwork, IEnumerable<Page> pages)
        {
            var topPostsInReports = TopPostsInPageReport.Create(socialNetwork, pages).ToArray();

            var topPostsReports = new ReportBase<IEnumerable<Post>>[]
            {
                new TopPostsPerDayReport(socialNetwork),
                new TopPostsPerWeekReport(socialNetwork),
                new TopPostsPerMonthReport(socialNetwork),
                new TopPostsPerYearReport(socialNetwork),
                new TopPostsPerAllTimeReport(socialNetwork)
            };

            return topPostsInReports.Concat(topPostsReports).ToArray();
        }

        #endregion

        #region Private Methods

        void SetPosts(IEnumerable<Post> posts)
        {
            ReportPosts = posts.Select((x, i) => new ReportPost
            {
                PageId = x.PageId,
                SocialNetwork = x.SocialNetwork,
                PostId = x.Id,
                ReportId = Id,
                Post = x,
                Top = i + 1
            })
            .ToList();
        }

        #endregion
    }
}