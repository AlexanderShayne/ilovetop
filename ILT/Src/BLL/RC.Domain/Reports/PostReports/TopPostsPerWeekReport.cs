using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.ValueObjects.DateTimeRanges;

namespace ILT.Domain.Reports.PostReports
{
    public class TopPostsPerWeekReport : TopPostsReport
    {
        private static class Consts
        {
            public const string ReportKey = "top-posts-per-week";
            public const string Photo50 = "http://ilovetop.net/app/assets/vk-communities/top-posts-per-week-50.png";
            public const string Photo100 = "http://ilovetop.net/app/assets/vk-communities/top-posts-per-week-100.png";
            public const string Photo200 = "http://ilovetop.net/app/assets/vk-communities/top-posts-per-week-200.png";
            public const int Order = 2;
        }

        public TopPostsPerWeekReport(SocialNetwork socialNetwork) : base(socialNetwork, DateTimeRangeType.Week, Consts.ReportKey, Consts.Photo50, Consts.Photo100, Consts.Photo200, Consts.Order)
        {
        }
    }
}