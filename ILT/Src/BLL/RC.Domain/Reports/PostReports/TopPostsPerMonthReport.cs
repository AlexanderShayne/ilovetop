﻿using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.ValueObjects.DateTimeRanges;

namespace ILT.Domain.Reports.PostReports
{
    public class TopPostsPerMonthReport : TopPostsReport
    {
        private static class Consts
        {
            public const string ReportKey = "top-posts-per-month";
            public const string Photo50 = "http://ilovetop.net/app/assets/vk-communities/top-posts-per-month-50.png";
            public const string Photo100 = "http://ilovetop.net/app/assets/vk-communities/top-posts-per-month-100.png";
            public const string Photo200 = "http://ilovetop.net/app/assets/vk-communities/top-posts-per-month-200.png";
            public const int Order = 3;
        }

        public TopPostsPerMonthReport(SocialNetwork socialNetwork) : base(socialNetwork, DateTimeRangeType.Month, Consts.ReportKey, Consts.Photo50, Consts.Photo100, Consts.Photo200, Consts.Order)
        {
        }
    }
}