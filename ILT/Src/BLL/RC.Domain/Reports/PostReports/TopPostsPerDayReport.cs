﻿using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.ValueObjects.DateTimeRanges;

namespace ILT.Domain.Reports.PostReports
{
    public class TopPostsPerDayReport : TopPostsReport
    {
        private static class Consts
        {
            public const string ReportKey = "top-posts-per-day";
            public const string Photo50 = "http://ilovetop.net/app/assets/vk-communities/top-posts-per-day-50.png";
            public const string Photo100 = "http://ilovetop.net/app/assets/vk-communities/top-posts-per-day-100.png";
            public const string Photo200 = "http://ilovetop.net/app/assets/vk-communities/top-posts-per-day-200.png";
            public const int Order = 1;
        }

        public TopPostsPerDayReport(SocialNetwork socialNetwork) : base(socialNetwork, DateTimeRangeType.Day, Consts.ReportKey, Consts.Photo50, Consts.Photo100, Consts.Photo200, Consts.Order)
        {
        }
    }
}