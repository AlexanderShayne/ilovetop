﻿using System;
using System.Collections.Generic;
using Contracts.Domain.Entities.Model;
using Contracts.Services.Infrastructure.SocialNetworks;
using FluentValidation;
using ILT.Contracts.Domain.Reports;
using ILT.Domain.Posts;

namespace ILT.Domain.Reports
{
    public class ReportPost : ICrudState, IHasTop
    {
        public Guid ReportId { get; set; }

        public long PostId { get; set; }
        public long PageId { get; set; }
        public SocialNetwork SocialNetwork { get; set; }

        public bool IsPosted { get; set; }

        public int Top { get; set; }
        public Post Post { get; set; }
        public CrudState CrudState { get; set; }

        #region Equality Comparer

        public class EqualityComparer : IEqualityComparer<ReportPost>
        {
            public bool Equals(ReportPost x, ReportPost y)
            {
                return x.ReportId.Equals(y.ReportId) && x.PageId.Equals(y.PageId) && x.PostId.Equals(y.PostId) && x.SocialNetwork.Equals(y.SocialNetwork);
            }

            public int GetHashCode(ReportPost obj)
            {
                return obj.ReportId.GetHashCode() + obj.PageId.GetHashCode() + obj.PostId.GetHashCode() + obj.SocialNetwork.GetHashCode();
            }
        }

        #endregion

        #region Validation

        public class Validator : AbstractValidator<ReportPost>
        {
            public Validator()
            {
                RuleFor(x => x.ReportId)
                    .NotEqual(Guid.Empty);

                RuleFor(x => x.PageId)
                    .GreaterThan(0);

                RuleFor(x => x.PostId)
                    .GreaterThan(0);

                RuleFor(x => x.Top)
                    .GreaterThan(0);

                RuleFor(x => x.Post)
                    .NotNull();

                RuleFor(x => x.Post)
                    .SetValidator(new Post.Validator());
            }
        }

        #endregion
    }
}