﻿using System;
using System.Collections.Generic;
using Contracts.Domain.Entities.Model;
using Contracts.Services.Infrastructure.SocialNetworks;
using FluentValidation;
using ILT.Contracts.Domain.Reports;
using ILT.Domain.PublicPages;

namespace ILT.Domain.Reports
{
    public class ReportPage : ICrudState, IHasTop
    {
        public Guid ReportId { get; set; }

        public long PageId { get; set; }
        public SocialNetwork SocialNetwork { get; set; }

        public int Top { get; set; }
        public int TopChanges { get; set; }
        public Page Page { get; set; }
        public CrudState CrudState { get; set; }

        #region Equality Comparer

        public class EqualityComparer : IEqualityComparer<ReportPage>
        {
            public bool Equals(ReportPage x, ReportPage y)
            {
                return x.ReportId.Equals(y.ReportId) && x.PageId.Equals(y.PageId) && x.SocialNetwork.Equals(y.SocialNetwork);
            }

            public int GetHashCode(ReportPage obj)
            {
                return obj.ReportId.GetHashCode() + obj.PageId.GetHashCode() + obj.SocialNetwork.GetHashCode();
            }
        }

        #endregion

        #region Validation

        public class Validator : AbstractValidator<ReportPage>
        {
            public Validator()
            {
                RuleFor(x => x.ReportId)
                    .NotEqual(Guid.Empty);

                RuleFor(x => x.PageId)
                    .GreaterThan(0);

                RuleFor(x => x.Top)
                    .GreaterThan(0);

                RuleFor(x => x.Page)
                    .NotNull();

                RuleFor(x => x.Page)
                    .SetValidator(new Page.Validator());
            }
        }

        #endregion
    }
}