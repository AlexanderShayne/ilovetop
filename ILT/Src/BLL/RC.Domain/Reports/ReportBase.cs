﻿namespace ILT.Domain.Reports
{
    public abstract class ReportBase<TData> : Report
    {
        public abstract void AddData(TData data);
    }
}