﻿using System;
using Contracts.Domain.Entities.Model;
using Domain.Entities.GuidEntities;
using FluentValidation;
using ILT.Contracts.Dal.Query.Hashtags.QueryRepositories;

namespace ILT.Domain.Hashtags
{
    public class Hashtag : GuidAggregateRootEntityBase
    {
        public string Name { get; set; }

        #region Validation

        public class Validator : AbstractValidator<Hashtag>
        {
            public Validator(IHashtagsQueryRepository hashtagsQueryRepository)
            {
                RuleFor(x => x.Id)
                    .NotEqual(Guid.Empty);

                RuleFor(x => x.IsDeleted)
                    .Equal(false)
                    .When(x => x.CrudState != CrudState.Deleted);

                RuleFor(x => x.CreatedAt)
                    .NotEqual(DateTime.MinValue);

                RuleFor(x => x.ModifiedAt)
                    .NotEqual(DateTime.MinValue);

                RuleFor(x => x.Name)
                    .NotEmpty()
                    .Must(x => x.StartsWith("#") || x.StartsWith("@"))
                    .WithMessage("Hashtag should start with # or @.")
                    .Length(0, 4000)
                    .Must(name => !hashtagsQueryRepository.Exists(name))
                    .WithMessage("Hashtag already exists.");

                RuleFor(x => x.Ts)
                    .Equal(null as byte[]);
            }
        }

        #endregion
    }
}
