using System;
using System.Collections.Generic;
using Contracts.Domain.Entities.Audit;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.Entities.GuidEntities;
using Domain.ValueObjects.DateTimeRanges;
using FluentValidation;

namespace ILT.Domain.PublicPages
{
    public class PageInfo : GuidEntityBase, ICreatedAt
    {
        public PageInfo()
        {
        }

        public PageInfo(Page page, int count)
        {
            PageId = page.Id;
            Count = count;
            SocialNetwork = page.SocialNetwork;
        }

        public long PageId { get; set; }
        public SocialNetwork SocialNetwork { get; set; }

        public int Count { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Today;

        public int? DayChanges { get; private set; }
        public int? WeekChanges { get; private set; }
        public int? MonthChanges { get; private set; }

        public void CalculateChanges(Func<long, SocialNetwork, DateTime, int?> getCountInSpecifiedDay)
        {
            DayChanges = Count - getCountInSpecifiedDay(PageId, SocialNetwork, GetComparedDate(DateTimeRangeType.Day));
            WeekChanges = Count - getCountInSpecifiedDay(PageId, SocialNetwork, GetComparedDate(DateTimeRangeType.Week));
            MonthChanges = Count - getCountInSpecifiedDay(PageId, SocialNetwork, GetComparedDate(DateTimeRangeType.Month));
        }

        #region Private Methods

        readonly IDictionary<DateTimeRangeType, DateTime> comparedDates = new Dictionary<DateTimeRangeType, DateTime>();
        DateTime GetComparedDate(DateTimeRangeType dateTimeRangeType)
        {
            if (!comparedDates.ContainsKey(dateTimeRangeType))
            {
                var period = new DateTimeRangeFactory().Create(dateTimeRangeType, 1);
                comparedDates[dateTimeRangeType] = period.Start;
            }

            return comparedDates[dateTimeRangeType];
        }

        #endregion

        #region Validation

        public class Validator : AbstractValidator<PageInfo>
        {
            public Validator()
            {
                RuleFor(x => x.Id)
                    .NotEqual(Guid.Empty);

                RuleFor(x => x.CreatedAt)
                    .NotEqual(DateTime.MinValue);

                RuleFor(x => x.Count)
                    .GreaterThanOrEqualTo(0);
            }
        }

        #endregion
    }
}