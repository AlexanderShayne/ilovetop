using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Crosscutting.IoC;
using Domain.Entities;
using Domain.Entities.Images;
using Domain.ValueObjects;
using FluentValidation;
using ILT.Contracts.Dal.Query.Reports.QueryRepositories;
using ILT.Domain.Hashtags;

namespace ILT.Domain.PublicPages
{
    public class PageKey : ValueObject<PageKey>
    {
        public long Id { get; set; }
        public SocialNetwork SocialNetwork { get; set; }
    }

    public class Page : AggregateRootEntityBase<long>
    {
        ISystemReportsQueryRepository SystemReportsQueryRepository => ContainerHolder.Container.Resolve<ISystemReportsQueryRepository>();

        public string Name { get; set; }
        public string ScreenName { get; set; }
        public string Permalink { get; set; }
        public bool ExcludeFromReports { get; set; }

        public SocialNetwork SocialNetwork { get; set; }

        public Image Avatar { get; set; }
        public PageInfo FollowersInfo { get; set; }
        public PageInfo LikesInfo { get; set; }
        public ICollection<Hashtag> Hashtags { get; set; }

        #region Public Methods

        public void AddFollowersInfo(int membersCount)
        {
            FollowersInfo = new PageInfo(this, membersCount);
            //FollowersInfo.CalculateChanges(SystemReportsQueryRepository.GetFollowersCount);
        }

        public void AddLikesInfo(int likesInfo)
        {
            LikesInfo = new PageInfo(this, likesInfo);
            //LikesInfo.CalculateChanges(SystemReportsQueryRepository.GetLikesCount);
        }

        #endregion

        #region Validation

        public class Validator : AbstractValidator<Page>
        {
            public Validator()
            {
                RuleFor(x => x.Id)
                    .GreaterThan(0);

                RuleFor(x => x.Name)
                    .NotEmpty()
                    .Length(0, 250);

                RuleFor(x => x.ScreenName)
                    .NotEmpty()
                    .Length(0, 250);

                RuleFor(x => x.Permalink)
                    .NotEmpty()
                    .Length(0, 4000);

                RuleFor(x => x.Avatar)
                    .NotNull();

                RuleFor(x => x.Hashtags)
                    .Must(HasUniqueHashtags)
                    .WithMessage("Dublicate hashtags.");

                RuleFor(x => x.FollowersInfo)
                    .SetValidator(new PageInfo.Validator());

                RuleFor(x => x.LikesInfo)
                    .SetValidator(new PageInfo.Validator());
            }

            static bool HasUniqueHashtags(ICollection<Hashtag> hashtags)
            {
                var hashset = new HashSet<string>();
                return hashtags.All(hashtag => hashset.Add(hashtag.Name));
            }
        }

        #endregion

        #region Equality Comparer

        public class EqualityComparer : IEqualityComparer<Page>
        {
            public bool Equals(Page x, Page y)
            {
                return x.Id.Equals(y.Id) && x.SocialNetwork.Equals(y.SocialNetwork);
            }

            public int GetHashCode(Page obj)
            {
                return obj.Id.GetHashCode() + obj.SocialNetwork.GetHashCode();
            }
        }

        #endregion
    }
}