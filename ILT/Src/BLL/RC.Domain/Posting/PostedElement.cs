﻿using Contracts.Services.Infrastructure.SocialNetworks;

namespace ILT.Domain.Posting
{
    public class PostedElement
    {
        public long Id { get; set; }
        public string Permalink { get; set; }
        public string OriginalTitle { get; set; }
        public SocialNetwork SocialNetwork { get; set; }
        public string Hashtags { get; set; }
        public string Message { get; set; }
    }
}