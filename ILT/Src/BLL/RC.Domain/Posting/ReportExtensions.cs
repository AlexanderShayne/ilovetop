using System;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Crosscutting.Infrastructure;
using Domain.ValueObjects.DateTimeRanges;
using ILT.Contracts.Domain.Posting;
using ILT.Domain.Reports;

namespace ILT.Domain.Posting
{
    public static class ReportExtensions
    {
        public static PostedElement GetNextNotPostedElement(this Report report, SharingType postingType)
        {
            PostedElement reporElement = null;

            if (postingType == SharingType.Report)
            {
                reporElement = new PostedElement
                {
                    Permalink = report.Permalink,
                    Hashtags = String.Join(" ", report.Hashtags.Select(x => x.Name)),
                    SocialNetwork = report.SocialNetwork
                };
            }

            if (postingType == SharingType.Post)
            {
                var reportItem = report.ReportPosts.OrderBy(x => x.Top).FirstOrDefault(x => !x.IsPosted);
                if (reportItem == null) return reporElement;

                var hashtags = report.Hashtags;

                if (reportItem.Post.Hashtags != null)
                {
                    hashtags = hashtags.Concat(reportItem.Post.Hashtags).ToList();
                }
                string name;

                switch (report.PeriodType)
                {
                    default:
                    case DateTimeRangeType.Day:
                        name = "���";
                        break;

                    case DateTimeRangeType.Week:
                        name = "������";
                        break;

                    case DateTimeRangeType.Month:
                        name = "������";
                        break;
                }

                string pageName = String.Empty;

                switch (report.SocialNetwork)
                {
                    default:
                    case SocialNetwork.Vk:
                        pageName = "�� ��������";
                        break;

                    case SocialNetwork.Coub:
                        pageName = "�� ������";
                        break;

                    case SocialNetwork.Vine:
                        pageName = "� ������������";
                        break;
                }

                var urlShortenerService = new Crosscutting.Infrastructure.UrlShortener.UrlShortenerService();

                var reportShortUrl = urlShortenerService.GetShortUrl(report.Permalink);
                var topPostsReportUrl = urlShortenerService.GetShortUrl($"http://ilovetop.net/{report.SocialNetwork.GetDescription()}/reports/top-posts-in-{reportItem.PageId}");

                reporElement = new PostedElement
                {
                    Id = reportItem.PostId,
                    OriginalTitle = reportItem.Post.Text,
                    Permalink = reportItem.Post.Permalink,
                    SocialNetwork = report.SocialNetwork,
                    Hashtags = String.Join(" ", hashtags.Select(x => x.Name)),
                    Message = String.Empty
                    //Message = $"��� {reportItem.Top} {report.SocialNetwork.GetDescription()} �� {name}.\r\n"
                    //Message = $"��� {reportItem.Top} {report.SocialNetwork.GetDescription()} {name} {reportShortUrl}\r\n" +
                    //          $"������ {pageName} {topPostsReportUrl}\r\n\r\n"
                };
            }

            return reporElement;
        }

        public static void MarkAsPosted(this Report report, SharingType postingType, string permanentUrl)
        {
            if (postingType == SharingType.Report)
            {
                report.IsPosted = true;
            }

            if (postingType == SharingType.Post)
            {
                var items = report.ReportPosts.Where(x => x.Post.Permalink == permanentUrl);

                foreach (var reportPost in items)
                {
                    reportPost.IsPosted = true;
                }
            }
        }
    }
}