﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.Entities;
using Domain.Entities.Images;
using FluentValidation;
using ILT.Domain.Hashtags;
using ILT.Domain.PublicPages;

namespace ILT.Domain.Posts
{
    public class Post : AggregateRootEntityBase<long>
    {
        public string Text { get; set; }
        public string Permalink { get; set; }

        public int CommentsCount { get; set; }
        public int LikesCount { get; set; }
        public int RepostsCount { get; set; }
        public int ViewsCount { get; set; }

        public long PageId { get; set; }
        public SocialNetwork SocialNetwork { get; set; }

        public Image Avatar { get; set; }
        public Page Page { get; set; }
        public ICollection<Hashtag> Hashtags { get; set; }

        #region Equality Comparer

        public class EqualityComparer : IEqualityComparer<Post>
        {
            public bool Equals(Post x, Post y)
            {
                return x.Id.Equals(y.Id) && x.PageId.Equals(y.PageId) && x.SocialNetwork.Equals(y.SocialNetwork);
            }

            public int GetHashCode(Post obj)
            {
                return obj.Id.GetHashCode() + obj.PageId.GetHashCode() + obj.SocialNetwork.GetHashCode();
            }
        }

        public class TextEqualityComparer : IEqualityComparer<Post>
        {
            public bool Equals(Post x, Post y)
            {
                return x.Text.Equals(y.Text, StringComparison.OrdinalIgnoreCase);
            }

            public int GetHashCode(Post obj)
            {
                return obj.Text.GetHashCode();
            }
        }

        #endregion

        #region Validation

        public class Validator : AbstractValidator<Post>
        {
            public Validator()
            {
                RuleFor(x => x.Id)
                    .GreaterThan(0);

                RuleFor(x => x.PageId)
                    .GreaterThan(0);

                RuleFor(x => x.CommentsCount)
                    .GreaterThanOrEqualTo(0);

                RuleFor(x => x.RepostsCount)
                    .GreaterThanOrEqualTo(0);

                RuleFor(x => x.LikesCount)
                    .GreaterThanOrEqualTo(0);

                RuleFor(x => x.Text)
                    .NotNull();

                RuleFor(x => x.Permalink)
                    .NotEmpty()
                    .Length(0, 4000);

                RuleFor(x => x.Avatar)
                    .NotNull();

                RuleFor(x => x.Hashtags)
                    .Must(HasUniqueHashtags)
                    .WithMessage("Dublicate hashtags.");
            }

            static bool HasUniqueHashtags(ICollection<Hashtag> hashtags)
            {
                var hashset = new HashSet<string>();
                return hashtags.All(hashtag => hashset.Add(hashtag.Name));
            }
        }

        #endregion
    }
}
