﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Contracts.Domain.Entities.Validation;
using Domain.Entities.Validation;
using Domain.ValueObjects.DateTimeRanges;
using FluentValidation;
using ILT.Domain.Hashtags;

namespace Domain.Configuration.Ioc
{
    public class Module : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component
                    .For<IValidationFactory>()
                    .ImplementedBy<ValidationFactory>()
                    .LifeStyle
                    .Singleton,

                Component
                    .For<IDateTimeRangeFactory>()
                    .ImplementedBy<DateTimeRangeFactory>()
                    .LifeStyle
                    .Singleton,

                Classes
                    .FromAssemblyContaining<Hashtag>()
                    .BasedOn(typeof(IValidator<>))
                    .WithServiceBase()
                    .LifestyleSingleton()
                );
        }
    }
}
