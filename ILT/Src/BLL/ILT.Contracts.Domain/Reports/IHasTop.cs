namespace ILT.Contracts.Domain.Reports
{
    public interface IHasTop
    {
        int Top { get; set; }
    }
}