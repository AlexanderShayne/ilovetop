using System.ComponentModel;

namespace ILT.Contracts.Domain.Reports
{
    public enum ReportCategory
    {
        [Description("top-posts")]
        TopPosts = 0,

        [Description("top-pages")]
        TopPages,

        [Description("anti-top-pages")]
        AntiTopPages,

        [Description("top-posts-in")]
        TopPagePosts
    }
}