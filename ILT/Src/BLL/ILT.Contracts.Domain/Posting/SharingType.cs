﻿namespace ILT.Contracts.Domain.Posting
{
    public enum SharingType
    {
        Report,
        Post,
        Page
    }
}