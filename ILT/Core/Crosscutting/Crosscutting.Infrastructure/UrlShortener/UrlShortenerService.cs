﻿using Contracts.Crosscutting.UrlShortener;
using Google.Apis.Services;
using Google.Apis.Urlshortener.v1;
using Google.Apis.Urlshortener.v1.Data;

namespace Crosscutting.Infrastructure.UrlShortener
{
    public class UrlShortenerService : IUrlShortenerService
    {
        #region Public Methods

        public string GetShortUrl(string longUrl)
        {
            var urlShortenerService = new UrlshortenerService(new BaseClientService.Initializer
            {
                ApiKey = "AIzaSyBR2E9sxI7N8M6_iS2z15gnE08AaNL_3C0"
            });

            var shortUrl =
                urlShortenerService
                    .Url
                    .Insert(new Url { LongUrl = longUrl })
                    .ExecuteAsync()
                    .Result
                    .Id;

            return shortUrl;
        }

        #endregion
    }
}
