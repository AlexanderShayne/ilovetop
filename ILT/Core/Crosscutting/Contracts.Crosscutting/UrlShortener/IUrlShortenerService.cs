﻿namespace Contracts.Crosscutting.UrlShortener
{
    public interface IUrlShortenerService
    {
        string GetShortUrl(string longUrl);
    }
}
