﻿using Contracts.Crosscutting.IoC;

namespace Crosscutting.IoC
{
    public static class ContainerHolder
    {
        public static IContainer Container { get; set; }
    }
}