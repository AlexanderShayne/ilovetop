﻿namespace Contracts.Domain.Entities.Model
{
    public interface ICrudState
    {
        CrudState CrudState { get; set; }
    }
}