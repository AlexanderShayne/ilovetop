﻿using Contracts.Domain.Entities.Model;

namespace Contracts.Domain.Entities
{
    public interface IEntityBase<TKey> : ICrudState
    {
        TKey Id { get; set; }
    }
}