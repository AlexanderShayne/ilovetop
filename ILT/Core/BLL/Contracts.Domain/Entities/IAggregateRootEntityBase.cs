using Contracts.Domain.Entities.Audit;
using Contracts.Domain.Entities.Model;
using Contracts.Domain.Entities.Validation;

namespace Contracts.Domain.Entities
{
    public interface IAggregateRootEntityBase<TKey> : IEntityBase<TKey>, IAuditable, IVersion, IDeletable, IValidation
    {
    }
}