﻿namespace Contracts.Domain.Entities.Audit
{
    public interface IAuditable : ICreatedAt, IModifiedAt
    {
    }
}