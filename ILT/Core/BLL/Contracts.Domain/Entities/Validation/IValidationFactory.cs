using FluentValidation;

namespace Contracts.Domain.Entities.Validation
{
    public interface IValidationFactory
    {
        IValidator<T> GetValidator<T>();
    }
}