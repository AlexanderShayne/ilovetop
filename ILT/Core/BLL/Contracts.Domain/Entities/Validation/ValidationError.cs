namespace Contracts.Domain.Entities.Validation
{
    public class ValidationError
    {
        public string Message { get; set; }
    }
}