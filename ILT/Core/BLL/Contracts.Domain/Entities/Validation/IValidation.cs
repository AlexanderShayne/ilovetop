﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Contracts.Domain.Entities.Validation
{
    public interface IValidation
    {
        bool IsValid { get; }
        bool IsNotValid { get; }

        Task<bool> IsValidAsync();
        Task<bool> IsNotValidAsync();

        ICollection<ValidationError> ValidationErrors { get; }
    }
}
