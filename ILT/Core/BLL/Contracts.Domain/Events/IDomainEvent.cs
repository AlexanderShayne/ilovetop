﻿using System;

namespace Contracts.Domain.Events
{
    public interface IDomainEvent
    {
        DateTime DateTimeEventOccurred { get; }
    }
}
