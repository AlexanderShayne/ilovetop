using Contracts.Domain.Entities.Validation;
using Crosscutting.IoC;
using FluentValidation;

namespace Domain.Entities.Validation
{
    public class ValidationFactory : IValidationFactory
    {
        public IValidator<T> GetValidator<T>()
        {
            return ContainerHolder.Container.Resolve<IValidator<T>>();
        }
    }
}