﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Contracts.Domain.Entities;
using Contracts.Domain.Entities.Validation;
using Crosscutting.IoC;
using FluentValidation;

namespace Domain.Entities
{
    public abstract class AggregateRootEntityBase<TKey> : EntityBase<TKey>, IAggregateRootEntityBase<TKey>
    {
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public byte[] Ts { get; set; }
        public bool IsDeleted { get; set; }

        #region Public Methods

        public bool IsValid => !IsNotValid;

        public bool IsNotValid
        {
            get
            {
                Validate();
                return ValidationErrors.Any();
            }
        }

        public async Task<bool> IsValidAsync()
        {
            return !await IsNotValidAsync();
        }

        public async Task<bool> IsNotValidAsync()
        {
            await ValidateAsync();
            return ValidationErrors.Any();
        }

        public ICollection<ValidationError> ValidationErrors { get; private set; }

        #endregion

        protected virtual IValidator GetValidator()
        {
            var factory = ContainerHolder.Container.Resolve<IValidationFactory>();

            MethodInfo method = typeof(IValidationFactory).GetMethod("GetValidator");

            var type = GetType();

            MethodInfo genericMethod = method.MakeGenericMethod(type);
            var validator = genericMethod.Invoke(factory, null) as IValidator;
            return validator;
        }

        #region Private Methods

        void Validate()
        {
            ValidationErrors =
                GetValidator()
                    .Validate(this)
                    .Errors
                    .Select(x => new ValidationError { Message = x.ErrorMessage })
                    .ToList();
        }

        async Task ValidateAsync()
        {
            var result = await GetValidator().ValidateAsync(this);

            ValidationErrors =
                result
                    .Errors
                    .Select(x => new ValidationError { Message = x.ErrorMessage })
                    .ToList();
        }

        #endregion
    }
}