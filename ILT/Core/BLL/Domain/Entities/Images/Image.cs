using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities.GuidEntities;

namespace Domain.Entities.Images
{
    public class Image : GuidEntityBase
    {
        public string UrlTemplate { get; set; }

        public Uri GetImageUri(int size)
        {
            var version = Versions.FirstOrDefault(x => x.Size == size);
            if (version == null) return null;

            var result = String.Format(UrlTemplate, version.TemplateValue);
            return new Uri(result);
        }

        public ICollection<ImageVersion> Versions { get; set; } = new List<ImageVersion>();

        #region Public Methods

        public Image AddVersion(int size, string templateValue)
        {
            //if (String.IsNullOrEmpty(templateValue)) return;

            Versions.Add(new ImageVersion
            {
                Size = size,
                TemplateValue = templateValue ?? String.Empty,
                ImageId = Id
            });

            return this;
        }

        #endregion

        #region EqualityComparer

        public class EqualityComparer : IEqualityComparer<Image>
        {
            public bool Equals(Image x, Image y)
            {
                return x.Id.Equals(y.Id);
            }

            public int GetHashCode(Image obj)
            {
                return obj.Id.GetHashCode();
            }
        }

        #endregion
    }
}