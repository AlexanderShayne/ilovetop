﻿using System;
using Contracts.Domain.Entities;
using Contracts.Domain.Entities.Model;

namespace Domain.Entities.GuidEntities
{
    public abstract class GuidEntityBase : IEntityBase<Guid>
    {
        protected GuidEntityBase()
        {
            Id = Guid.NewGuid();
        }

        public CrudState CrudState { get; set; }
        public Guid Id { get; set; }
    }
}