﻿namespace Contracts.Services.Infrastructure.Files.Models.View
{
    public class FileSummary
    {
        public string StorageType { get; set; }
        public string FileName { get; set; }
        public string Location { get; set; }
    }
}
