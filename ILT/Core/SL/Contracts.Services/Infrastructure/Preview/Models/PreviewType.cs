namespace Contracts.Services.Infrastructure.Preview.Models
{
    public enum PreviewType
    {
        Thumbnail,
        Preview
    }
}