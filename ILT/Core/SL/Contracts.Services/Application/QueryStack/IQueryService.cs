﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.Services.Application.QueryStack.Pagging.Models;

namespace Contracts.Services.Application.QueryStack
{
    public interface IQueryService
    {
        Task<PaggedResult<T>> QueryAsync<T>(Func<int, int, IEnumerable<T>> itemsGetter, Func<long> totalCountGetter, int page, int pageSize);
        Task<T> ReadAsync<T>(Func<T> itemGetter);
    }
}
