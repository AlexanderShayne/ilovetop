﻿namespace Contracts.Services.Application
{
    public interface ICreate<out TViewModel, in TInputModel>
    {
        TViewModel Create(TInputModel im);
    }
}
