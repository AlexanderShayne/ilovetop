﻿namespace Contracts.Services.Application
{
    public interface IDeleteChild<in TParentKey, in TKey>
    {
        void DeleteChild(TParentKey parentKey, TKey key);
    }
}