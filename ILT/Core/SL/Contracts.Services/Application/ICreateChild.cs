namespace Contracts.Services.Application
{
    public interface ICreateChild<in TParrentKey, out TViewModel, in TInputModel>
    {
        TViewModel CreateChild(TParrentKey key, TInputModel im);
    }
}