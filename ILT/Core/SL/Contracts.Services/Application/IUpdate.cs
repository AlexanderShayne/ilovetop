﻿namespace Contracts.Services.Application
{
    public interface IUpdate<in TKey, out TViewModel, in TInputModel>
    {
        TViewModel Update(TKey key, TInputModel model);
    }
}