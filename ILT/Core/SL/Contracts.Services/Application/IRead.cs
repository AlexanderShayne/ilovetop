namespace Contracts.Services.Application
{
    public interface IRead<out T, in TKey>
    {
        T Read(TKey key, string[] includes = null);
    }
}