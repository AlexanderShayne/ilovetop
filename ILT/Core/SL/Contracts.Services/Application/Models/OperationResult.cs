﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Contracts.Services.Application.Models
{
    public class OperationResult
    {
        public OperationResult()
        {
            Errors = new List<Exception>();
        }

        public OperationResult(string error) : this()
        {
            Errors.Add(new Exception(error));
        }

        public bool IsSuccess => !IsFailed;
        public bool IsFailed => Errors != null && Errors.Any();

        public ICollection<Exception> Errors { get; set; }

        public Exception Error => Errors.FirstOrDefault();
    }
}