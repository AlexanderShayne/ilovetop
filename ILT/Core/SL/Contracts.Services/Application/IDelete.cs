﻿namespace Contracts.Services.Application
{
    public interface IDelete<in TKey>
    {
        void Delete(TKey key);
    }
}