﻿namespace Contracts.Services.Application
{
    public interface ICrud<in TKey, in TInputModel, out TViewModel> :
        ICreate<TViewModel, TInputModel>,
        IRead<TViewModel, TKey>,
        IUpdate<TKey, TViewModel, TInputModel>,
        IDelete<TKey>
        where TInputModel : class
        where TViewModel : class
    {
    }
}