namespace Services.Infrastructure.Emails
{
    public class Template
    {
        public string Subject { get; set; }
        public string Tmpl { get; set; }
    }
}