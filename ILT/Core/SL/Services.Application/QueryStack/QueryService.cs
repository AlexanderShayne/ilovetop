﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.Services.Application.QueryStack;
using Contracts.Services.Application.QueryStack.Pagging.Models;

namespace Services.Application.QueryStack
{
    public class QueryService : IQueryService
    {
        public Task<PaggedResult<T>> QueryAsync<T>(Func<int, int, IEnumerable<T>> itemsGetter, Func<long> totalCountGetter, int page, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<T> ReadAsync<T>(Func<T> itemGetter)
        {
            throw new NotImplementedException();
        }
    }
}
