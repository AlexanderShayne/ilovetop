﻿namespace Contracts.Dal
{
    public static class DalConsts
    {
        public static class ConnectionString
        {
            public const string Oltp = "OltpConnectionString";
            public const string ReadOnly = "ReadOnlyConnectionString";
        }
    }
}