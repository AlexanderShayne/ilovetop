﻿using System.Threading.Tasks;
using Contracts.Crosscutting.IoC;
using Microsoft.Owin.Security.Google;
using UserContext.Contracts.Application.Models.Input;

namespace UserContext.Application.SocialNetworkProviders
{
    public class GoogleAuthProvider : GoogleOAuth2AuthenticationProvider
    {
        public IContainer Container { get; set; }

        ISocialNetworkRegistrationSevice SocialNetworkRegistrationSevice => Container.Resolve<ISocialNetworkRegistrationSevice>();
        const string ProviderName = "Google";

        public override Task ReturnEndpoint(GoogleOAuth2ReturnEndpointContext context)
        {
            context.RedirectUri = SocialNetworkRegistrationSevice.AddTokenToRedirectUrl(context.OwinContext, context.RedirectUri);
            return base.ReturnEndpoint(context);
        }

        public override async Task Authenticated(GoogleOAuth2AuthenticatedContext context)
        {
            var userModel = new UserProviderInputModel
            {
                LoginProvider = ProviderName,
                ProviderKey = context.Id,
                Email = context.Email,
                UserName = context.Name,
                AccessToken = context.AccessToken,
                Parameters = context.Properties.Dictionary
            };

            await SocialNetworkRegistrationSevice.AddAccessTokenToContext(context.OwinContext, userModel);
        }
    }
}