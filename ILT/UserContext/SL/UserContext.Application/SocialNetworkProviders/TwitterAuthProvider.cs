﻿using System;
using System.Threading.Tasks;
using System.Web;
using Contracts.Crosscutting.IoC;
using Microsoft.Owin.Security.Twitter;

namespace UserContext.Application.SocialNetworkProviders
{
    public class TwitterAuthProvider : TwitterAuthenticationProvider
    {
        public IContainer Container { get; set; }

        ISocialNetworkRegistrationSevice SocialNetworkRegistrationSevice => Container.Resolve<ISocialNetworkRegistrationSevice>();

        const string ProviderName = "Twitter";

        public override Task ReturnEndpoint(TwitterReturnEndpointContext context)
        {
            bool emailRequired = context.OwinContext.Get<bool>("EmailRequired");

            context.RedirectUri = SocialNetworkRegistrationSevice.AddTokenToRedirectUrl(context.OwinContext, context.RedirectUri);

            if (emailRequired)
            {
                var redirectUri = new UriBuilder(context.RedirectUri);
                var httpValueCollection = HttpUtility.ParseQueryString(redirectUri.Query);
                httpValueCollection.Add("emailrequired", "true");

                redirectUri.Query = httpValueCollection.ToString();
                context.RedirectUri = redirectUri.ToString();
            }

            return base.ReturnEndpoint(context);
        }

        public override async Task Authenticated(TwitterAuthenticatedContext context)
        {
            var parameters = context.Properties.Dictionary;

            //parameters.Add(User.ScreenName, context.ScreenName);
            //parameters.Add(User.AccessToken, context.AccessToken);
            //parameters.Add(User.AccessTokenSecret, context.AccessTokenSecret);

            var userModel = new UserProviderInputModel
            {
                LoginProvider = ProviderName,
                ProviderKey = context.UserId,
                Parameters = parameters,
                UserName = context.ScreenName,
                AccessToken = context.AccessToken,
                Email = ""
            };

            await SocialNetworkRegistrationSevice.AddAccessTokenToContext(context.OwinContext, userModel);
        }
    }
}