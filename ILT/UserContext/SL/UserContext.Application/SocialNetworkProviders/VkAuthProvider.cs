using System.Threading.Tasks;
using Contracts.Crosscutting.IoC;
using UserContext.Infrastructure.Vk.Provider;

namespace UserContext.Application.SocialNetworkProviders
{
    public class VkAuthProvider : VkAuthenticationProvider
    {
        public IContainer Container { get; set; }

        ISocialNetworkRegistrationSevice SocialNetworkRegistrationSevice => Container.Resolve<ISocialNetworkRegistrationSevice>();
        const string ProviderName = "Vk";

        public override Task ReturnEndpoint(VkReturnEndpointContext context)
        {
            context.RedirectUri = SocialNetworkRegistrationSevice.AddTokenToRedirectUrl(context.OwinContext, context.RedirectUri);
            return base.ReturnEndpoint(context);
        }

        public override async Task Authenticated(VkAuthenticatedContext context)
        {
            var userModel = new UserProviderInputModel
            {
                UserName = context.DefaultName,
                LoginProvider = ProviderName,
                ProviderKey = context.Id,
                Email = context.Email,
                AccessToken = context.AccessToken,
                Parameters = context.Properties.Dictionary
            };

            await SocialNetworkRegistrationSevice.AddAccessTokenToContext(context.OwinContext, userModel);
        }
    }
}