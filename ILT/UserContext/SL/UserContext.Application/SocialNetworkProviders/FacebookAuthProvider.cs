﻿using System.Threading.Tasks;
using Contracts.Crosscutting.IoC;
using Microsoft.Owin.Security.Facebook;

namespace UserContext.Application.SocialNetworkProviders
{
    public class FacebookAuthProvider : FacebookAuthenticationProvider
    {
        public IContainer Container { get; set; }

        ISocialNetworkRegistrationSevice SocialNetworkRegistrationSevice => Container.Resolve<ISocialNetworkRegistrationSevice>();
        const string ProviderName = "Facebook";

        public override Task ReturnEndpoint(FacebookReturnEndpointContext context)
        {
            context.RedirectUri = SocialNetworkRegistrationSevice.AddTokenToRedirectUrl(context.OwinContext, context.RedirectUri);
            return base.ReturnEndpoint(context);
        }

        public override async Task Authenticated(FacebookAuthenticatedContext context)
        {
            var userModel = new UserProviderInputModel
            {
                LoginProvider = ProviderName,
                ProviderKey = context.Id,
                Email = context.Email,
                UserName = context.UserName,
                AccessToken = context.AccessToken,
                Parameters = context.Properties.Dictionary
            };

            await SocialNetworkRegistrationSevice.AddAccessTokenToContext(context.OwinContext, userModel);
        }
    }
}