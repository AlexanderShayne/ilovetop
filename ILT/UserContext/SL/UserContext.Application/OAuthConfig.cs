﻿using System;
using Crosscutting.Infrastructure.Configuration;
using Crosscutting.IoC;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.Twitter;
using Owin;
using UserContext.Application.AuthenticationProviders;
using UserContext.Infrastructure.Vk;
using UserContext.Infrastructure.Vk.Provider;

namespace UserContext.Application
{
    public static class OAuthConfig
    {
        public static OAuthAuthorizationServerOptions OAuthBearerOptions;

        public static void Register(IAppBuilder app)
        {
            var config = new WebConfig();

            var issuer = config.Get<string>("oauthTokenIssuer");
            var audience = config.Get<string>("oauthClientId");
            var base64Secret = config.Get<string>("oauthSecret");
            SetupExternalOAuthProviders(app);
            SetupOAuthServer(app, issuer, audience, base64Secret);
            SetupOAauthAuthentication(app, issuer, audience, base64Secret);
        }

        private static void SetupOAauthAuthentication(IAppBuilder app, string issuer, string audience, string base64Secret)
        {
            var jwtBearerAuthenticationOptions = new JwtBearerAuthenticationOptions
            {
                AuthenticationMode = AuthenticationMode.Active,
                AllowedAudiences = new[] { audience },
                IssuerSecurityTokenProviders = new IIssuerSecurityTokenProvider[]
                {
                    new SymmetricKeyIssuerSecurityTokenProvider(issuer, TextEncodings.Base64Url.Decode(base64Secret))
                }
            };

            app.UseJwtBearerAuthentication(jwtBearerAuthenticationOptions);
        }

        private static void SetupOAuthServer(IAppBuilder app, string issuer, string audience, string base64Secret)
        {
            var config = new WebConfig();
            var enableRefreshTokens = config.Get<bool>("EnableRefreshTokens"); 

            OAuthBearerOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = OAuthConfigUtility.AllowInsecureHttp,
                TokenEndpointPath = new PathString("/Users/Token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(OAuthConfigUtility.AccessTokenExpireTimeMin),
                Provider = ContainerHolder.Container.Resolve<OAuthServerProvider>(),
                AccessTokenFormat = new JwtFormat(issuer, base64Secret, audience)
            };

            if (enableRefreshTokens)
            {
                OAuthBearerOptions.RefreshTokenProvider = ContainerHolder.Container.Resolve<IAuthenticationTokenProvider>();
            }
 
            app.UseOAuthAuthorizationServer(OAuthBearerOptions);
        }

        static void SetupExternalOAuthProviders(IAppBuilder app)
        {
            app.SetDefaultSignInAsAuthenticationType(DefaultAuthenticationTypes.ExternalCookie);

            var facebookAuthenticationOptions = new FacebookAuthenticationOptions
            {
                AppId = OAuthConfigUtility.FacebookAppId,
                AppSecret = OAuthConfigUtility.FacebookAppSecret,
                Provider = ContainerHolder.Container.Resolve<FacebookAuthenticationProvider>(),
                Scope = { "email" }
            };
            app.UseFacebookAuthentication(facebookAuthenticationOptions);

            var twitterAuthenticationOptions = new TwitterAuthenticationOptions
            {
                ConsumerKey = OAuthConfigUtility.TwitterConsumerKey,
                ConsumerSecret = OAuthConfigUtility.TwitterConsumerSecret,
                Provider = ContainerHolder.Container.Resolve<TwitterAuthenticationProvider>()
            };
            app.UseTwitterAuthentication(twitterAuthenticationOptions);

            var googleAuthOptions = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = OAuthConfigUtility.GoogleClientId,
                ClientSecret = OAuthConfigUtility.GoogleClientSecret,
                Provider = ContainerHolder.Container.Resolve<GoogleOAuth2AuthenticationProvider>()
            };
            app.UseGoogleAuthentication(googleAuthOptions);

            var vkAuthOptions = new VkAuthenticationOptions
            {
                AppId = "5465658",
                AppSecret = "rLdFB6gJSYGQBtYIsA4q",
                Provider = ContainerHolder.Container.Resolve<VkAuthenticationProvider>(),
                Scope = "wall,offline",
                SignInAsAuthenticationType = app.GetDefaultSignInAsAuthenticationType()
            };

            app.UseVkAuthentication(vkAuthOptions);
        }
    }
}