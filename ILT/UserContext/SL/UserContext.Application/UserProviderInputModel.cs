﻿using System.Collections.Generic;

namespace UserContext.Application
{
    public class UserProviderInputModel
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public string AccessToken { get; set; }

        public IDictionary<string, string> Parameters { get; set; }
    }
}