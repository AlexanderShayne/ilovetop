﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Contracts.Crosscutting.Configuration;
using Contracts.Services.Application;
using Contracts.Services.Infrastructure.Emails;
using Microsoft.AspNet.Identity;
using UserContext.Contracts.Application.Models.Input;
using UserContext.Contracts.Application.Models.View;
using UserContext.Contracts.Application.Services;
using UserContext.Data;
using UserContext.Domain;

namespace UserContext.Application
{
    public class UserContextService : IUserContextService
    {
        public Data.UserContext UserContext { get; set; }
        public MyUserManager UserManager { get; set; }
        public IEmailService EmailService { get; set; }
        public IConfig Config { get; set; }
        public IGuard Guard { get; set; }

        public UserSummary Create(UserDetails m)
        {
            Guard.NotNull(m);

            var user = User.Create(m.UserName, m.Email, m.NewPassword, m.Roles);

            Guard.DomainIsValid(user);

            IdentityResult identityResult = UserManager.CreateAsync(user, m.NewPassword).Result;

            GuardIfSuccess(identityResult);

            return new UserSummary
            {
                Email = user.Email,
                UserName = user.UserName,
                Id = user.Id,
                Roles = m.Roles
            };
        }

        public bool IsEmailUnique(string email)
        {
            var result = UserContext.Users.FirstOrDefault(x => x.Email == email) == null;
            return result;
        }

        public bool IsUserNameUnique(string userName)
        {
            var result = UserContext.Users.FirstOrDefault(x => x.UserName == userName) == null;
            return result;
        }

        public UserSummary Update(UserDetails m)
        {
            Guard.NotNull(m);

            var currentUserName = HttpContext.Current.User.Identity.Name;
            var user = UserManager.FindByNameAsync(currentUserName).Result;

            user.Email = m.Email ?? user.Email;
            user.UserName = m.UserName ?? user.Email;

            Guard.DomainIsValid(user);

            IdentityResult identityResult = UserManager.UpdateAsync(user).Result;

            GuardIfSuccess(identityResult);

            return new UserSummary
            {
                Email = user.Email,
                UserName = user.UserName,
                Id = user.Id,
                Roles = new string[0]
            };
        }

        public void ChangePassword(UserDetails vm)
        {
            var userId = Guid.Parse(HttpContext.Current.User.Identity.GetUserId());
            var identityResult = UserManager.ChangePasswordAsync(userId, vm.CurrentPassword, vm.NewPassword).Result;

            GuardIfSuccess(identityResult);
        }

        public void ChangePassword(Guid userId, UserDetails vm)
        {
            var identityResult = UserManager.ChangePasswordAsync(userId, vm.CurrentPassword, vm.NewPassword).Result;

            GuardIfSuccess(identityResult);
        }

        public void SendResetPasswordEmail(ForgottenPasswordDetails vm)
        {
            var user = UserManager.FindByEmailAsync(vm.Email).Result;

            if (user == null)
            {
                throw new ArgumentException($"Email {vm.Email} doesn't exist or is not confirmed.");
            }

            var code = UserManager.GeneratePasswordResetTokenAsync(user.Id).Result;
            var urlTmpl = Config.Get<string>("ForgottenPasswordUriTmpl");

            var url = String.Concat(urlTmpl, "?code=", Uri.EscapeDataString(code), "&accountId=", user.Id);

            var email = new EmailDetails
            {
                MailTo = user.Email,
                TmplName = "ResetPassword",
                Parameters = new Dictionary<string, string>
                    {
                        { "UserName", user.UserName },
                        { "Uri", url }
                    }
            };
            EmailService.Send(email);
        }

        public void ResetPassword(Guid id, ResetPasswordDetails vm)
        {
            var identityResult = UserManager.ResetPasswordAsync(id, vm.Code, vm.Password).Result;
            GuardIfSuccess(identityResult);
        }

        public void Dispose()
        {
            UserContext.Dispose();
            UserManager.Dispose();
        }

        #region Private Methods

        void GuardIfSuccess(IdentityResult result)
        {
            if (!result.Succeeded)
            {
                throw new ArgumentException(String.Join(Environment.NewLine, result.Errors));
            }
        }

        #endregion
    }
}