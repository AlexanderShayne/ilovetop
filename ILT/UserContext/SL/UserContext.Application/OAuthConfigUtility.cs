using Crosscutting.Infrastructure.Configuration;

namespace UserContext.Application
{
    public static class OAuthConfigUtility
    {
        private const string OauthServerConfigSectionPath = "oAuth/server/";
        private const string AllowInsecureHttpKey = "allowInsecureHttp";
        private const string AccessTokenExpireTimeMinKey = "accessTokenExpireTimeMin";

        private const string FacebookConfigSectionPath = "oAuth/facebook/";
        private const string AppIdKey = "appId";
        private const string AppSecretKey = "appSecret";

        private const string TwitterConfigSectionPath = "oAuth/twitter/";
        private const string ConsumerKey = "consumerKey";
        private const string ConsumerSecret = "consumerSecret";

        private const string GoogleConfigSectionPath = "oAuth/google/";
        private const string ClientId = "clientId";
        private const string ClientSecret = "clientSecret";

        public static bool AllowInsecureHttp => new WebConfig().Get<bool>(OauthServerConfigSectionPath + AllowInsecureHttpKey);

        public static int AccessTokenExpireTimeMin => new WebConfig().Get<int>(OauthServerConfigSectionPath + AccessTokenExpireTimeMinKey);

        public static string FacebookAppId => new WebConfig().Get<string>(FacebookConfigSectionPath + AppIdKey);

        public static string FacebookAppSecret => new WebConfig().Get<string>(FacebookConfigSectionPath + AppSecretKey);

        public static string TwitterConsumerKey => new WebConfig().Get<string>(TwitterConfigSectionPath + ConsumerKey);

        public static string TwitterConsumerSecret => new WebConfig().Get<string>(TwitterConfigSectionPath + ConsumerSecret);

        public static string GoogleClientId => new WebConfig().Get<string>(GoogleConfigSectionPath + ClientId);

        public static string GoogleClientSecret => new WebConfig().Get<string>(GoogleConfigSectionPath + ClientSecret);
    }
}