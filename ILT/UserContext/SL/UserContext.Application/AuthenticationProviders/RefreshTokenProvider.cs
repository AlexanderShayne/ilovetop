﻿using System;
using System.Threading.Tasks;
using Contracts.Crosscutting.IoC;
using Microsoft.Owin.Security.Infrastructure;
using UserContext.Domain;

namespace UserContext.Application.AuthenticationProviders
{
    public class RefreshTokenProvider : AuthenticationTokenProvider
    {
        public IContainer Container { get; set; }

        IRefreshTokenService RefreshTokenService => Container.Resolve<IRefreshTokenService>();

        public override async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            int clientId;

            var dictionary = context.Ticket.Properties.Dictionary;

            if(
                !dictionary.ContainsKey("as:client_id") ||
                String.IsNullOrEmpty(dictionary["as:client_id"]) ||
                !Int32.TryParse(dictionary["as:client_id"], out clientId))
            {
                return;
            }

            //var refreshTokenLifeTime = context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime");

            var token = new OAuthRefreshToken
            {
                ClientId = clientId.ToString(),
                Subject = context.Ticket.Identity.Name,
                IssuedUtc = DateTime.UtcNow,
                ExpiresUtc = DateTime.MaxValue
            };

            context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
            context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

            var refreshToken = context.SerializeTicket();

            token.ProtectedTicket = refreshToken;

            var result = RefreshTokenService.AddRefreshToken(token);

            if(result)
            {
                context.SetToken(refreshToken);
            }
        }

        public override async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin") ?? "*";
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });


            var refreshToken = RefreshTokenService.FindRefreshToken(context.Token);

            if(refreshToken != null)
            {
                context.DeserializeTicket(refreshToken.ProtectedTicket);
                RefreshTokenService.RemoveRefreshToken(refreshToken);
            }
        }
    }
}