﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Contracts.Crosscutting.IoC;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using UserContext.Data;
using UserContext.Domain;

namespace UserContext.Application.AuthenticationProviders
{
    public class OAuthServerProvider : OAuthAuthorizationServerProvider
    {
        public IContainer Container { get; set; }

        MyUserManager UserManager => Container.Resolve<MyUserManager>();
        Data.UserContext UserContext => Container.Resolve<Data.UserContext>();

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;

            if(!context.TryGetBasicCredentials(out clientId, out clientSecret))
            {
                context.TryGetFormCredentials(out clientId, out clientSecret);
            }

            if(context.ClientId == null)
            {
                context.Validated();
                return Task.FromResult<object>(null);
            }

            var client = UserContext.Clients.Find(Convert.ToInt32(context.ClientId));

            if(client == null)
            {
                context.SetError("invalid_clientId", string.Format("Client '{0}' is not registered in the system.", context.ClientId));
                return Task.FromResult<object>(null);
            }

            if(client.ApplicationType == ApplicationType.NativeConfidential)
            {
                if(string.IsNullOrWhiteSpace(clientSecret))
                {
                    context.SetError("invalid_clientId", "Client secret should be sent.");
                    return Task.FromResult<object>(null);
                }
            }

            if(!client.Active)
            {
                context.SetError("invalid_clientId", "Client is inactive.");
                return Task.FromResult<object>(null);
            }

            context.OwinContext.Set("as:clientAllowedOrigin", "*");
            //context.OwinContext.Set("as:clientRefreshTokenLifeTime", "2880");
            context.Validated();

            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            AddCorsHeader(context, "*");

            var user = await UserManager.FindByEmailAsync(context.UserName);

            if (user == null || !await UserManager.CheckPasswordAsync(user, context.Password))
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            var oAuthIdentity = await UserManager.CreateIdentityAsync(user, context.Options.AuthenticationType);

            context.Validated(new AuthenticationTicket(oAuthIdentity, CreateProperties(user, context.ClientId)));
        }

        public override Task MatchEndpoint(OAuthMatchEndpointContext context)
        {
            if(context.OwinContext.Request.Method == "OPTIONS" && context.IsTokenEndpoint)
            {
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Methods", new[] { "POST" });
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Headers", new[] { "accept", "authorization", "content-type" });
                context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
                context.OwinContext.Response.StatusCode = 200;
                context.RequestCompleted();

                return Task.FromResult<object>(null);
            }

            return base.MatchEndpoint(context);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
            var currentClient = context.ClientId;

            if(originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                return Task.FromResult<object>(null);
            }

            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach(var property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        void AddCorsHeader(OAuthGrantResourceOwnerCredentialsContext context, params string[] allowedOrigins)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", allowedOrigins);
        }

        AuthenticationProperties CreateProperties(User user, string clientId)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "as:client_id", clientId ?? String.Empty},
                { "id", user.Id.ToString() },
                { "email", user.Email ?? String.Empty },
                { "userName", user.UserName ?? String.Empty }
            };

            var result = new AuthenticationProperties(data);
            return result;
        }
    }
}