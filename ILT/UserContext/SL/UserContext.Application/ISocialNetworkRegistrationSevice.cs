using System.Threading.Tasks;
using Microsoft.Owin;
using UserContext.Domain;

namespace UserContext.Application
{
    public interface ISocialNetworkRegistrationSevice
    {
        string AddTokenToRedirectUrl(IOwinContext context, string redirectUrl);
        Task AddAccessTokenToContext(IOwinContext context, UserProviderInputModel m);
        Task<string> GenerateAccessToken(User user);
    }
}