﻿using System;
using System.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Thinktecture.IdentityModel.Tokens;

namespace UserContext.Application
{
    public class JwtFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly string _issuer;
        private readonly string _secret;
        private readonly string _audienceId;

        public JwtFormat(string issuer, string secret, string audienceId)
        {
            _issuer = issuer;
            _secret = secret;
            _audienceId = audienceId;

            if(string.IsNullOrWhiteSpace(_audienceId))
            {
                throw new InvalidOperationException("AudienceId can't be null");
            }
        }

        public string Protect(AuthenticationTicket data)
        {
            if(data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            var signingKey = new HmacSigningCredentials(TextEncodings.Base64Url.Decode(_secret));

            var issued = data.Properties.IssuedUtc;
            var expires = data.Properties.ExpiresUtc;

            var token = new JwtSecurityToken(
                issuer: _issuer,
                audience: _audienceId, 
                claims: data.Identity.Claims, 
                notBefore: issued.Value.UtcDateTime, 
                expires: expires.Value.UtcDateTime,
                signingCredentials: signingKey
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}