using UserContext.Domain;

namespace UserContext.Application
{
    public interface ISocialNetworkUserContext
    {
        User CreateOrUpdate(UserProviderInputModel im);
    }
}