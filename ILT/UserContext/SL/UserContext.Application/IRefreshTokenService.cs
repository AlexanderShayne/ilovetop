using UserContext.Domain;

namespace UserContext.Application
{
    public interface IRefreshTokenService
    {
        bool AddRefreshToken(OAuthRefreshToken token);
        bool RemoveRefreshToken(OAuthRefreshToken refreshToken);
        OAuthRefreshToken FindRefreshToken(string token);
    }
}