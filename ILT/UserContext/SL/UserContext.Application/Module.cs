﻿using System;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.Twitter;
using Services.Infrastructure.Emails;
using UserContext.Application.AuthenticationProviders;
using UserContext.Application.Queries;
using UserContext.Application.SocialNetworkProviders;
using UserContext.Application.Validators;
using UserContext.Contracts.Application.Services;
using UserContext.Data;
using UserContext.Domain;
using UserContext.Domain.Queries;
using UserContext.Domain.Validators;
using UserContext.Infrastructure.Vk.Provider;

namespace UserContext.Application
{
    public class Module : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(

                Component
                    .For<Data.UserContext>()
                    .ImplementedBy<Data.UserContext>()
                    .Named("authContext")
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<MyUserManager>()
                    .ImplementedBy<MyUserManager>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<IUserStore<User, Guid>>()
                    .ImplementedBy<MyUserStore>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<OAuthServerProvider>()
                    .ImplementedBy<OAuthServerProvider>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<IAuthenticationTokenProvider>()
                    .ImplementedBy<RefreshTokenProvider>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<FacebookAuthenticationProvider>()
                    .ImplementedBy<FacebookAuthProvider>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<TwitterAuthenticationProvider>()
                    .ImplementedBy<TwitterAuthProvider>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<GoogleOAuth2AuthenticationProvider>()
                    .ImplementedBy<GoogleAuthProvider>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<VkAuthenticationProvider>()
                    .ImplementedBy<VkAuthProvider>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<IUserContextService, IUserPasswordService>()
                    .ImplementedBy<UserContextService>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<IRefreshTokenService>()
                    .ImplementedBy<RefreshTokenService>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<ISocialNetworkUserContext>()
                    .ImplementedBy<SocialNetworkUserContext>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<IUserTokenProvider<User, Guid>>()
                    .ImplementedBy<EmailTokenProvider<User, Guid>>()
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<ISocialNetworkRegistrationSevice>()
                    .ImplementedBy<SocialNetworkRegistrationSevice>()
                    .DependsOn(Dependency.OnValue("accessTokenExpireTimeMin", OAuthConfigUtility.AccessTokenExpireTimeMin))
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<ITemplateReader>()
                    .ImplementedBy<EmailTmplReader>()
                    .LifeStyle
                    .PerWebRequest,

                //Component
                //    .For<IServiceBus>()
                //    .ImplementedBy<MassTransitAdapter>()
                //    .LifeStyle.Transient,

                Component
                    .For<IUserValidation>()
                    .ImplementedBy<IsEmailUniqueValidator>()
                    .Named("IsEmailUnique")
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<IUserValidation>()
                    .ImplementedBy<IsUserNameUniqueValidator>()
                    .Named("IsUserNameUnique")
                    .LifeStyle
                    .PerWebRequest,

                Component
                    .For<IRolesQuery>()
                    .ImplementedBy<RolesQuery>()
                    .LifeStyle
                    .PerWebRequest
            );

            //var bus = ServiceBusFactory.New(sbc =>
            //{
            //    sbc.UseMsmq(configurator =>
            //    {
            //        configurator.VerifyMsmqConfiguration();
            //    });
            //   // sbc.VerifyMsDtcConfiguration();
            //    sbc.UseJsonSerializer();
            //    sbc.ReceiveFrom(String.Format(new WebConfig().Get<string>("msmqNameTmpl"), Environment.MachineName));
            //    sbc.Subscribe(x => x.Handler<SendEmailMessage>(msg =>
            //    {
            //        var handler = container.Resolve<IHandle<SendEmailMessage>>();
            //        handler.Handle(msg);
            //        container.Release(handler);
            //    }));
            //});
            //
            //container.Register(Component.For<MassTransit.IServiceBus>().Instance(bus));
        }
    }
}