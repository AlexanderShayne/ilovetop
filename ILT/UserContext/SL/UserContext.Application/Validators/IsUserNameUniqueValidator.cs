using System.Linq;
using UserContext.Domain;
using UserContext.Domain.Validators;

namespace UserContext.Application.Validators
{
    public class IsUserNameUniqueValidator : IUserValidation
    {
        public Data.UserContext UserContext { get; set; }

        public bool IsValid(User user)
        {
            var result = UserContext.Users.FirstOrDefault(x => x.UserName == user.UserName) == null;
            return result;
        }
    }
}