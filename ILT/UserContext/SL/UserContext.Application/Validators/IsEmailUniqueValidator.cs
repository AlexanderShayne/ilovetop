using System.Linq;
using UserContext.Domain;
using UserContext.Domain.Validators;

namespace UserContext.Application.Validators
{
    public class IsEmailUniqueValidator : IUserValidation
    {
        public Data.UserContext UserContext { get; set; }

        public bool IsValid(User user)
        {
            var result = UserContext.Users.FirstOrDefault(x => x.Email == user.Email) == null;
            return result;
        }
    }
}