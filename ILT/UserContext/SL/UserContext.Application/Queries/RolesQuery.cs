using System.Collections.Generic;
using UserContext.Domain;
using UserContext.Domain.Queries;

namespace UserContext.Application.Queries
{
    public class RolesQuery : IRolesQuery
    {
        public Data.UserContext UserContext { get; set; }

        public IEnumerable<Role> Get()
        {
            return UserContext.Roles;
        }
    }
}