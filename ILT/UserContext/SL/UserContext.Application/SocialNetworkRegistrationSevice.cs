﻿using System;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using UserContext.Contracts.Application.Services;
using UserContext.Data;
using UserContext.Domain;

namespace UserContext.Application
{
    public class SocialNetworkRegistrationSevice : ISocialNetworkRegistrationSevice
    {
        public IUserContextService AuthService { get; set; }
        public ISocialNetworkUserContext SocialNetworkUserContext { get; set; }
        public IAuthenticationTokenProvider AuthenticationTokenProvider { get; set; }
        public MyUserManager UserManager { get; set; }
        public int AccessTokenExpireTimeMin { get; set; }

        const string AccessTokenErrorKey = "GetAccessTokenError";
        const string AccessTokenKey = "jwtAccessToken";
        const string RefreshTokenKey = "jwtRefreshToken";

        public string AddTokenToRedirectUrl(IOwinContext context, string redirectUrl)
        {
            var redirectUri = new UriBuilder(redirectUrl);
            var httpValueCollection = HttpUtility.ParseQueryString(redirectUri.Query);

            var error = context.Get<string>(AccessTokenErrorKey);
            var token = context.Get<string>(AccessTokenKey);
            var refreshToken = context.Get<string>(RefreshTokenKey);

            if (String.IsNullOrWhiteSpace(error))
            {
                if (!String.IsNullOrWhiteSpace(token))
                {
                    httpValueCollection.Add("access_token", token);
                    httpValueCollection.Add("refresh_token", refreshToken);
                }
            }

            if (!String.IsNullOrWhiteSpace(error))
            {
                httpValueCollection.Add("error", error);
            }

            redirectUri.Query = httpValueCollection.ToString();

            return redirectUri.ToString();
        }

        public async Task AddAccessTokenToContext(IOwinContext context, UserProviderInputModel m)
        {
            User user = null;

            try
            {
                user = SocialNetworkUserContext.CreateOrUpdate(m);
            }
            catch (Exception ex)
            {
                context.Set(AccessTokenErrorKey, ex.Message);
            }

            if (user == null) return;

            if (String.IsNullOrEmpty(user.Email))
            {
                context.Set("EmailRequired", true);
            }

            context.Set(AccessTokenKey, await GenerateAccessToken(user));

            var createContext = new AuthenticationTokenCreateContext(
                context,
                OAuthConfig.OAuthBearerOptions.RefreshTokenFormat,
                await Create(user));

            await AuthenticationTokenProvider.CreateAsync(createContext);

            context.Set(RefreshTokenKey, createContext.Token);
        }

        AuthenticationTicket authenticationTicket;

        async Task<AuthenticationTicket> Create(User user)
        {
            if (authenticationTicket == null)
            {
                var oAuthIdentity = await UserManager.CreateIdentityAsync(user, OAuthDefaults.AuthenticationType);

                authenticationTicket = new AuthenticationTicket(oAuthIdentity, new AuthenticationProperties
                {
                    IssuedUtc = DateTime.UtcNow,
                    ExpiresUtc = DateTime.UtcNow.Add(TimeSpan.FromMinutes(AccessTokenExpireTimeMin)),
                    Dictionary = { { "as:client_id", ApplicationType.JavaScript.ToString("d") } }
                });
            }

            return authenticationTicket;
        }

        public async Task<string> GenerateAccessToken(User user)
        {
            var ticket = await Create(user);
            var result =  OAuthConfig.OAuthBearerOptions.AccessTokenFormat.Protect(ticket);

            return result;
        }
    }
}