using System;
using System.Threading.Tasks;
using Contracts.Crosscutting.Configuration;
using Contracts.Services.Application;
using Contracts.Services.Infrastructure.Emails;
using Microsoft.AspNet.Identity;
using UserContext.Data;
using UserContext.Domain;

namespace UserContext.Application
{
    public class SocialNetworkUserContext : ISocialNetworkUserContext
    {
        public Data.UserContext UserContext { get; set; }
        public MyUserManager UserManager { get; set; }
        public IEmailService EmailService { get; set; }
        public IConfig Config { get; set; }
        public IGuard Guard { get; set; }

        public User CreateOrUpdate(UserProviderInputModel m)
        {
            var userLoginInfo = new UserLoginInfo(m.LoginProvider, m.ProviderKey);
            var user = FindAsync(userLoginInfo).Result;
            bool isNewAccount = user == null;

            IdentityResult identityResult;

            if (isNewAccount)
            {
                user = User.CreateExternalUser(m.Email, m.UserName, m.LoginProvider, m.ProviderKey, m.AccessToken,
                    m.Parameters);
                Guard.DomainIsValid(user);

                identityResult = UserManager.CreateAsync(user).Result;
            }
            else
            {
                user.UpdateExternalUser(m.Email, m.UserName, m.LoginProvider, m.ProviderKey, m.AccessToken,
                    m.Parameters);
                Guard.DomainIsValid(user);

                identityResult = UserManager.UpdateAsync(user).Result;
            }

            GuardIfSuccess(identityResult);

            return user;
        }

        #region Private Methods

        async Task<User> FindAsync(UserLoginInfo loginInfo)
        {
            User user = UserManager.Find(loginInfo);

            return user;
        }

        void GuardIfSuccess(IdentityResult result)
        {
            if (!result.Succeeded)
            {
                throw new ArgumentException(String.Join(Environment.NewLine, result.Errors));
            }
        }

        #endregion
    }
}