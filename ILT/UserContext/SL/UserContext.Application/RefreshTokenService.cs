using System.Data.Entity;
using System.Linq;
using UserContext.Domain;

namespace UserContext.Application
{
    public class RefreshTokenService : IRefreshTokenService
    {
        public Data.UserContext UserContext { get; set; }

        public bool AddRefreshToken(OAuthRefreshToken token)
        {
            var clientId = token.ClientId;
            var existingToken = UserContext
                .RefreshTokens
                .SingleOrDefault(r => r.Subject == token.Subject && r.ClientId == clientId);

            if (existingToken != null)
            {
                var result = RemoveRefreshToken(existingToken);
            }

            UserContext.RefreshTokens.Add(token);

            return UserContext.SaveChanges() > 0;
        }

        public bool RemoveRefreshToken(OAuthRefreshToken refreshToken)
        {
            UserContext.RefreshTokens.Remove(refreshToken);
            return UserContext.SaveChanges() > 0;
        }

        public OAuthRefreshToken FindRefreshToken(string token)
        {
            var result = UserContext.RefreshTokens.FirstOrDefaultAsync(t => t.ProtectedTicket == token).Result;
            return result;
        }
    }
}