using System;
using UserContext.Contracts.Application.Models.Input;

namespace UserContext.Contracts.Application.Services
{
    public interface IUserPasswordService
    {
        void ChangePassword(UserDetails vm);
        void ChangePassword(Guid userId, UserDetails vm);
        void SendResetPasswordEmail(ForgottenPasswordDetails vm);
        void ResetPassword(Guid id, ResetPasswordDetails vm);
    }
}