using System;
using UserContext.Contracts.Application.Models.Input;
using UserContext.Contracts.Application.Models.View;

namespace UserContext.Contracts.Application.Services
{
    public interface IUserContextService : IDisposable, IUserPasswordService
    {
        UserSummary Create(UserDetails m);
        UserSummary Update(UserDetails userUpdateDetails);

        bool IsEmailUnique(string email);
        bool IsUserNameUnique(string userName);
    }
}