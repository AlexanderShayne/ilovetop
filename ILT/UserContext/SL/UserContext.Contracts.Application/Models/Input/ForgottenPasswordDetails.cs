﻿using System.ComponentModel.DataAnnotations;

namespace UserContext.Contracts.Application.Models.Input
{
    public class ForgottenPasswordDetails
    {
        [MaxLength(256)]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$")]
        public string Email { get; set; }
    }
}