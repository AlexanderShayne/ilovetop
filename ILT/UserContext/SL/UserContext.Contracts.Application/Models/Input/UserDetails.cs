﻿namespace UserContext.Contracts.Application.Models.Input
{
    public class UserDetails
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string[] Roles { get; set; }
    }
}