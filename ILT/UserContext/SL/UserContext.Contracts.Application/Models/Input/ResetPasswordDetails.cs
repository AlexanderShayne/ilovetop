using System.ComponentModel.DataAnnotations;

namespace UserContext.Contracts.Application.Models.Input
{
    public class ResetPasswordDetails
    {
        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [Required]
        public string Code { get; set; }
    }
}