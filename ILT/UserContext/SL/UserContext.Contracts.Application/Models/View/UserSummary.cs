﻿using System;

namespace UserContext.Contracts.Application.Models.View
{
    public class UserSummary
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

        public string[] Roles { get; set; }
    }
}