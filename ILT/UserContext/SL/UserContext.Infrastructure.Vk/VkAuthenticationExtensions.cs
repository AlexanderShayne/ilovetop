using Owin;

namespace UserContext.Infrastructure.Vk
{
    public static class VkAuthenticationExtensions
    {
        public static IAppBuilder UseVkAuthentication(this IAppBuilder app, VkAuthenticationOptions options)
        {
            app.Use(typeof(VkAuthenticationMiddleware), app, options);
            return app;
        }
    }
}
