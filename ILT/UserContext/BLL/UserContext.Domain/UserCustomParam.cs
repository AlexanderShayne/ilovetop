﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entities;

namespace UserContext.Domain
{
    [Table("UserCustomParam")]
    public class UserCustomParam : EntityBase<Guid>
    {
        public UserCustomParam()
        {
            Id = Guid.NewGuid();
        }

        [Column(Order = 0), Key]
        public Guid UserId { get; set; }

        [Column(Order = 1), Key]
        [MaxLength(50)]
        public string Key { get; set; }

        [MaxLength(2048)]
        public string Value { get; set; }
    }
}