using System.Collections.Generic;

namespace UserContext.Domain.Queries
{
    public interface IRolesQuery
    {
        IEnumerable<Role> Get();
    }
}