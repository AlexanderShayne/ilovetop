﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Contracts.Domain.Entities;
using Contracts.Domain.Entities.Model;
using Contracts.Domain.Entities.Validation;
using Crosscutting.Infrastructure;
using Crosscutting.IoC;
using FluentValidation;
using Microsoft.AspNet.Identity.EntityFramework;
using UserContext.Domain.Queries;
using UserContext.Domain.Validators;

namespace UserContext.Domain
{
    public class User : IdentityUser<Guid, UserLogin, UserRole, UserClaim>, IAggregateRootEntityBase<Guid>
    {
        public User()
        {
            Id = Guid.NewGuid();
        }

        [NotMapped]
        public string Password { get; set; }

        [NotMapped]
        public bool IsExternalLogin { get; set; }

        [ForeignKey("UserId")]
        public virtual ICollection<UserCustomParam> UserCustomParams { get; set; }

        [NotMapped]
        public CrudState CrudState { get; set; }

        [NotMapped]
        public DateTime CreatedAt { get; set; }

        [NotMapped]
        public DateTime ModifiedAt { get; set; }

        [NotMapped]
        public byte[] Ts { get; set; }

        [NotMapped]
        public bool IsDeleted { get; set; }

        public bool IsNotValid
        {
            get
            {
                Validate();
                return ValidationErrors.Any();
            }
        }

        public Task<bool> IsValidAsync()
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsNotValidAsync()
        {
            throw new NotImplementedException();
        }

        public bool IsValid => !IsNotValid;

        void Validate()
        {
            ValidationErrors = GetValidator()?.Validate(this)?.Errors.Select(x => new ValidationError { Message = x.ErrorMessage }).ToList();
        }

        [NotMapped]
        public ICollection<ValidationError> ValidationErrors { get; private set; }

        public class Validator : AbstractValidator<User>
        {
            public Validator()
            {
                RuleFor(x => x.UserName)
                    .NotNull()
                    .Length(0, 256);

                RuleFor(x => x.UserName)
                    .NotEmpty()
                    .Matches(@"(^[a-zA-Z0-9\.\-]+$)")
                    .When(x => !x.IsExternalLogin);

                RuleFor(x => x.Email)
                    .NotNull()
                    .Length(0, 256);

                RuleFor(x => x.Email)
                    .Matches(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$")
                    .When(x => !x.IsExternalLogin);

                RuleFor(x => x.Password)
                    .NotEmpty()
                    .Length(6, 100)
                    .When(x => !x.IsExternalLogin);

                foreach (var userValidation in ContainerHolder.Container.ResolveAll<IUserValidation>())
                {
                    RuleFor(x => x).Must(userValidation.IsValid).When(x => !x.IsExternalLogin);
                }
            }
        }

        protected virtual IValidator GetValidator()
        {
            return new Validator();
        }

        public static User Create(string userName, string email, string password, string[] roles)
        {
            var user = new User
            {
                Email = email,
                UserName = String.IsNullOrEmpty(userName) ? String.IsNullOrEmpty(email) ? null : email.Split('@')[0] : userName,
                EmailConfirmed = true,
                Password = password
            };

            user.AddRoles(roles);

            return user;
        }

        public void AddRoles(params string[] roles)
        {
            if (roles.IsNullOrEmpty()) return;

            var rolesQuery = ContainerHolder.Container.Resolve<IRolesQuery>();
            var allRoles = rolesQuery.Get();

            foreach (var role in roles)
            {
                var roleId = allRoles.First(x => String.Equals(x.Name, role, StringComparison.OrdinalIgnoreCase)).Id;

                if (Roles.All(x => x.RoleId != roleId))
                {
                    Roles.Add(new UserRole
                    {
                        RoleId = roleId
                    });
                }
            }
        }

        public void UpdateExternalUser(string email, string userName, string loginProvider, string providerKey, string accessToken, IDictionary<string, string> parameters)
        {
            Email = email ?? String.Empty;
            UserName = userName ?? String.Empty;
            IsExternalLogin = true;

            var login = Logins.Single();
            login.LoginProvider = loginProvider;
            login.ProviderKey = providerKey;
            login.AccessToken = accessToken;
        }

        public static User CreateExternalUser(string email, string userName, string loginProvider, string providerKey, string accessToken, IDictionary<string, string> parameters)
        {
            var user = new User
            {
                Logins =
                {
                    new UserLogin()
                }
            };

            user.UpdateExternalUser(email, userName, loginProvider, providerKey, accessToken, parameters);

            return user;
        }
    }
}