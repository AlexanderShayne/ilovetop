﻿namespace UserContext.Domain
{
    public enum ApplicationType
    {
        JavaScript = 1,
        NativeConfidential = 2
    };
}