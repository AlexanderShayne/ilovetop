﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.Domain.Entities;
using Contracts.Domain.Entities.Model;
using Microsoft.AspNet.Identity.EntityFramework;

namespace UserContext.Domain
{
    public class Role : IdentityRole<Guid, UserRole>, IEntityBase<Guid>
    {
        [NotMapped]
        public CrudState CrudState { get; set; }
    }
}