﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;

namespace UserContext.Domain
{
    public class UserLogin : IdentityUserLogin<Guid>
    {
        public string AccessToken { get; set; }
    }
}