﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.Domain.Entities;
using Contracts.Domain.Entities.Model;

namespace UserContext.Domain
{
    [Table("OAuthRefreshToken")]
    public class OAuthRefreshToken : IEntityBase<Guid>
    {
        public OAuthRefreshToken()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        public Guid Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Subject { get; set; }
        [Required]
        [MaxLength(50)]
        public string ClientId { get; set; }
        public DateTime IssuedUtc { get; set; }
        public DateTime ExpiresUtc { get; set; }
        [Required]
        public string ProtectedTicket { get; set; }

        [NotMapped]
        public CrudState CrudState { get; set; }
    }
}