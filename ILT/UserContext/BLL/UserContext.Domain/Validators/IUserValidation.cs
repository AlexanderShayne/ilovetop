namespace UserContext.Domain.Validators
{
    public interface IUserValidation
    {
        bool IsValid(User user);
    }
}