﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.Domain.Entities;
using Contracts.Domain.Entities.Model;

namespace UserContext.Domain
{
    [Table("OAuthClient")]
    public class OAuthClient : IEntityBase<int>
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Secret { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public ApplicationType ApplicationType { get; set; }
        public bool Active { get; set; }
        public int RefreshTokenLifeTime { get; set; }
        [MaxLength(100)]
        public string AllowedOrigin { get; set; }

        [NotMapped]
        public CrudState CrudState { get; set; }
    }
}