﻿using System;
using Microsoft.AspNet.Identity.EntityFramework;
using UserContext.Domain;

namespace UserContext.Data
{
    public class MyUserStore : UserStore<User, Role, Guid, UserLogin, UserRole, UserClaim>
    {
        public MyUserStore(UserContext context)
            : base(context)
        {
        }
    }
}