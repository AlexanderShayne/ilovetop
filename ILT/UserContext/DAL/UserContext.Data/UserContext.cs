﻿using System;
using System.Data.Entity;
using Contracts.Dal;
using Microsoft.AspNet.Identity.EntityFramework;
using UserContext.Domain;

namespace UserContext.Data
{
    public class UserContext : IdentityDbContext<User, Role, Guid, UserLogin, UserRole, UserClaim>
    {
        public UserContext() : base(DalConsts.ConnectionString.Oltp) { }

        public DbSet<OAuthClient> Clients { get; set; }
        public DbSet<OAuthRefreshToken> RefreshTokens { get; set; }
    }
}