﻿using System;
using Microsoft.AspNet.Identity;
using UserContext.Domain;

namespace UserContext.Data
{
    public class MyUserManager : UserManager<User, Guid>
    {
        public MyUserManager(IUserStore<User, Guid> store, IUserTokenProvider<User, Guid> userTokenProvider)
            : base(store)
        {
            UserTokenProvider = userTokenProvider;
            UserValidator = new UserValidator<User, Guid>(this)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = false
            };
        }
    }
}