﻿using System.Linq;
using Crosscutting.Infrastructure.RetryInvocation;
using NUnit.Framework;
using FluentAssertions;
using ILT.Vk.Infrastructure.VkApis.Services;

namespace Tests.VkApi
{
    [TestFixture]
    public class GroupsFixture
    {
        [Test]
        public void GetById()
        {
            var retry = new Retry();
            var vk = new VkGroups(retry);

            var range = Enumerable.Range(1, 500).Cast<long>();

            var data = vk.GetById(range);

            data.Should().NotBeNull();
            data.LongCount().ShouldBeEquivalentTo(500);
        }

        [Test]
        public void GetById_2Requests()
        {
            var retry = new Retry();
            var vk = new VkGroups(retry);

            AssertGetById(vk, 1);
            AssertGetById(vk, 101);
        }

        void AssertGetById(VkGroups vkGroups, int startId)
        {
            var range = Enumerable.Range(startId, 100).Cast<long>();

            var data = vkGroups.GetById(range);

            data.Should().NotBeNull();
            data.LongCount().ShouldBeEquivalentTo(100);
        }

        [Test]
        public void GetById_Id()
        {
            var retry = new Retry();
            var vk = new VkGroups(retry);

            var data = vk.GetById(new long[]{ 1 });

            data.Should().NotBeNull();
            data.LongCount().ShouldBeEquivalentTo(1);
            data.Single().Id.Should().Be(1);
        }

        [Test]
        public void GetById_Photos()
        {
            var retry = new Retry();
            var vk = new VkGroups(retry);

            var data = vk.GetById(new long[] { 1 });

            data.Should().NotBeNull();
            data.LongCount().ShouldBeEquivalentTo(1);
            var item = data.Single();

            item.Photo_50.Should().NotBeNullOrEmpty();
            item.Photo_100.Should().NotBeNullOrEmpty();
            item.Photo_200.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void GetById_MembersCount()
        {
            var retry = new Retry();
            var vk = new VkGroups(retry);

            var data = vk.GetById(new long[] { 1 });

            data.Should().NotBeNull();
            data.LongCount().ShouldBeEquivalentTo(1);
            data.Single().MembersCount.Should().BeGreaterThan(0);
        }
    }
}
