﻿using System;
using Crosscutting.Infrastructure.RetryInvocation;
using FluentAssertions;
using ILT.Vk.Infrastructure.VkApis.Services;
using NUnit.Framework;

namespace Tests.VkApi
{
    [TestFixture]
    public class WallFixture
    {
        [Test]
        public void Test()
        {
            var retry = new Retry();
            var vkWall = new VkWall(retry);

            var response = vkWall.Get(1, 0, 100);

            response.Should().NotBeNull();
            response.Count.Should().BeGreaterThan(0);
            response.Items.Should().NotBeNull();
        }

        [Test]
        public void Test2()
        {
            var date = new DateTimeOffset(1, 1, 1, 6, 0, 0, 0, new TimeSpan(0, 3, 0, 0));

            Console.WriteLine(date.ToString());
        }
    }
}