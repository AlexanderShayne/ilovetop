﻿using System.Linq;
using Crosscutting.Infrastructure.RetryInvocation;
using FluentAssertions;
using ILT.Coub.Infrastructure.CoubApis.Services;
using NUnit.Framework;

namespace Tests.CoubApis
{
    [TestFixture]
    public class CoubChannelsFixture
    {
        [Test]
        public void GetCoubChannelsTest()
        {
            var retry = new Retry();
            var coubChannels = new CoubChannels(retry);

            var range = Enumerable.Range(1, 50).Cast<long>();

            var data = coubChannels.GetChannels(range);

            data.Should().NotBeNull();
        }

        [Test]
        public void GetCoubChannelCoubsTest()
        {
            var retry = new Retry();
            var coubChannelCoubs = new CoubChannelCoubs(retry);

            var data = coubChannelCoubs.GetCoubs(2464846, 1, 1000);

            data.Should().NotBeNull();
        }
    }
}
