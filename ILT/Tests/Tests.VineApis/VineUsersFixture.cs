﻿using System.Linq;
using Crosscutting.Infrastructure.RetryInvocation;
using FluentAssertions;
using ILT.Vine.Infrustructure.VineApis.Services;
using NUnit.Framework;

namespace Tests.VineApis
{
    [TestFixture]
    public class CoubChannelsFixture
    {
        [Test]
        public void GetVineUsersTest()
        {
            var retry = new Retry();
            var vineUsers = new VineUsers(retry);

            var data = vineUsers.GetUsers("a a", 2, 100);

            data.Should().NotBeNull();
        }
    }
}
