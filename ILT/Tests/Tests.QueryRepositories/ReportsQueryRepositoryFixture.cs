﻿using System;
using System.Diagnostics;
using Contracts.Services.Infrastructure.SocialNetworks;
using Domain.ValueObjects.DateTimeRanges;
using ILT.Data.Reports.QueryRepositories;
using NUnit.Framework;

namespace Tests.QueryRepositories
{
    [TestFixture]
    public class ReportsQueryRepositoryFixture
    {
        [Test]
        public void TestQuery()
        {
            var repo = new ReportsQueryRepository(new DateTimeRangeFactory());

            Stopwatch sw = new Stopwatch();

            sw.Start();

            var result = repo.Query(SocialNetwork.Coub, 1, 10, null);

            sw.Stop();

            Console.WriteLine(sw.ElapsedMilliseconds);
        }
    }
}
